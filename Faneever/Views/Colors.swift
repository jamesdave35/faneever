//
//  Colors.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

 class Colors {
    
     static let greenBrandColor = UIColor(named: "greenBrand")!
     static let disabledButtonColor = UIColor(hexString: "B2B2B2")
     static let primaryBackground = UIColor(named: "primaryBackground")!
     static let alternatePrimaryBackground = UIColor(named: "alternatePrimaryBackground")!
     static let secondaryBackground = UIColor(named: "secondaryBackground")!
     static let tertiaryBackgroundColor = UIColor(named: "tertiaryBackground")!
     static let profilesMenu = UIColor(named: "profilesMenuColor")!
     static let comparePlayers = UIColor(named: "comparePlayers")!
     static let compareTeams = UIColor(named: "compareTeams")!
     static let alternateComparePlayers = UIColor(named: "alternateComparePlayers")!
     static let playerProfileHeader = UIColor(named: "playerProfileHeader")!
     static let alternateCompareTeams = UIColor(named: "alternateCompareTeams")!
     static let compareBackground = UIColor(named: "compareBackground")!
     static let primaryText = UIColor(named: "primaryText")!
     static let secondaryText = UIColor(named: "secondaryText")!
    
     static let pithcViewColor: UIColor = {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return UIColor(hexString: "202020")
                } else {
                    return UIColor(hexString: "00C853").darker(by: 40)
                }
            }
        } else {
            return UIColor(hexString: "202020")
        }
     }()
    
     static let seperatorColor: UIColor = {
        if #available(iOS 13, *)  {
            return UIColor.separator
        } else {
            return UIColor.lightGray
        }
     }()
    
     static let placeholderColor: UIColor = {
        if #available(iOS 13, *)  {
            return UIColor.placeholderText
        } else {
            return UIColor.lightGray
        }
    }()
    

}
