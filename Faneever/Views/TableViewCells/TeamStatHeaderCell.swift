//
//  TeamStatHeaderCell.swift
//  Faneever
//
//  Created by James Meli on 5/6/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamStatHeaderCell: UITableViewCell {

    @IBOutlet weak var headerName: PaddingLabel!
    

    
    var header: String? {
        didSet {
            self.headerName.text = header!
        }
    }

}
