//
//  LineupBenchCell.swift
//  Faneever
//
//  Created by James Meli on 4/6/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class LineupBenchCell: UITableViewCell {

    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var numberOfAssists: AnimatableLabel!
    @IBOutlet weak var numberOfGoals: AnimatableLabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var assistImage: UIImageView!
    @IBOutlet weak var ballImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = Colors.secondaryBackground
        playerName.textColor = Colors.primaryText
        numberOfGoals.fillColor = Colors.tertiaryBackgroundColor
        numberOfAssists.fillColor = Colors.tertiaryBackgroundColor
        numberOfGoals.textColor = Colors.secondaryText
        numberOfAssists.textColor = Colors.secondaryText
        position.textColor = Colors.secondaryText
    }
    
    override func prepareForReuse() {
        playerImage.image = Constants.defaultFaceImage
        playerName.text = ""
        numberOfGoals.isHidden = true
        numberOfAssists.isHidden = true
        ballImage.isHidden = true
        assistImage.isHidden = true
        cardView.isHidden = true
    }
    
    var player: LineupPlayer? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let player = player {
            if let name = player.player_name, let number = player.number {
                playerName.text = "\(number) \(name)"
            }
            
            if let pos = player.position {
                position.text = Constants.positionMap[pos]
            }
            
            if let url = player.player?.data?.getImageURL() {
                playerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
            
            if player.hasScored() {
                ballImage.isHidden = false
                if player.numberOfGoals() > 1 {
                    numberOfGoals.isHidden = false
                    numberOfGoals.text = "\(player.numberOfGoals())"
                }
            }
            
            if player.hasAssited() {
                assistImage.isHidden = false
                if player.numberOfAssits() > 1 {
                    numberOfAssists.isHidden = false
                    numberOfAssists.text =  "\(player.numberOfAssits())"
                }
            }
            
            if player.hasYellowcard() {
                cardView.isHidden = false
                cardView.backgroundColor = .systemYellow
            } else if player.hasRedCard() {
                cardView.isHidden = false
                cardView.backgroundColor = .systemRed
            }
        }
    }



}
