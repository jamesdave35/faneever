//
//  FirstTeamStatCell.swift
//  Faneever
//
//  Created by James Meli on 3/10/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import EZYGradientView

class FirstTeamStatCell: UITableViewCell {
    
    @IBOutlet weak var containerView: EZYGradientView!
    @IBOutlet weak var firstCountryLogo: UIImageView!
    @IBOutlet weak var secondCountryLogo: UIImageView!
    @IBOutlet weak var secondCountryName: UILabel!
    @IBOutlet weak var firstCountryName: UILabel!
    @IBOutlet weak var secondTeamName: UILabel!
    @IBOutlet weak var firstTeamLogo: UIImageView!
    @IBOutlet weak var firstTeamName: UILabel!
    @IBOutlet weak var secondTeamLogo: UIImageView!
    @IBOutlet weak var secondTeamStackView: UIStackView!
    
    var delegate: CompareTeamDelegate?
    var canChooseTeam: Bool?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.firstColor = Colors.compareTeams
        containerView.secondColor = Colors.secondaryBackground
        containerView.angleº = 0
        containerView.colorRatio = 0.3
        containerView.fadeIntensity = 1

        

        
    }
    
    var firstTeam: Team? {
        didSet {
            updateView()
        }
    }
    
    var secondTeam: Team? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        DispatchQueue.main.async {
            if let team = self.firstTeam {
                if let url = team.getLogoUrl() {
                    self.firstTeamLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
                }
                
                if let name = team.name {
                    self.firstTeamName.text = name
                }
                
                team.getCountry { [weak self] result in
                    switch result {
                    case .success(let country):
                        if let url = country.getLogoUrl() {
                            self?.firstCountryLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
                        }
                        
                        if let countryName = country.name {
                            self?.firstCountryName.text = countryName
                        }
                    case .failure(let error):
                        print("Error")
                    }
                }
                
              
            }
            
            if let team = self.secondTeam {
                if let url = team.getLogoUrl() {
                    self.secondTeamLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
                }
                
                if let name = team.name {
                    self.secondTeamName.text = name
                }
                
                
                team.getCountry { [weak self] result in
                    switch result {
                    case .success(let country):
                        if let url = country.getLogoUrl() {
                            self?.secondCountryLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
                        }
                        
                        if let countryName = country.name {
                            self?.secondCountryName.text = countryName
                        }
                    case .failure(let error):
                        print("Error")
                    }
                }

                
              
            }
        }
        
        if let canChoose = self.canChooseTeam {
            if canChoose {
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(secondStackViewSelected))
                secondTeamStackView.addGestureRecognizer(tapGesture)
            }
        }
    }
    
    
    @objc private func secondStackViewSelected() {
        delegate?.wantsToChangeTeam()
    }



}
