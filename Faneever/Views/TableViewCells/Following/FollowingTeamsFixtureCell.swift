//
//  FollowingTeamsFixtureCell.swift
//  Faneever
//
//  Created by James Meli on 3/28/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class FollowingTeamsFixtureCell: UITableViewCell {

    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var homeTeamImage: UIImageView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var awayTeamName: UILabel!
    @IBOutlet weak var homeTeamName: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateView: AnimatableView!
    @IBOutlet weak var containerView: AnimatableView!
    
    

    
    var fixture: Fixture? {
        didSet {
            updateView()
        }
    }
    
    
    private func updateView() {
        if let homeTeam = fixture?.getLocalTeam()?.name, let awayTeam = fixture?.getVisitorTeam()?.name {
            homeTeamName.text = homeTeam
            awayTeamName.text = awayTeam
            
        }
        
        if let homeTeamUrl = fixture?.getLocalTeam()?.getLogoUrl(), let awayTeamUrl = fixture?.getVisitorTeam()?.getLogoUrl() {
            homeTeamImage.sd_setImage(with: homeTeamUrl, placeholderImage: Constants.defaultTeamImage)
            awayTeamImage.sd_setImage(with: awayTeamUrl, placeholderImage: Constants.defaultTeamImage)
        }
        
        if let date = fixture?.getSchedule()?.getDateString(), let time = fixture?.getSchedule()?.getTimeString() {
            dateLabel.text = date
            timeLabel.text = time
        }
        

    }



}
