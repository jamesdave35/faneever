//
//  FixtureTeamCell.swift
//  Faneever
//
//  Created by James Meli on 3/28/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable
class FixtureTeamCell: UITableViewCell {

    @IBOutlet weak var containerView: AnimatableView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    

    
    override func prepareForReuse() {
        self.containerView.cornerRadius = 0
        
    }
    
    var team: Team? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let name = team?.name {
            teamName.text = name
        }
        
        if let url = team?.getLogoUrl() {
            teamImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
        }
    }



}
