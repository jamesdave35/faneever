//
//  TeamOverviewCell.swift
//  Faneever
//
//  Created by James Meli on 3/18/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class TeamOverviewCell: UITableViewCell {

    @IBOutlet weak var awayScore: PaddingLabel!
    @IBOutlet weak var localScore: PaddingLabel!
    @IBOutlet weak var liveLabel: PaddingLabel!
    @IBOutlet weak var gameMinute: PaddingLabel!
    @IBOutlet weak var nextGameLeague: PaddingLabel!
    @IBOutlet weak var secondStackView: UIStackView!
    @IBOutlet weak var firstStackView: UIStackView!
    @IBOutlet weak var containerView: AnimatableView!
    @IBOutlet weak var overViewLabel: UILabel!
    @IBOutlet weak var nextGameLabel: UILabel!
    @IBOutlet weak var nextGameDate: UILabel!
    @IBOutlet weak var nextGameHomeTeam: UILabel!
    @IBOutlet weak var nextGameHomeTeamLogo: UIImageView!
    @IBOutlet weak var nextGameTime: PaddingLabel!
    @IBOutlet weak var nextGameAwayTeamLogo: UIImageView!
    @IBOutlet weak var nextGameAwayTeam: UILabel!
    @IBOutlet weak var recentFormLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var delegate: FollowingDelegate?
    var timer: Timer?
    var nextGameFixture: Fixture? {
        didSet {
            updateView()
        }
    }
    
    var last5Games: [Fixture] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nextGameTime.fillColor = UIColor(named: "tertiaryBackground")
        nextGameLeague.fillColor = UIColor(named: "tertiaryBackground")
        gameMinute.fillColor = Colors.greenBrandColor
        gameMinute.cornerRadius = 12.5
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let tapGestureOne = UITapGestureRecognizer(target: self, action: #selector(fixtureSelected))
        let tapGestureTwo = UITapGestureRecognizer(target: self, action: #selector(fixtureSelected))
        let tapGestureThree = UITapGestureRecognizer(target: self, action: #selector(fixtureSelected))
        let goToLeagueGesture = UITapGestureRecognizer(target: self, action: #selector(goToLeague))
        firstStackView.addGestureRecognizer(tapGestureOne)
        secondStackView.addGestureRecognizer(tapGestureTwo)
        nextGameTime.addGestureRecognizer(tapGestureThree)
        nextGameLeague.addGestureRecognizer(goToLeagueGesture)
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(animateLabel), userInfo: nil, repeats: true)
        
    }
    
    private func updateView() {
        if let fix = nextGameFixture {
            if fix.isPostponed() {
                nextGameTime.text = "PP"
            } else {
                
                if let gameTime = fix.getSchedule()?.getTimeString() {
                    if !fix.hasStarted() {
                        nextGameTime.isHidden = false
                        nextGameTime.text = gameTime
                    } else {
                        localScore.isHidden = false
                        awayScore.isHidden = false
                        if let local = fix.getLocalScore(), let away = fix.getVisitorScore() {
                            if local == away {
                                localScore.fillColor = UIColor(named: "tertiaryBackground")
                                localScore.textColor = Colors.primaryText
                                awayScore.fillColor = UIColor(named: "tertiaryBackground")
                                awayScore.textColor = Colors.primaryText
                            } else if local > away {
                                localScore.fillColor = .systemBlue
                                localScore.textColor = .white
                                awayScore.fillColor = UIColor(named: "tertiaryBackground")
                                awayScore.textColor = Colors.primaryText
                            } else {
                                localScore.fillColor = UIColor(named: "tertiaryBackground")
                                localScore.textColor = Colors.primaryText
                                awayScore.fillColor = .systemBlue
                                awayScore.textColor = .white
                            }
                            
                            localScore.text = "\(local)"
                            awayScore.text = "\(away)"
                        }
                    }
                    
                }
            }
            if let min = fix.time?.minute, let status = fix.time?.status {
                if status == "LIVE" {
                    nextGameDate.isHidden = true
                    liveLabel.isHidden = false
                    gameMinute.isHidden = false
                    gameMinute.text = "\(min)'"

                } else if status == "FT" || status == "HT"{
                    gameMinute.isHidden = false
                    gameMinute.text = status
                }
                
            }
            if fix.isToday() {
                nextGameDate.text = "Today"
            } else {
                if let gameDate = fix.getSchedule()?.getDateString() {
                    nextGameDate.text = gameDate
                }
            }
            if let leagueName = fix.getLeagueName() {
                nextGameLeague.text = leagueName
            }
            
            if let localTeamName = fix.getLocalTeam()?.name, let localTeamURL = fix.getLocalTeam()?.getLogoUrl() {
                nextGameHomeTeam.text = localTeamName
                nextGameHomeTeamLogo.sd_setImage(with: localTeamURL, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let localTeamName = fix.getVisitorTeam()?.name, let localTeamURL = fix.getVisitorTeam()?.getLogoUrl() {
                nextGameAwayTeam.text = localTeamName
                nextGameAwayTeamLogo.sd_setImage(with: localTeamURL, placeholderImage: Constants.defaultTeamImage)
            }
        }
    }
    
    @objc private func fixtureSelected() {
        if let fixture = self.nextGameFixture {
            delegate?.didSelectFixture(fixture: fixture)
        }
    }
    
    @objc private func goToLeague() {
        if let league = self.nextGameFixture?.league?.data {
            delegate?.didSelectLeague(league: league)
        }
    }
    
    @objc private func animateLabel() {
        UIView.animate(withDuration: 1, animations: {
            self.liveLabel.alpha = 0.2
        }) { _ in
            UIView.animate(withDuration: 1) {
                self.liveLabel.alpha = 1
            }
        }
    }
    
    deinit {
        timer?.invalidate()
    }


}

extension TeamOverviewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return last5Games.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeamFormCell", for: indexPath) as! TeamFormCell
        cell.fixture = last5Games[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 57, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let fixture = last5Games[indexPath.row]
        delegate?.didSelectFixture(fixture: fixture)
        
    }
}


