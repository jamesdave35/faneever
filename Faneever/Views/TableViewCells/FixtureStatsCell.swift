//
//  FixtureStatsCell.swift
//  Faneever
//
//  Created by James Meli on 4/6/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class FixtureStatsCell: UITableViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var secondTeamStat: PaddingLabel!
    @IBOutlet weak var firstTeamStat: PaddingLabel!
    @IBOutlet weak var statName: PaddingLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.containerView.backgroundColor = Colors.secondaryBackground
        self.statName.fillColor = Colors.tertiaryBackgroundColor
        self.statName.textColor = Colors.secondaryText
        self.secondTeamStat.textColor = Colors.primaryText
        self.firstTeamStat.textColor = Colors.primaryText
    }
    
    var teamStat: StatComparison? {
        didSet {
            updateView()
        }
    }
    
    override func prepareForReuse() {
        resetContent()
    }
    
    func resetContent() {
        self.firstTeamStat.text = "-"
        self.firstTeamStat.fillColor = .clear
        self.secondTeamStat.text = "-"
        self.secondTeamStat.fillColor = .clear
        self.firstTeamStat.textColor = Colors.primaryText
        self.secondTeamStat.textColor = Colors.primaryText
    }
    
    
    private func updateView() {
        DispatchQueue.main.async {
            if let stat = self.teamStat {
                self.statName.text = stat.statName
                
                if let value1 = stat.firstValue {
                    if stat.statName == "Possession" {
                       self.firstTeamStat.text = "\(value1)%"
                    } else {
                       self.firstTeamStat.text = "\(value1)"
                    }
                    
                }
                
                if let value2 = stat.secondValue {
                    if stat.statName == "Possession" {
                        self.secondTeamStat.text = "\(value2)%"
                    } else {
                       self.secondTeamStat.text = "\(value2)"
                    }
                    
                }
                
                if let valueOne = stat.firstValue, let valueTwo = stat.secondValue {
                    if valueOne > valueTwo {
                        self.firstTeamStat.fillColor = .systemBlue
                        self.firstTeamStat.textColor = .white
                    } else if valueTwo > valueOne {
                        self.secondTeamStat.fillColor = .systemBlue
                        self.secondTeamStat.textColor = .white
                    }
                    

                }

            }
        }

    }

}
