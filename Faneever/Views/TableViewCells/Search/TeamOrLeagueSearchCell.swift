//
//  TeamOrLeagueSearchCell.swift
//  Faneever
//
//  Created by James Meli on 5/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamOrLeagueSearchCell: UITableViewCell {

    @IBOutlet weak var teamOrLeagueName: UILabel!
    @IBOutlet weak var teamOrLeagueLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    var team: Team? {
        didSet {
            if let name = team?.name {
                teamOrLeagueName.text = name
            }
            
            if let url = team?.getLogoUrl() {
                teamOrLeagueLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
            }
        }
    }
    
    var league: League? {
        didSet {
            if let name = league?.name {
                teamOrLeagueName.text = name
            }
            
            if let url = league?.getLogoURL() {
                teamOrLeagueLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
            }
        }
    }



}
