//
//  PlayerSearchCell.swift
//  Faneever
//
//  Created by James Meli on 5/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class PlayerSearchCell: UITableViewCell {

    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    var player: Player? {
        didSet {
            if let name = player?.display_name {
                playerName.text = name
            }
            
            if let url = player?.getImageURL() {
                playerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
        }
    }
    
    override func prepareForReuse() {
        playerName.text = ""
        playerImage.image = nil
    }



}
