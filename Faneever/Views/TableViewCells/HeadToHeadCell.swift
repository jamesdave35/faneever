//
//  HeadToHeadCell.swift
//  Faneever
//
//  Created by James Meli on 4/4/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class HeadToHeadCell: UITableViewCell {

    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var awayTeamScore: PaddingLabel!
    @IBOutlet weak var dash: UILabel!
    @IBOutlet weak var homeTeamScore: PaddingLabel!
    @IBOutlet weak var awayTeamName: UILabel!
    @IBOutlet weak var homeTeamName: UILabel!
    @IBOutlet weak var homeTeamImage: UIImageView!
    @IBOutlet weak var league: PaddingLabel!
    @IBOutlet weak var date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        date.textColor = Colors.secondaryText
        league.textColor = Colors.secondaryText
        league.fillColor = Colors.tertiaryBackgroundColor
        league.roundCorners()
        homeTeamName.textColor = Colors.primaryText
        awayTeamName.textColor = Colors.primaryText
        homeTeamScore.textColor = Colors.primaryText
        awayTeamScore.textColor = Colors.primaryText
        dash.textColor = Colors.primaryText
        homeTeamScore.cornerRadius = 15
        awayTeamScore.cornerRadius = 15
        
    }
    
    var fixture: Fixture? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let fix = fixture {
            if let dateString = fix.getSchedule()?.getSecondDateString() {
                date.text = dateString
            }
            
            if let leagueName = fix.getLeagueName() {
                league.text = leagueName
            }
            
            if let homeTeam = fix.getLocalTeam()?.name {
                homeTeamName.text = homeTeam
            }
            
            if let homeTeamLogoUrl = fix.getLocalTeam()?.getLogoUrl() {
                homeTeamImage.sd_setImage(with: homeTeamLogoUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let awayTeam = fix.getVisitorTeam()?.name {
                awayTeamName.text = awayTeam
            }
            
            if let awayTeamLogoUrl = fix.getVisitorTeam()?.getLogoUrl() {
                awayTeamImage.sd_setImage(with: awayTeamLogoUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let localScore = fix.getLocalScore(), let visitorScore = fix.getVisitorScore() {
                if localScore == visitorScore {
                    homeTeamScore.fillColor = Colors.tertiaryBackgroundColor
                    homeTeamScore.textColor = Colors.primaryText
                    awayTeamScore.fillColor = Colors.tertiaryBackgroundColor
                    awayTeamScore.textColor = Colors.primaryText
                } else if localScore > visitorScore {
                    homeTeamScore.fillColor = .systemBlue
                    homeTeamScore.textColor = .white
                    awayTeamScore.fillColor = Colors.tertiaryBackgroundColor
                    awayTeamScore.textColor = Colors.primaryText
                } else {
                    homeTeamScore.fillColor = Colors.tertiaryBackgroundColor
                    homeTeamScore.textColor = Colors.primaryText
                    awayTeamScore.fillColor = .systemBlue
                    awayTeamScore.textColor = .white
                }
                homeTeamScore.text = "\(localScore)"
                awayTeamScore.text = "\(visitorScore)"
            }
        }
    }



}
