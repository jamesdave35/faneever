//
//  FixtureStatsHeaderCell.swift
//  Faneever
//
//  Created by James Meli on 4/6/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class FixtureStatsHeaderCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = Colors.secondaryBackground
        headerLabel.textColor = Colors.primaryText
    }
    
    var header: String? {
        didSet {
            if let header = self.header {
                self.headerLabel.text = header
            }
            
        }
    }



}
