//
//  VideoPostCell.swift
//  Faneever
//
//  Created by James Meli on 5/10/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import SwiftVideoBackground
import IBAnimatable

class VideoPostCell: UITableViewCell {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var shareButton: UIImageView!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var likeButton: UIImageView!
    @IBOutlet weak var numberOfReplies: UILabel!
    @IBOutlet weak var replyButton: UIImageView!
    @IBOutlet weak var postMessage: UILabel!
    @IBOutlet weak var userTeam: UILabel!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var likeView: AnimatableView!
    @IBOutlet weak var shareView: AnimatableView!
    
    var videoPlayer = VideoBackground()
    var videoUrl: URL?
    var delegate: PostCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        videoView.layer.cornerRadius = 20
        videoView.layer.cornerCurve = .continuous
        
        let playVideoGesture = UITapGestureRecognizer(target: self, action: #selector(playVideo))
        videoView.addGestureRecognizer(playVideoGesture)
        
        let replyGesture = UITapGestureRecognizer(target: self, action: #selector(replyPost))
        replyView.addGestureRecognizer(replyGesture)
        
        let likeGesture = UITapGestureRecognizer(target: self, action: #selector(likePost))
        likeView.addGestureRecognizer(likeGesture)
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImage.sd_cancelCurrentImageLoad()
    }
    
    var post: Post? {
        didSet {
            updateView()
        }
    }
    
    func resetContent() {
        username.text = ""
        postMessage.text = ""
        timeStamp.text = ""
        likeButton.image = Constants.likeImage
        likeButton.tintColor = Colors.secondaryText
        numberOfLikes.isHidden = true
        numberOfReplies.isHidden = true
        profileImage.image = nil
    }
    
    private func updateView() {
        if let post = self.post {
            self.postMessage.text = post.message
            
            if let name = post.userName, let teamName = post.userTeamName {
                self.username.text = name
                self.userTeam.textColor = TeamColors.teamColors[teamName]
                self.userTeam.text = teamName
            }
            
            if let url = post.getUserImageURL() {
                self.profileImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
            
            if let url = post.getVideoURL() {
                self.videoUrl = url
                self.videoPlayer.play(view: self.videoView, url: url, darkness: 0, isMuted: true, willLoopVideo: true, setAudioSessionAmbient: true, preventsDisplaySleepDuringVideoPlayback: true)
            }
            
            DatabaseServices.getNumberOfPostReplies(post: post) { (number) in
                if let number = number {
                    if number > 0 {
                        self.numberOfReplies.isHidden = false
                        self.numberOfReplies.text = "\(number)"
                    }

                }
            }
            
            DatabaseServices.getNumberOfLikes(post: post) { (number) in
                if let number = number {
                    if number > 0 {
                        self.numberOfLikes.isHidden = false
                        self.numberOfLikes.text = "\(number)"
                    } else {
                        self.numberOfLikes.isHidden = true
                    }
                }
            }
            
            if let id = Constants.currentUserId {
                DatabaseServices.hasLikedPost(id: id, post: post) { (value) in
                    if value {
                        self.likeButton.image = Constants.likedImage
                        self.likeButton.tintColor = .systemRed
                    } else {
                        self.likeButton.image = Constants.likeImage
                        self.likeButton.tintColor = Colors.secondaryText
                    }
                }
            }
            
            self.timeStamp.text = post.timeStamp.timeAgoDisplay()
        }
    }
    
    @objc private func playVideo() {
        if let url = self.videoUrl {
            delegate?.didPlayVideo(videoUrl: url)
        }
        
    }
    
    @objc private func replyPost() {
        if let post = self.post {
            delegate?.wantsToReplyToPost(post: post)
        }
    }
    
    @objc private func likePost() {
        if let post = self.post, let id = Constants.currentUserId {
            GlobalFunctions.playSelectionFeedbackGenerator()
            DatabaseServices.hasLikedPost(id: id, post: post) { (value) in
                if value {
                    self.likeButton.image = Constants.likeImage
                    self.likeButton.tintColor = Colors.secondaryText
                    DatabaseServices.unLikePost(id: id, post: post)
                } else {
                    self.likeButton.image = Constants.likedImage
                    self.likeButton.tintColor = .systemRed
                    DatabaseServices.likePost(id: id, post: post)
                }
            }
        }
    }



}
