//
//  PlayerCompPostCell.swift
//  Faneever
//
//  Created by James Meli on 5/10/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import EZYGradientView
import IBAnimatable

class PlayerCompPostCell: UITableViewCell {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var seeStatsLabel: UILabel!
    @IBOutlet weak var vsLabel: PaddingLabel!
    @IBOutlet weak var secondCompareLabel: UILabel!
    @IBOutlet weak var secondCompareImage: UIImageView!
    @IBOutlet weak var firstCompareLabel: UILabel!
    @IBOutlet weak var firstCompareImage: UIImageView!
    @IBOutlet weak var compareView: EZYGradientView!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var shareButton: UIImageView!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var likeButton: UIImageView!
    @IBOutlet weak var numberOfReplies: UILabel!
    @IBOutlet weak var replyButton: UIImageView!
    @IBOutlet weak var postMessage: UILabel!
    @IBOutlet weak var userTeam: UILabel!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var likeView: AnimatableView!
    @IBOutlet weak var shareView: AnimatableView!
    
    var delegate: PostCellDelegate?
    var playerOne: Player?
    var playerTwo: Player?
    var statsDict: NSDictionary?

    override func awakeFromNib() {
        super.awakeFromNib()
        

        vsLabel.fillColor = UIColor(named: "tertiaryBackground")
        self.firstCompareImage.layer.cornerRadius = 32.5
        self.secondCompareImage.layer.cornerRadius = 32.5
        
        let seeStatsGesture = UITapGestureRecognizer(target: self, action: #selector(seeStats))
        compareView.addGestureRecognizer(seeStatsGesture)
        
        let replyGesture = UITapGestureRecognizer(target: self, action: #selector(replyPost))
        replyView.addGestureRecognizer(replyGesture)
        
        let likeGesture = UITapGestureRecognizer(target: self, action: #selector(likePost))
        likeView.addGestureRecognizer(likeGesture)
        
        configureGradient()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        firstCompareImage.sd_cancelCurrentImageLoad()
        secondCompareImage.sd_cancelCurrentImageLoad()
        profileImage.sd_cancelCurrentImageLoad()
    }
    
    var post: Post? {
        didSet {
            updateView()
        }
    }
    
    private func configureGradient() {
        compareView.firstColor = Colors.alternateComparePlayers
        compareView.secondColor = Colors.compareBackground
        compareView.angleº = 0
        compareView.colorRatio = 0.3
        compareView.fadeIntensity = 1
    }
    
    func resetContent() {
        username.text = ""
        postMessage.text = ""
        timeStamp.text = ""
        likeButton.image = Constants.likeImage
        likeButton.tintColor = Colors.secondaryText
        numberOfLikes.isHidden = true
        numberOfReplies.isHidden = true
        profileImage.image = nil
        shadowView.applyShadow(cornerRadius: 20, shadowOpacity: 0.2, radius: 15, offset: CGSize(width: 1, height: 3))

    }
    
    private func updateView() {
        if let post = post {
            self.postMessage.text = post.message
            if let name = post.userName, let teamName = post.userTeamName {
                self.username.text = name
                self.userTeam.textColor = TeamColors.teamColors[teamName]
                self.userTeam.text = teamName
            }
            
            if let url = post.getUserImageURL() {
                self.profileImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
            
            DatabaseServices.getNumberOfPostReplies(post: post) { (number) in
                if let number = number {
                    if number > 0 {
                        self.numberOfReplies.isHidden = false
                        self.numberOfReplies.text = "\(number)"
                    }

                }
            }
            
            DatabaseServices.getNumberOfLikes(post: post) { (number) in
                if let number = number {
                    if number > 0 {
                        self.numberOfLikes.isHidden = false
                        self.numberOfLikes.text = "\(number)"
                    } else {
                        self.numberOfLikes.isHidden = true
                    }
                }
            }
            
            if let id = Constants.currentUserId {
                DatabaseServices.hasLikedPost(id: id, post: post) { (value) in
                    if value {
                        self.likeButton.image = Constants.likedImage
                        self.likeButton.tintColor = .systemRed
                    } else {
                        self.likeButton.image = Constants.likeImage
                        self.likeButton.tintColor = Colors.secondaryText
                    }
                }
            }
            
            if let dict = post.playerComparisonDict {
                if let firstPlayer = dict["firstPlayer"] as? NSDictionary, let firstPlayerName = firstPlayer["playerName"] as? String, let secondPlayer = dict["secondPlayer"] as? NSDictionary, let secondPlayerName = secondPlayer["playerName"] as? String, let stats = dict["stats"] as? NSDictionary {
                    self.playerOne = Player(firebaseData: firstPlayer)
                    self.playerTwo = Player(firebaseData: secondPlayer)
                    self.statsDict = stats
                    self.firstCompareLabel.text = firstPlayerName
                    self.secondCompareLabel.text = secondPlayerName
                    
                    if let firstPlayerUrlString = firstPlayer["imageURL"] as? String {
                        let url = URL(string: firstPlayerUrlString)
                        self.firstCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                    }
                    
                    if let secondPlayerUrlString = secondPlayer["imageURL"] as? String {
                        let url = URL(string: secondPlayerUrlString)
                        self.secondCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                    }
                    self.shadowView.isHidden = false
                    self.compareView.isHidden = false
                    self.seeStatsLabel.isHidden = false
                }
            }
            
            self.timeStamp.text = post.timeStamp.timeAgoDisplay()
        }
    }
    
    @objc private func replyPost() {
        if let post = self.post {
            delegate?.wantsToReplyToPost(post: post)
        }
    }
    
    @objc private func likePost() {
        if let post = self.post, let id = Constants.currentUserId {
            GlobalFunctions.playSelectionFeedbackGenerator()
            DatabaseServices.hasLikedPost(id: id, post: post) { (value) in
                if value {
                    self.likeButton.image = Constants.likeImage
                    self.likeButton.tintColor = Colors.secondaryText
                    DatabaseServices.unLikePost(id: id, post: post)
                } else {
                    self.likeButton.image = Constants.likedImage
                    self.likeButton.tintColor = .systemRed
                    DatabaseServices.likePost(id: id, post: post)
                }
            }
        }
    }
    
    @objc private func seeStats() {
        if let post = self.post {
            if let firstPlayer = self.playerOne, let secondPlayer = self.playerTwo, let stats = self.statsDict {
                GlobalFunctions.playSelectionFeedbackGenerator()
                let convertStatsDict = GlobalFunctions.convertStatDictToListOfStats(stats: stats)
                delegate?.didSelectPlayerStats(firstPlayer: firstPlayer, secondPlayer: secondPlayer, statsDict: convertStatsDict, indexPath: self.indexPath!)
            }
        }

    }



}
