//
//  LineUpCell.swift
//  Faneever
//
//  Created by James Meli on 4/5/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class LineUpCell: UITableViewCell {

    
    @IBOutlet weak var awayTeamName: PaddingLabel!
    @IBOutlet weak var homeTeamName: PaddingLabel!
    @IBOutlet weak var homeFormationLabel: PaddingLabel!
    @IBOutlet weak var awayFormationLabel: PaddingLabel!
    @IBOutlet weak var secondTableView: UITableView!
    @IBOutlet weak var awayBoxView: AnimatableView!
    @IBOutlet weak var awayPitchView: UIView!
    @IBOutlet weak var homePitchView: UIView!
    @IBOutlet weak var playerTableView: UITableView!
    @IBOutlet weak var homeBoxView: AnimatableView!
    
    var numberOfHomeCells = 0
    var numberOfAwayCells = 0
    var homePlayers = [LineupPlayer]()
    var awayPlayers = [LineupPlayer]()
    var homeFormSections = [String]()
    var awayFormSections = [String]()
    var previousNum = 0
    var homeCount = 1
    var awayCount = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        homeBoxView.backgroundColor = Colors.pithcViewColor
        awayBoxView.backgroundColor = Colors.pithcViewColor
        awayPitchView.backgroundColor = Colors.pithcViewColor
        homePitchView.backgroundColor = Colors.pithcViewColor
        playerTableView.tag = 1
        secondTableView.tag = 2
        playerTableView.delegate = self
        playerTableView.dataSource = self
        secondTableView.delegate = self
        secondTableView.dataSource = self
    }
    
    override func prepareForReuse() {
        homeCount = 1
        awayCount = 0
    }
    
    var fixture: Fixture? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let fix = fixture {
            if let homeTeam = fix.getLocalTeam()?.short_code, let awayTeam = fix.getVisitorTeam()?.short_code {
                homeTeamName.text = homeTeam
                awayTeamName.text = awayTeam
                
            }
            
            if let homeTeamFormation = fix.formations?.localteam_formation, let homeTeamPlayers = fix.getHomeTeamPlayersLineup(), let awayTeamFormation = fix.formations?.visitorteam_formation, let awayTeamPlayers = fix.getAwayTeamPlayersLineup() {
                homeFormationLabel.text = homeTeamFormation
                awayFormationLabel.text = awayTeamFormation
                homePlayers = homeTeamPlayers
                awayPlayers = awayTeamPlayers
                if homeTeamFormation == "4-2-2-2" {
                    homeFormSections = ["0", "4", "4", "2"]
                } else {
                    homeFormSections = homeTeamFormation.components(separatedBy: "-")
                    homeFormSections.insert("0", at: 0)
                }
                if awayTeamFormation == "4-2-2-2" {
                    awayFormSections = ["2", "4", "4", "0"]
                } else {
                    let awaySec = awayTeamFormation.components(separatedBy: "-")
                    for index in stride(from: awaySec.count - 1, through: 0, by: -1) {
                        awayFormSections.append(awaySec[index])
                    }
                    awayFormSections.append("0")
                }

                numberOfHomeCells = homeFormSections.count
                numberOfAwayCells = awayFormSections.count
                playerTableView.reloadData()
                secondTableView.reloadData()
                
            }
        }
    }



}

extension LineUpCell: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return numberOfHomeCells
        } else {
            return numberOfAwayCells
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FormationRowCell", for: indexPath) as! FormationRowCell
            if !homeFormSections.isEmpty && !homePlayers.isEmpty {
                if let num = Int(homeFormSections[indexPath.row]) {
                    if num == 0 {
                        cell.players = [homePlayers[0]]
                    } else {
                        let i = num - 1
                        if homeCount + i < homePlayers.count {
                            cell.players = Array(homePlayers[homeCount...i + homeCount])
                        }
                        
                        homeCount = homeCount + num
                    }
                }
            }
            return cell
        } else {
            let cell = secondTableView.dequeueReusableCell(withIdentifier: "SecondFormationRowCell", for: indexPath) as! SecondFormationRowCell
            if !awayFormSections.isEmpty && !awayPlayers.isEmpty {
                if let num = Int(awayFormSections[indexPath.row]) {
                    if num == 0 {
                        cell.players = [awayPlayers[awayPlayers.count - 1]]
                    } else {
                        let i = num - 1
                        if awayCount + i < awayPlayers.count {
                            cell.players = Array(awayPlayers[awayCount...i + awayCount])
                        }
                        
                        awayCount = awayCount + num
                    }
                }
            }
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1 {
            if homeFormSections.count < 5 {
                return 130
            } else {
               return 100
            }
        } else {
            if awayFormSections.count < 5 {
                return 130
            } else {
               return 100
            }
        }

        
    }
}
