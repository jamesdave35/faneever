//
//  PlayerSeasonHeaderCell.swift
//  Faneever
//
//  Created by James Meli on 5/11/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class PlayerSeasonHeaderCell: UITableViewCell {

    @IBOutlet weak var headerName: UILabel!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var holderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = Colors.secondaryBackground
        holderView.backgroundColor = Colors.tertiaryBackgroundColor
        headerName.textColor = Colors.primaryText
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        headerName.text = ""
        headerImage.image = nil
    }
    
    var header: String? {
        didSet {
            if let header = self.header {
                headerName.text = header
                switch header {
                case "TOP STATS":
                    headerImage.image = UIImage(named: "top-stats")
                case "DISTRIBUTION":
                    headerImage.image = UIImage(named: "distribute")
                case "TAKE ONS":
                    headerImage.image = UIImage(named: "arrow")
                case "DEFENSE":
                    headerImage.image = UIImage(named: "shield")
                case "PENALTIES":
                    headerImage.image = UIImage(named: "penalty")
                case "SAVES":
                    headerImage.image = UIImage(named: "save")
                default :
                    headerImage.image = UIImage(named: "whistle")
                }
            }
        }
    }

}
