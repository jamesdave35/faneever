//
//  PlayerSeasonLeagueCell.swift
//  Faneever
//
//  Created by James Meli on 5/11/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class PlayerSeasonLeagueCell: UITableViewCell {

    @IBOutlet weak var leagueName: UILabel!
    @IBOutlet weak var leagueLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = Colors.secondaryBackground
        leagueName.textColor = Colors.primaryText
    }
    
    var league: League? {
        didSet {
            if let name = league?.name {
                leagueName.text = name
                
            }
            
            if let url = league?.getLogoURL() {
                leagueLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
            }
        }
    }

}
