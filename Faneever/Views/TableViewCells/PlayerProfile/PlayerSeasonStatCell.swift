//
//  PlayerSeasonStatCell.swift
//  Faneever
//
//  Created by James Meli on 5/11/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class PlayerSeasonStatCell: UITableViewCell {

    @IBOutlet weak var statValue: PaddingLabel!
    @IBOutlet weak var statName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = Colors.secondaryBackground
        statName.textColor = Colors.secondaryText
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        statName.text = ""
        statValue.text = "-"
    }

    var stat: StatDisplay? {
        didSet {
            statName.text = stat?.statName
            if let value = stat?.statValue {
                statValue.text = "\(value)"
            }
        }
    }



}
