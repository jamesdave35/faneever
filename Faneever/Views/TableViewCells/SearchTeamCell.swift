//
//  SearchTeamCell.swift
//  Faneever
//
//  Created by James Meli on 3/10/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class SearchTeamCell: UITableViewCell {
    
    
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.teamName.textColor = Colors.primaryText
    }
    
    override func prepareForReuse() {

    }

    
    var team: Team? {
        didSet {
            updateView()
        }
    }
    
    func resetContent() {
        teamLogo.image = Constants.defaultTeamImage
        teamName.text = ""
    }
    
    private func updateView() {
        
        if let url = team?.getLogoUrl() {
            self.teamLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
        }
        
        if let name = team?.name {
            self.teamName.text = name
        }
    }



}
