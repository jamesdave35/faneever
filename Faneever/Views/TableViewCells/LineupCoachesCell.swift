//
//  LineupCoachesCell.swift
//  Faneever
//
//  Created by James Meli on 4/6/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class LineupCoachesCell: UITableViewCell {

    @IBOutlet weak var teamName: PaddingLabel!
    @IBOutlet weak var coachName: UILabel!
    @IBOutlet weak var coachImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = Colors.secondaryBackground
        coachName.textColor = Colors.primaryText
        teamName.fillColor = Colors.tertiaryBackgroundColor
        teamName.textColor = Colors.secondaryText
    }
    
    override func prepareForReuse() {
        coachImage.image = UIImage(named: "coach")
    }
    
    
    
    var coach: FixtureAndCoach? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let coach = coach {
            if let name = coach.coach.fullname {
                self.coachName.text = name
            }
            
            if let url = coach.coach.getImageURL() {
                coachImage.sd_setImage(with: url, placeholderImage: UIImage(named: "coach"))
            }
            
            if let name = coach.teamName {
                teamName.text = name
            }
        }
    }


}

struct FixtureAndCoach {
    var teamName: String?
    var coach: Coach
}
