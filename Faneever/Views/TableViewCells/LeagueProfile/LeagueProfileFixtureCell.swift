//
//  LeagueProfileFixtureCell.swift
//  Faneever
//
//  Created by James Meli on 5/12/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class LeagueProfileFixtureCell: UITableViewCell {

    @IBOutlet weak var scoreStackView: UIStackView!
    @IBOutlet weak var fixtureTime: PaddingLabel!
    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var awayTeamScore: PaddingLabel!
    @IBOutlet weak var dash: UILabel!
    @IBOutlet weak var homeTeamScore: PaddingLabel!
    @IBOutlet weak var awayTeamName: UILabel!
    @IBOutlet weak var homeTeamName: UILabel!
    @IBOutlet weak var homeTeamImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = Colors.secondaryBackground
        fixtureTime.textColor = Colors.secondaryText
        fixtureTime.fillColor = Colors.tertiaryBackgroundColor
        fixtureTime.cornerRadius = 12.5
        homeTeamName.textColor = Colors.primaryText
        awayTeamName.textColor = Colors.primaryText
        homeTeamScore.textColor = Colors.primaryText
        awayTeamScore.textColor = Colors.primaryText
        dash.textColor = Colors.primaryText
        
    }
    
    override func prepareForReuse() {
        scoreStackView.isHidden = true
        fixtureTime.isHidden = true
        homeTeamImage.image = nil
        awayTeamImage.image = nil
        homeTeamName.text = ""
        awayTeamName.text = ""
        homeTeamName.text = ""
        awayTeamName.text = ""
    }
    
    var fixture: Fixture? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let fix = fixture {
            if fix.hasPassed() && !fix.isPostponed() {
                self.scoreStackView.isHidden = false
            } else {
                self.fixtureTime.isHidden = false
            }
            if let time = fix.getSchedule()?.getTimeString() {
                fixtureTime.text = time
            }
            
            
            if let homeTeam = fix.getLocalTeam()?.name {
                homeTeamName.text = homeTeam
            }
            
            if let homeTeamLogoUrl = fix.getLocalTeam()?.getLogoUrl() {
                homeTeamImage.sd_setImage(with: homeTeamLogoUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let awayTeam = fix.getVisitorTeam()?.name {
                awayTeamName.text = awayTeam
            }
            
            if let awayTeamLogoUrl = fix.getVisitorTeam()?.getLogoUrl() {
                awayTeamImage.sd_setImage(with: awayTeamLogoUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let localScore = fix.getLocalScore(), let visitorScore = fix.getVisitorScore() {
                if localScore == visitorScore {
                    homeTeamScore.fillColor = Colors.tertiaryBackgroundColor
                    homeTeamScore.textColor = Colors.primaryText
                    awayTeamScore.fillColor = Colors.tertiaryBackgroundColor
                    awayTeamScore.textColor = Colors.primaryText
                } else if localScore > visitorScore {
                    homeTeamScore.fillColor = .systemBlue
                    homeTeamScore.textColor = .white
                    awayTeamScore.fillColor = Colors.tertiaryBackgroundColor
                    awayTeamScore.textColor = Colors.primaryText
                } else {
                    homeTeamScore.fillColor = Colors.tertiaryBackgroundColor
                    homeTeamScore.textColor = Colors.primaryText
                    awayTeamScore.fillColor = .systemBlue
                    awayTeamScore.textColor = .white
                }
                homeTeamScore.text = "\(localScore)"
                awayTeamScore.text = "\(visitorScore)"
            }
        }
    }



}
