//
//  AllTopPlayersFirstCell.swift
//  Faneever
//
//  Created by James Meli on 5/14/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import EZYGradientView

class AllTopPlayersFirstCell: UITableViewCell {

    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var gradientView: EZYGradientView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        firstName.textColor = Colors.primaryText
        lastName.textColor = Colors.primaryText
        teamName.textColor = Colors.secondaryText
        value.textColor = Colors.primaryText
        
        configureGradientView()
        
    }
    
    var topScorer: AggregatedGoalScorers? {
        didSet {
            updateView()
        }
    }
    
    var topAssist: AggregatedAssitScorers? {
        didSet {
            updateView()
        }
    }
    
    var topYellowCard: AggregatedCardScorers? {
        didSet {
            updateView()
        }
    }
    
    var player: TopPlayerDisplay?
    
    private func updateView() {
        
        if let topScorer = self.topScorer {
            player = TopPlayerDisplay(value: topScorer.goals, team: topScorer.team?.data, player: topScorer.player?.data, position: topScorer.position)
        } else if let topAssist = self.topAssist {
            player = TopPlayerDisplay(value: topAssist.assists,team: topAssist.team?.data, player: topAssist.player?.data, position: topAssist.position)
        } else if let topCard = self.topYellowCard {
            player = TopPlayerDisplay(value: topCard.yellowcards,team: topCard.team?.data, player: topCard.player?.data, position: topCard.position)
        }
        
        if let name = player?.player?.display_name {
            let names = name.components(separatedBy: " ")
            if names.count == 2 {
                firstName.text = names[0]
                lastName.text = names[1]
            } else if names.count == 1 {
                firstName.text = ""
                lastName.text = names[0]
            } else {
                firstName.text = names[0]
                lastName.text = "\(names[1]) \(names[2])"
            }
        }
        
        if let name = player?.team?.name, let url = player?.team?.getLogoUrl() {
            teamName.text = name
            teamImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
        }
        
        if let statValue = player?.value {
            value.text = "\(statValue)"
        }
        
        if let url = player?.player?.getImageURL() {
            playerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
        }
    }
    
    private func configureGradientView() {
          let firstColor: UIColor = {
            if #available(iOS 13, *) {
                return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                    if UITraitCollection.userInterfaceStyle == .dark {
                        /// Return the color for Dark Mode
                        return UIColor(hexString: "2D2D2D")
                    } else {
                        /// Return the color for Light Mode
                        return UIColor(hexString: "DFDFDF")
                    }
                }
            } else {
                /// Return a fallback color for iOS 12 and lower.
                return UIColor(hexString: "202020")
            }
        }()
        
        gradientView.firstColor = Colors.secondaryBackground
        gradientView.secondColor = firstColor
        gradientView.angleº = 45
        gradientView.fadeIntensity = 1
        gradientView.colorRatio = 0.5
        

    }



}
