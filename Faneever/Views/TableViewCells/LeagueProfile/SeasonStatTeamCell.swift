//
//  SeasonStatTeamCell.swift
//  Faneever
//
//  Created by James Meli on 5/14/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class SeasonStatTeamCell: UITableViewCell {
    
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var rank: UILabel!
    @IBOutlet weak var statValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.backgroundColor = Colors.secondaryBackground
        rank.textColor = Colors.secondaryText
        teamName.textColor = Colors.primaryText
        statValue.textColor = Colors.primaryText
    }
    
    var statName: String?
    var position: Int?
    var seasonId: Int!
    
    var teamStat: TeamStats? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let name = teamStat?.name {
            teamName.text = name
        }
        
        if let pos = self.position {
            rank.text = "\(pos)"
        }
        
        if let url = teamStat?.getLogoURL() {
            teamImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
        }
        
        if let name = statName {
            switch name {
            case "Goals Scored":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getGoalsScored() {
                    statValue.text = "\(value)"
                }
            case "Avg Goals/Game":
                if let value = teamStat?.getRequiredSeason(id: seasonId).avgGoalsPerGame() {
                    statValue.text = "\(value)"
                }
            case "Goals Conceded":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getGoalsConceded() {
                    statValue.text = "\(value)"
                }
            case "Wins":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getWins() {
                    statValue.text = "\(value)"
                }
            case "Draws":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getDraws() {
                    statValue.text = "\(value)"
                }
            case "Lost":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getLosses() {
                    statValue.text = "\(value)"
                }
            case "Avg Possesion/Game":
                if let value = teamStat?.getRequiredSeason(id: seasonId).avgPossession() {
                    statValue.text = "\(value)"
                }
            case "Avg Shots on Target/Game":
                if let value = teamStat?.getRequiredSeason(id: seasonId).avgShotsOnTarget() {
                    statValue.text = "\(value)"
                }
            case "Cleen Sheets":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getCleanSheets() {
                    statValue.text = "\(value)"
                }
            case "Corners":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getCorners() {
                    statValue.text = "\(value)"
                }
            case "Offsides":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getOffsides() {
                    statValue.text = "\(value)"
                }
            case "Fouls":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getFouls() {
                    statValue.text = "\(value)"
                }
            case "Yellow Cards":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getYellowCards() {
                    statValue.text = "\(value)"
                }
            default:
                if let value = teamStat?.getRequiredSeason(id: seasonId).getRedCards() {
                    statValue.text = "\(value)"
                }
                
                
                
            }
        }
    }



}
