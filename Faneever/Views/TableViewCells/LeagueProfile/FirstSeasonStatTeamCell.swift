//
//  FirstSeasonStatTeamCell.swift
//  Faneever
//
//  Created by James Meli on 5/14/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import EZYGradientView

class FirstSeasonStatTeamCell: UITableViewCell {

    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var statValue: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var statLabel: UILabel!
    @IBOutlet weak var gradientView: EZYGradientView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        statLabel.textColor = Colors.primaryText
        teamName.textColor = Colors.primaryText
        statValue.textColor = Colors.primaryText
        configureGradientView()
       
    }
    
    var statName: String?
    var seasonId: Int!
    
    var teamStat: TeamStats? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let name = statName {
            statLabel.text = name
            
            if let tName = teamStat?.name {
                teamName.text = tName
            }
            
            if let url = teamStat?.getLogoURL() {
                teamImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
            }
            
            switch name {
            case "Goals Scored":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getGoalsScored() {
                    statValue.text = "\(value)"
                }
            case "Avg Goals/Game":
                if let value = teamStat?.getRequiredSeason(id: seasonId).avgGoalsPerGame() {
                    statValue.text = "\(value)"
                }
            case "Goals Conceded":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getGoalsConceded() {
                    statValue.text = "\(value)"
                }
            case "Wins":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getWins() {
                    statValue.text = "\(value)"
                }
            case "Draws":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getDraws() {
                    statValue.text = "\(value)"
                }
            case "Lost":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getLosses() {
                    statValue.text = "\(value)"
                }
            case "Avg Possesion/Game":
                if let value = teamStat?.getRequiredSeason(id: seasonId).avgPossession() {
                    statValue.text = "\(value)"
                }
            case "Avg Shots on Target/Game":
                if let value = teamStat?.getRequiredSeason(id: seasonId).avgShotsOnTarget() {
                    statValue.text = "\(value)"
                }
            case "Cleen Sheets":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getCleanSheets() {
                    statValue.text = "\(value)"
                }
            case "Corners":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getCorners() {
                    statValue.text = "\(value)"
                }
            case "Offsides":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getOffsides() {
                    statValue.text = "\(value)"
                }
            case "Fouls":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getFouls() {
                    statValue.text = "\(value)"
                }
            case "Yellow Cards":
                if let value = teamStat?.getRequiredSeason(id: seasonId).getYellowCards() {
                    statValue.text = "\(value)"
                }
            default:
                if let value = teamStat?.getRequiredSeason(id: seasonId).getRedCards() {
                    statValue.text = "\(value)"
                }
                
                
                
            }
        }
    }
    
    private func configureGradientView() {
          let firstColor: UIColor = {
            if #available(iOS 13, *) {
                return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                    if UITraitCollection.userInterfaceStyle == .dark {
                        /// Return the color for Dark Mode
                        return UIColor(hexString: "2D2D2D")
                    } else {
                        /// Return the color for Light Mode
                        return UIColor(hexString: "DFDFDF")
                    }
                }
            } else {
                /// Return a fallback color for iOS 12 and lower.
                return UIColor(hexString: "202020")
            }
        }()
        
        gradientView.firstColor = Colors.secondaryBackground
        gradientView.secondColor = firstColor
        gradientView.angleº = 45
        gradientView.fadeIntensity = 1
        gradientView.colorRatio = 0.5
        

    }



}
