//
//  LeagueTeamStatsCell.swift
//  Faneever
//
//  Created by James Meli on 5/14/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class LeagueTeamStatsCell: UITableViewCell {

    @IBOutlet weak var teamsTableView: UITableView!
    
    var delegate: LeagueProfileDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        teamsTableView.delegate = self
        teamsTableView.dataSource = self
        
    }
    
    var stats: [TeamStats]? {
        didSet {
            teamsTableView.reloadData()
        }
    }
    
    var seasonId: Int!
    
    var statName: String?



}

extension LeagueTeamStatsCell: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = teamsTableView.dequeueReusableCell(withIdentifier: "FirstSeasonStatTeamCell", for: indexPath) as! FirstSeasonStatTeamCell
            cell.seasonId = seasonId
            cell.statName = self.statName
            if let stats = self.stats {
               cell.teamStat = stats[indexPath.row]
            }
            
            return cell
        } else {
            let cell = teamsTableView.dequeueReusableCell(withIdentifier: "SeasonStatTeamCell", for: indexPath) as! SeasonStatTeamCell
            cell.seasonId = seasonId
            cell.statName = self.statName
            cell.position = indexPath.row
            if let stats = self.stats {
               cell.teamStat = stats[indexPath.row]
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 150
        } else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let stats = stats {
            if let id = stats[indexPath.row].id {
                delegate?.didSelectTeam!(teamId: id)
            }
        }
        
    }
    
 
}


