//
//  AllTopPlayersCell.swift
//  Faneever
//
//  Created by James Meli on 5/14/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class AllTopPlayersCell: UITableViewCell {

    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var rank: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = Colors.secondaryBackground
        rank.textColor = Colors.secondaryText
        playerName.textColor = Colors.primaryText
        teamName.textColor = Colors.secondaryText
        value.textColor = Colors.primaryText
       
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        rank.text = ""
        playerName.text = ""
        playerImage.image = nil
        teamImage.image = nil
        teamName.text = ""
        value.text = ""
    }
    
    var topScorer: AggregatedGoalScorers? {
        didSet {
            updateView()
        }
    }
    
    var topAssist: AggregatedAssitScorers? {
        didSet {
            updateView()
        }
    }
    
    var topYellowCard: AggregatedCardScorers? {
        didSet {
            updateView()
        }
    }
    
    var player: TopPlayerDisplay?
    
    private func updateView() {
        
        if let topScorer = self.topScorer {
            player = TopPlayerDisplay(value: topScorer.goals, team: topScorer.team?.data, player: topScorer.player?.data, position: topScorer.position)
        } else if let topAssist = self.topAssist {
            player = TopPlayerDisplay(value: topAssist.assists,team: topAssist.team?.data, player: topAssist.player?.data, position: topAssist.position)
        } else if let topCard = self.topYellowCard {
            player = TopPlayerDisplay(value: topCard.yellowcards,team: topCard.team?.data, player: topCard.player?.data, position: topCard.position)
        }
        
        if let pos = player?.position {
            rank.text = "\(pos)"
        }
        
        if let url = player?.player?.getImageURL() {
            playerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
        }
        
        if let name = player?.player?.display_name {
            playerName.text = name
        }
        
        if let name = player?.team?.name, let url = player?.team?.getLogoUrl() {
            teamImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
            teamName.text = name
            
        }
        
        if let statValue = player?.value {
            value.text = "\(statValue)"
        }
        
    }



}
