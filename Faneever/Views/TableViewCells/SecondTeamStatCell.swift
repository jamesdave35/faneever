//
//  SecondTeamStatCell.swift
//  Faneever
//
//  Created by James Meli on 3/11/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class SecondTeamStatCell: UITableViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var secondTeamStat: PaddingLabel!
    @IBOutlet weak var firstTeamStat: PaddingLabel!
    @IBOutlet weak var statName: PaddingLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.statName.fillColor = UIColor(named: "tertiaryBackground")
        
    }
    
    var teamStat: StatComparison? {
        didSet {
            updateView()
        }
    }
    
    override func prepareForReuse() {

    }
    
    func resetContent() {
        self.firstTeamStat.text = "-"
        self.firstTeamStat.fillColor = .clear
        self.secondTeamStat.text = "-"
        self.secondTeamStat.fillColor = .clear
        self.firstTeamStat.textColor = UIColor(named: "primaryText")
        self.secondTeamStat.textColor = UIColor(named: "primaryText")
    }
    
    
    private func updateView() {
        DispatchQueue.main.async {
            if let stat = self.teamStat {
                self.statName.text = stat.statName
                
                if let value1 = stat.firstValue {
                    self.firstTeamStat.text = "\(value1)"
                }
                
                if let value2 = stat.secondValue {
                    self.secondTeamStat.text = "\(value2)"
                }
                
                if let valueOne = stat.firstValue, let valueTwo = stat.secondValue {
                    if valueOne > valueTwo {
                        self.firstTeamStat.fillColor = .systemBlue
                        self.firstTeamStat.textColor = .white
                    } else if valueTwo > valueOne {
                        self.secondTeamStat.fillColor = .systemBlue
                        self.secondTeamStat.textColor = .white
                    }
                    

                }

            }
        }

    }

}
