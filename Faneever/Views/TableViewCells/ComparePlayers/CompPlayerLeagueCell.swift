//
//  CompPlayerLeagueCell.swift
//  Faneever
//
//  Created by James Meli on 5/31/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class CompPlayerLeagueCell: UITableViewCell {

    @IBOutlet weak var secondStackView: UIStackView!
    @IBOutlet weak var firstStackView: UIStackView!
    @IBOutlet weak var firstPlayerLeagueImage: UIImageView!
    @IBOutlet weak var firstPlayerLeagueName: UILabel!
    @IBOutlet weak var firstPlayerLeagueSeason: UILabel!
    @IBOutlet weak var secondPlayerLeagueName: UILabel!
    @IBOutlet weak var secondPlayerLeagueImage: UIImageView!
    @IBOutlet weak var secondPlayerLeagueSeason: UILabel!
    
    var firstPlayerId: Int?
    var secondPlayerId: Int?
    
    var delegate: ComparePlayerDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGestureOne = UITapGestureRecognizer(target: self, action: #selector(changeFirstPlayerSeason))
        let tapGestureTwo = UITapGestureRecognizer(target: self, action: #selector(changeFirstPlayerSeason))
        let tapGestureThree = UITapGestureRecognizer(target: self, action: #selector(changeSecondPlayerSeason))
        let tapGestureFour = UITapGestureRecognizer(target: self, action: #selector(changeSecondPlayerSeason))
        firstStackView.addGestureRecognizer(tapGestureOne)
        firstPlayerLeagueSeason.addGestureRecognizer(tapGestureTwo)
        secondStackView.addGestureRecognizer(tapGestureThree)
        secondPlayerLeagueSeason.addGestureRecognizer(tapGestureFour)
        
    }
    
    var firstPlayerLeague: League? {
        didSet {
            if let leagueUrl = firstPlayerLeague?.getLogoURL() {
                firstPlayerLeagueImage.sd_setImage(with: leagueUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let leagueName = firstPlayerLeague?.name {
                firstPlayerLeagueName.text = leagueName
            }
        }
    }
    
    var firstPlayerSeason: Season? {
        didSet {
            if let seasonName = firstPlayerSeason?.name {
                firstPlayerLeagueSeason.text = seasonName
            }
        }
    }
    
    var secondPlayerLeague: League? {
        didSet {
            if let leagueUrl = secondPlayerLeague?.getLogoURL() {
                secondPlayerLeagueImage.sd_setImage(with: leagueUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let leagueName = secondPlayerLeague?.name {
                secondPlayerLeagueName.text = leagueName
            }
        }
    }
    
    var secondPlayerSeason: Season? {
        didSet {
            if let seasonName = secondPlayerSeason?.name {
                secondPlayerLeagueSeason.text = seasonName
            }
        }
    }
    
    @objc private func changeFirstPlayerSeason() {
        if let id = firstPlayerId {
            delegate?.wantsToChangeFirstPlayerSeason(playerId: id)
        }
    }
    
    @objc private func changeSecondPlayerSeason() {
        if let id = secondPlayerId {
             delegate?.wantsToChangeSecondPlayerSeason(playerId: id)
         }
    }
    

}
