//
//  PlayerCompSearchCell.swift
//  Faneever
//
//  Created by James Meli on 3/5/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class PlayerCompSearchCell: UITableViewCell {

    @IBOutlet weak var playerPhoto: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    
    
    
    

    
    var player: Player? {
        didSet {
            updateView()
        }
    }
    
    func resetContent() {
        playerPhoto.image = UIImage(named: "default-face")
    }
    
    private func updateView() {
        
        if let url = player?.getImageURL() {
            self.playerPhoto.sd_setImage(with: url, placeholderImage: UIImage(named: "default-face"))
        }
        
        if let name = player?.display_name {
            self.playerName.text = name
        }
    }
    
    
    



}
