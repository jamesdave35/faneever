//
//  SecondStatCell.swift
//  Faneever
//
//  Created by James Meli on 3/9/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class SecondStatCell: UITableViewCell {
    
    @IBOutlet weak var secondContainerView: UIView!
    @IBOutlet weak var secondPlayerWeight: UILabel!
    @IBOutlet weak var firstPlayerWeight: UILabel!
    @IBOutlet weak var weightLabel: PaddingLabel!
    @IBOutlet weak var secondPlayerHeight: UILabel!
    @IBOutlet weak var heightLabel: PaddingLabel!
    @IBOutlet weak var firstPlayerHeight: UILabel!
    @IBOutlet weak var secondPlayerAge: UILabel!
    @IBOutlet weak var ageLabel: PaddingLabel!
    @IBOutlet weak var firstPlayerAge: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.ageLabel.fillColor = UIColor(named: "tertiaryBackground")
        self.heightLabel.fillColor = UIColor(named: "tertiaryBackground")
        self.weightLabel.fillColor = UIColor(named: "tertiaryBackground")

    }
    
    var firstPlayer: Player? {
        didSet {
            updateView()
        }
    }
    
    var secondPlayer: Player? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        DispatchQueue.main.async {
            if let player = self.firstPlayer {
                if let height = player.height, let weight = player.weight {
                    self.firstPlayerWeight.text = weight
                    self.firstPlayerHeight.text = height
                    
                }
                
                if let age = player.getPlayerAge() {
                    self.firstPlayerAge.text = "\(age)"
                } else if let age = player.age {
                    self.firstPlayerAge.text = "\(age)"
                }
            }
            
            if let player = self.secondPlayer {
                if let height = player.height, let weight = player.weight {
                    self.secondPlayerWeight.text = weight
                    self.secondPlayerHeight.text = height
                   
                }
                
                if let age = player.getPlayerAge() {
                     self.secondPlayerAge.text = "\(age)"
                } else if let age = player.age {
                     self.secondPlayerAge.text = "\(age)"
                }
            }
        }

    }
    



}
