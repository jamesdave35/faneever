//
//  FirstStatCell.swift
//  Faneever
//
//  Created by James Meli on 3/9/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import EZYGradientView
import IBAnimatable

class FirstStatCell: UITableViewCell {
    
    @IBOutlet weak var secondPlayerStackView: UIStackView!
    @IBOutlet weak var secondPlayerTeamName: PaddingLabel!
    @IBOutlet weak var secondPlayerTeamImage: UIImageView!
    @IBOutlet weak var secondPlayerName: UILabel!
    @IBOutlet weak var secondPlayerPhoto: UIImageView!
    @IBOutlet weak var firstPlayerTeamName: PaddingLabel!
    @IBOutlet weak var firstPlayerTeamImage: UIImageView!
    @IBOutlet weak var firstContainerView: EZYGradientView!
    @IBOutlet weak var firstPlayerPhoto: UIImageView!
    @IBOutlet weak var firstPlayerName: UILabel!
    
    var delegate: ComparePlayerDelegate?
    
    var canChoosePlayer: Bool?

    override func awakeFromNib() {
        super.awakeFromNib()
       
        setGradientViewColors()
        self.contentView.layer.masksToBounds = true
        self.firstPlayerTeamName.fillColor = UIColor(named: "tertiaryBackground")
        self.secondPlayerTeamName.fillColor = UIColor(named: "tertiaryBackground")

    
    }
    
    var firstPlayer: Player? {
        didSet {
            updateView()
        }
    }
    
    var secondPlayer: Player? {
        didSet {
            updateView()
        }
    }
    
    private func setGradientViewColors() {
        firstContainerView.firstColor = Colors.comparePlayers
        firstContainerView.secondColor = Colors.secondaryBackground
        firstContainerView.angleº = 0
        firstContainerView.colorRatio = 0.3
        firstContainerView.fadeIntensity = 1
    }
    
    private func updateView() {
        DispatchQueue.main.async {
            if let player = self.firstPlayer {
                if let url = player.getImageURL() {
                    self.firstPlayerPhoto.sd_setImage(with: url, placeholderImage: UIImage(named: "default-face"))
                }
                
                if let name = player.display_name {
                    self.firstPlayerName.text = name
                }
                
                player.getTeam { [weak self] result in
                    switch result {
                    case .success(let team):
                        if let url = team?.getLogoUrl() {
                            self?.firstPlayerTeamImage.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
                        }
                        
                        if let teamName = team?.name {
                            DispatchQueue.main.async {
                                self?.firstPlayerTeamName.text = teamName
                            }
                            
                        }
                        
                    case .failure(let error):
                        print("Error")
                    }
                }
                
                
            }
            
            if let player = self.secondPlayer {
                if let url = player.getImageURL() {
                    self.secondPlayerPhoto.sd_setImage(with: url, placeholderImage: UIImage(named: "default-face"))
                }
                
                if let name = player.display_name {
                    self.secondPlayerName.text = name
                }
                
                 player.getTeam { [weak self] result in
                    switch result {
                    case .success(let team):
                        if let url = team?.getLogoUrl() {
                            self?.secondPlayerTeamImage.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
                        }
                        
                        if let teamName = team?.name {
                            DispatchQueue.main.async {
                                self?.secondPlayerTeamName.text = teamName
                            }
                            
                        }
                        
                    case .failure(let error):
                        print("Error")
                    }
                }
                

            }
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(secondStackViewSelected))
        if let canChoose = self.canChoosePlayer {
            if canChoose {
                secondPlayerStackView.addGestureRecognizer(tapGesture)
            }
        }

    }
    
    @objc private func secondStackViewSelected() {
        delegate?.wantsToChangePlayer()
    }
    


}
