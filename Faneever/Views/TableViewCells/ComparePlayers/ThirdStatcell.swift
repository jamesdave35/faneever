//
//  ThirdStatcell.swift
//  Faneever
//
//  Created by James Meli on 3/9/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class ThirdStatcell: UITableViewCell {
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var secondPlayerStat: PaddingLabel!
    @IBOutlet weak var firstPlayerStat: PaddingLabel!
    @IBOutlet weak var statName: PaddingLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.statName.fillColor = UIColor(named: "tertiaryBackground")

    }
    
    var playerStat: StatComparison? {
        didSet {
            updateView()
        }
    }
    
    override func prepareForReuse() {

    }
    
    func resetContent() {
        self.firstPlayerStat.text = "-"
        self.firstPlayerStat.fillColor = .clear
        self.secondPlayerStat.text = "-"
        self.secondPlayerStat.fillColor = .clear
        self.firstPlayerStat.textColor = UIColor(named: "primaryText")
        self.secondPlayerStat.textColor = UIColor(named: "primaryText")
    }
    
    
    private func updateView() {
        DispatchQueue.main.async {
            if let stat = self.playerStat {
                self.statName.text = stat.statName
                
                if let value1 = stat.firstValue {
                    self.firstPlayerStat.text = "\(value1)"
                }
                
                if let value2 = stat.secondValue {
                    self.secondPlayerStat.text = "\(value2)"
                }
                
                if let valueOne = stat.firstValue, let valueTwo = stat.secondValue {
                    if valueOne > valueTwo {
                        self.firstPlayerStat.fillColor = .systemBlue
                        self.firstPlayerStat.textColor = .white
                    } else if valueTwo > valueOne {
                        self.secondPlayerStat.fillColor = .systemBlue
                        self.secondPlayerStat.textColor = .white
                    }
                    

                }

            }
        }

    }
    



}
