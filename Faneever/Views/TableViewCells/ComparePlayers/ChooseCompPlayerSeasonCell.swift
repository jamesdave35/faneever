//
//  ChooseCompPlayerSeasonCell.swift
//  Faneever
//
//  Created by James Meli on 5/31/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class ChooseCompPlayerSeasonCell: UITableViewCell {

    @IBOutlet weak var leagueImage: UIImageView!
    @IBOutlet weak var leagueName: UILabel!
    @IBOutlet weak var seasonName: UILabel!
    
    
    var playerLeague: League? {
        didSet {
            if let leagueUrl = playerLeague?.getLogoURL() {
                leagueImage.sd_setImage(with: leagueUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let name = playerLeague?.name {
                leagueName.text = name
            }
        }
    }
    
    var playerSeason: Season? {
        didSet {
            if let name = playerSeason?.name {
               seasonName.text = name
            }
        }
    }

}
