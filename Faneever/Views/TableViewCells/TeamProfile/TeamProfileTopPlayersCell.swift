//
//  TeamProfileTopPlayersCell.swift
//  Faneever
//
//  Created by James Meli on 4/22/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamProfileTopPlayersCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    var team: Team?
    
    var topPlayers: [TopPlayerCategory] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    

}


extension TeamProfileTopPlayersCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topPlayers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeamProfileBestPlayersCell", for: indexPath) as! TeamProfileBestPlayersCell
        cell.team = team
        cell.topPlayers = topPlayers[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 250, height: 300)
    }
    
    
    
    
}
