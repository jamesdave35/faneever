//
//  TeamSquadCell.swift
//  Faneever
//
//  Created by James Meli on 4/23/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamSquadCell: UITableViewCell {
    
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.backgroundColor = Colors.secondaryBackground
        playerName.textColor = Colors.primaryText
        countryName.textColor = Colors.secondaryText
        
    }
    
    override func prepareForReuse() {
        playerImage.image = nil
        playerName.text = ""
        countryFlag.image = nil
        countryName.text = ""
    }
    
    var player: PlayerStatNumbers? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let player = player {
            if let name = player.player?.data?.display_name {
                playerName.text = name
            }
            
            if let url = player.player?.data?.getImageURL() {
                playerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
            
            if let country = player.player?.data?.getCountryName() {
                countryName.text = country
            }
            
            if let url = player.player?.data?.getCountryFlagUrl() {
                countryFlag.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
            }
        }
    }
    


}
