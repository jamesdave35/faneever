//
//  TeamProfileStandingsCell.swift
//  Faneever
//
//  Created by James Meli on 5/7/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamProfileStandingsCell: UITableViewCell {

    @IBOutlet weak var rankView: UIView!
    @IBOutlet weak var numberOfPoints: UILabel!
    @IBOutlet weak var numberLost: UILabel!
    @IBOutlet weak var numberWon: UILabel!
    @IBOutlet weak var numberOfDraws: UILabel!
    @IBOutlet weak var numberOfGames: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var rankNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        rankNumber.textColor = Colors.secondaryText
        teamName.textColor = Colors.primaryText
        numberWon.textColor = Colors.primaryText
        numberOfDraws.textColor = Colors.primaryText
        numberLost.textColor = Colors.primaryText
        numberOfGames.textColor = Colors.primaryText
        numberOfPoints.textColor = Colors.primaryText
    }
    
    override func prepareForReuse() {
        teamName.text = ""
        teamImage.image = nil
        numberOfPoints.text = ""
        numberOfGames.text = ""
        numberWon.text = ""
        numberLost.text = ""
        numberOfDraws.text = ""
        rankView.backgroundColor = .clear
    }
    
    var standing: Standings? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let standing = self.standing {
            if let rank = standing.position, let name = standing.team_name, let url = standing.team?.data?.getLogoUrl() {
                rankNumber.text = "\(rank)"
                teamName.text = "\(name)"
                teamImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let played = standing.overall?.games_played, let won = standing.overall?.won, let lost = standing.overall?.lost, let draws = standing.overall?.draw, let points = standing.total?.points {
                numberOfGames.text = "\(played)"
                numberWon.text = "\(won)"
                numberLost.text = "\(lost)"
                numberOfDraws.text = "\(draws)"
                numberOfPoints.text = "\(points)"
            }
            
            if standing.isChampionsLeagueQualified() {
                rankView.backgroundColor = .systemGreen
            } else if standing.isEuropaLeagueQualified() {
                rankView.backgroundColor = .systemYellow
            } else if standing.isInRelegationZone() {
                rankView.backgroundColor = .systemRed
            }
        }
    }



}
