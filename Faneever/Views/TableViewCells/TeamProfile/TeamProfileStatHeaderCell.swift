//
//  TeamProfileStatHeaderCell.swift
//  Faneever
//
//  Created by James Meli on 5/7/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamProfileStatHeaderCell: UITableViewCell {

    @IBOutlet weak var headerName: UILabel!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var holderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = Colors.secondaryBackground
        holderView.backgroundColor = Colors.tertiaryBackgroundColor
        headerName.textColor = Colors.primaryText
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        headerName.text = ""
        headerImage.image = nil
    }
    
    var header: String? {
        didSet {
            if let header = self.header {
                headerName.text = header
                switch header {
                case "TOP STATS":
                    headerImage.image = UIImage(named: "top-stats")
                case "GAMES":
                    headerImage.image = UIImage(named: "soccer-net")
                case "GOALS":
                    headerImage.image = UIImage(named: "soccer-ball")
                case "OFFENSE":
                    headerImage.image = UIImage(named: "arrow")
                case "DEFENSE":
                    headerImage.image = UIImage(named: "shield")
                default :
                    headerImage.image = UIImage(named: "whistle")
                }
            }
        }
    }

    

}
