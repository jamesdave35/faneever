//
//  TeamOverviewStandingCell.swift
//  Faneever
//
//  Created by James Meli on 4/22/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class TeamOverviewStandingCell: UITableViewCell {

    @IBOutlet weak var containerView: AnimatableView!
    @IBOutlet weak var leagueLogo: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var delegate: TeamProfileDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.backgroundColor = Colors.secondaryBackground
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    var leagueLogoURL: URL? {
        didSet {
            updateView()
        }
    }
    
    var standings: [Standings] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    private func updateView() {
        if let url = leagueLogoURL {
            leagueLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
        }
    }

}

extension TeamOverviewStandingCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return standings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeamLeagueStandingCell", for: indexPath) as! TeamLeagueStandingCell
        if indexPath.row == 0 {
            cell.standing = TeamPos(standing: standings[indexPath.row], flag: true)
        } else {
            cell.standing = TeamPos(standing: standings[indexPath.row], flag: false)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
           return CGSize(width: collectionView.frame.width, height: 65)
        } else {
            return CGSize(width: collectionView.frame.width, height: 40)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! TeamLeagueStandingCell
        if let teamId = cell.standing?.standing?.team_id {
            delegate?.didSelectTeam(teamId: teamId)
        }
    }
}
