//
//  FormationRowCell.swift
//  Faneever
//
//  Created by James Meli on 4/5/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class FormationRowCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    var players: [LineupPlayer] = [] {
        didSet {
            updateView()
        }
    }
    
    override func prepareForReuse() {
        players = []
    }
    
    private func updateView() {
        collectionView.reloadData()
    }



}

extension FormationRowCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return players.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayerLineupCell", for: indexPath) as! PlayerLineupCell
        cell.player = players[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if players.count > 3 && players.count < 5 {
            return CGSize(width: 75, height: 115)
        } else if players.count == 5  {
            return CGSize(width: 60, height: 115)
        } else {
            return CGSize(width: 95, height: 115)
        }
        
    }
    
    func centerItemsInCollectionView(cellWidth: Double, numberOfItems: Double, spaceBetweenCell: Double, collectionView: UICollectionView) -> UIEdgeInsets {
        let totalWidth = cellWidth * numberOfItems
        let totalSpacingWidth = spaceBetweenCell * (numberOfItems - 1)
        let leftInset = (collectionView.frame.width - CGFloat(totalWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if players.count > 3 && players.count < 5 {
            return centerItemsInCollectionView(cellWidth: 75, numberOfItems: Double(players.count), spaceBetweenCell: 15, collectionView: collectionView)
        } else if players.count == 5 {
            return centerItemsInCollectionView(cellWidth: 60, numberOfItems: Double(players.count), spaceBetweenCell: 5, collectionView: collectionView)
        } else {
            return centerItemsInCollectionView(cellWidth: 95, numberOfItems: Double(players.count), spaceBetweenCell: 15, collectionView: collectionView)
        }


    }


}
