//
//  PostCell.swift
//  Faneever
//
//  Created by James Meli on 3/2/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import SwiftVideoBackground
import EZYGradientView
import IBAnimatable

class PostCell: UITableViewCell {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var seeStatsLabel: UILabel!
    @IBOutlet weak var vsLabel: PaddingLabel!
    @IBOutlet weak var secondCompareLabel: UILabel!
    @IBOutlet weak var secondCompareImage: UIImageView!
    @IBOutlet weak var firstCompareLabel: UILabel!
    @IBOutlet weak var firstCompareImage: UIImageView!
    @IBOutlet weak var compareView: EZYGradientView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var timeStamp: UILabel!
    @IBOutlet weak var stackViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var shareButton: UIImageView!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var likeButton: UIImageView!
    @IBOutlet weak var numberOfReplies: UILabel!
    @IBOutlet weak var replyButton: UIImageView!
    @IBOutlet weak var postMessage: UILabel!
    @IBOutlet weak var userTeam: UILabel!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var likeView: AnimatableView!
    @IBOutlet weak var shareView: AnimatableView!
    
    var videoPlayer = VideoBackground()
    var videoUrl: URL?
    var delegate: PostCellDelegate?
    var playerOne: Player?
    var playerTwo: Player?
    var teamOne: Team?
    var teamTwo: Team?
    var statsDict: NSDictionary?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.backgroundColor = Colors.secondaryBackground
        shadowView.backgroundColor = .clear
        username.textColor = Colors.primaryText
        dotView.backgroundColor = Colors.secondaryText
        timeStamp.textColor = Colors.secondaryText
        postMessage.textColor = Colors.primaryText
        replyView.backgroundColor = Colors.tertiaryBackgroundColor
        likeView.fillColor = Colors.tertiaryBackgroundColor
        shareView.fillColor = Colors.tertiaryBackgroundColor
        replyButton.tintColor = Colors.secondaryText
        likeButton.tintColor = Colors.secondaryText
        shareButton.tintColor = Colors.secondaryText
        //likeView.borderColor = Colors.subtitleLabel
        numberOfLikes.textColor = Colors.secondaryText
        numberOfReplies.textColor = Colors.secondaryText
        postImage.layer.cornerRadius = 20
        videoView.layer.cornerRadius = 20
        postImage.layer.cornerCurve = .continuous
        videoView.layer.cornerCurve = .continuous
        compareView.layer.cornerCurve = .continuous
        
        secondCompareLabel.textColor = Colors.primaryText
        firstCompareLabel.textColor = Colors.primaryText
        vsLabel.fillColor = Colors.tertiaryBackgroundColor
        vsLabel.textColor = Colors.secondaryText
        seeStatsLabel.textColor = Colors.secondaryText
        
        let playVideoGesture = UITapGestureRecognizer(target: self, action: #selector(playVideo))
        videoView.addGestureRecognizer(playVideoGesture)
        
        let seeStatsGesture = UITapGestureRecognizer(target: self, action: #selector(seeStats))
        compareView.addGestureRecognizer(seeStatsGesture)
        
        let replyGesture = UITapGestureRecognizer(target: self, action: #selector(replyPost))
        replyButton.addGestureRecognizer(replyGesture)
        
        let likeGesture = UITapGestureRecognizer(target: self, action: #selector(likePost))
        likeButton.addGestureRecognizer(likeGesture)
        
        
    }
    
    

    
    override func prepareForReuse() {
        super.prepareForReuse()
         prepareCellForReuse()
    }
    
    private func configurePlayerGradient() {
        compareView.firstColor = Colors.alternateComparePlayers
        compareView.secondColor = Colors.compareBackground
        compareView.angleº = 0
        compareView.colorRatio = 0.3
        compareView.fadeIntensity = 1
    }
    
    private func configureTeamGradient() {
        compareView.firstColor = Colors.alternateCompareTeams
        compareView.secondColor = Colors.compareBackground
        compareView.angleº = 0
        compareView.colorRatio = 0.3
        compareView.fadeIntensity = 1
    }
    

    
    var post: Post? {
        didSet {
            updateView()
        }
    }
        
    
    
    func resetContent() {
        username.text = ""
        postMessage.text = ""
        timeStamp.text = ""
        shadowView.isHidden = true
        postImage.isHidden = true
        videoView.isHidden = true
        compareView.isHidden = true
        seeStatsLabel.isHidden = true
        likeButton.image = Constants.likeImage
        likeButton.tintColor = Colors.secondaryText
        numberOfLikes.isHidden = true
        stackViewTopConstraint.constant = -155
        configurePlayerGradient()
        firstCompareImage.layer.cornerRadius = 32.5
        secondCompareImage.layer.cornerRadius = 32.5
        numberOfReplies.isHidden = true
        profileImage.image = nil
        shadowView.applyShadow(cornerRadius: 20, shadowOpacity: 0.2, radius: 8.5, offset: CGSize(width: 1, height: 3))

    }
    
    private func prepareCellForReuse() {
        firstCompareImage.sd_cancelCurrentImageLoad()
        secondCompareImage.sd_cancelCurrentImageLoad()
        postImage.sd_cancelCurrentImageLoad()
        profileImage.sd_cancelCurrentImageLoad()
        firstCompareImage.image = nil
        secondCompareImage.image = nil
        postImage.image = nil
    }
    
    func updateView() {
        if let post = self.post {
            self.postMessage.text = post.message
            if post.hasImage() {
                self.shadowView.isHidden = false
                self.postImage.isHidden = false
                self.stackViewTopConstraint.constant = 28
                if let url = post.getImageURL() {
                self.postImage.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
                }
            } else if post.hasVideo() {
                self.shadowView.isHidden = false
                self.videoView.isHidden = false
                self.stackViewTopConstraint.constant = 28
                if let url = post.getVideoURL() {
                    self.videoUrl = url
                    self.videoPlayer.play(view: self.videoView, url: url, darkness: 0, isMuted: true, willLoopVideo: true, setAudioSessionAmbient: true, preventsDisplaySleepDuringVideoPlayback: true)
                }
            } else {
                self.stackViewTopConstraint.constant = -155
            
            }
            
            if let name = post.userName, let teamName = post.userTeamName {
                self.username.text = name
                self.userTeam.textColor = TeamColors.teamColors[teamName]
                self.userTeam.text = teamName
            }
            
            if let url = post.getUserImageURL() {
                self.profileImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
            
            
//            DatabaseServices.getUserWithId(id: post.userId) { (user) in
//                if let user = user {
//                    self.username.text = user.displayName
//                        if let url = URL(string: user.profileImageUrl) {
//                            self.profileImage.sd_setImage(with: url, placeholderImage: UIImage(named: "default-profile"))
//                        }
//                    if user.favoriteTeam == "Arsenal" || user.favoriteTeam == "Liverpool" {
//                        self.userTeam.textColor = .systemRed
//                    } else {
//                        self.userTeam.textColor = .systemBlue
//                    }
//                        self.userTeam.text = user.favoriteTeam
//                    }
//             }
            
            DatabaseServices.getNumberOfPostReplies(post: post) { (number) in
                if let number = number {
                    if number > 0 {
                        self.numberOfReplies.isHidden = false
                        self.numberOfReplies.text = "\(number)"
                    }

                }
            }
            
            DatabaseServices.getNumberOfLikes(post: post) { (number) in
                if let number = number {
                    if number > 0 {
                        self.numberOfLikes.isHidden = false
                        self.numberOfLikes.text = "\(number)"
                    } else {
                        self.numberOfLikes.isHidden = true
                    }
                }
            }
            
            if let id = Constants.currentUserId {
                DatabaseServices.hasLikedPost(id: id, post: post) { (value) in
                    if value {
                        self.likeButton.image = Constants.likedImage
                        self.likeButton.tintColor = .systemRed
                    } else {
                        self.likeButton.image = Constants.likeImage
                        self.likeButton.tintColor = Colors.secondaryText
                    }
                }
            }
            
            
            if post.isPlayerComparison() {
                self.configurePlayerGradient()
                self.stackViewTopConstraint.constant = 35
                if let dict = post.playerComparisonDict {
                    if let firstPlayer = dict["firstPlayer"] as? NSDictionary, let firstPlayerName = firstPlayer["playerName"] as? String, let secondPlayer = dict["secondPlayer"] as? NSDictionary, let secondPlayerName = secondPlayer["playerName"] as? String, let stats = dict["stats"] as? NSDictionary {
                        self.playerOne = Player(firebaseData: firstPlayer)
                        self.playerTwo = Player(firebaseData: secondPlayer)
                        self.statsDict = stats
                        self.firstCompareLabel.text = firstPlayerName
                        self.secondCompareLabel.text = secondPlayerName
                        
                        if let firstPlayerUrlString = firstPlayer["imageURL"] as? String {
                            let url = URL(string: firstPlayerUrlString)
                            self.firstCompareImage.layer.cornerRadius = 32.5
                            self.firstCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let secondPlayerUrlString = secondPlayer["imageURL"] as? String {
                            let url = URL(string: secondPlayerUrlString)
                            self.secondCompareImage.layer.cornerRadius = 32.5
                            self.secondCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        self.shadowView.isHidden = false
                        self.compareView.isHidden = false
                        self.seeStatsLabel.isHidden = false
                    }
                }
            
            } else if post.isTeamComparison() {
                self.configureTeamGradient()
                
                self.stackViewTopConstraint.constant = 35
                if let dict = post.teamComparisonDict {
                    if let firstTeam = dict["firstTeam"] as? NSDictionary, let firstTeamName = firstTeam["name"] as? String, let secondTeam = dict["secondTeam"] as? NSDictionary, let secondTeamName = secondTeam["name"] as? String, let stats = dict["stats"] as? NSDictionary {
                        self.teamOne = Team(firebaseData: firstTeam)
                        self.teamTwo = Team(firebaseData: secondTeam)
                        self.statsDict = stats
                        self.firstCompareLabel.text = firstTeamName
                        self.secondCompareLabel.text = secondTeamName
                        
                        if let firstTeamUrlString = firstTeam["teamLogo"] as? String {
                            let url = URL(string: firstTeamUrlString)
                            self.firstCompareImage.layer.cornerRadius = 0
                            self.firstCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
                        }
                        
                        if let secondTeamUrlString = secondTeam["teamLogo"] as? String {
                            let url = URL(string: secondTeamUrlString)
                            self.secondCompareImage.layer.cornerRadius = 0
                            self.secondCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
                        }
                        self.shadowView.isHidden = false
                        self.compareView.isHidden = false
                        self.seeStatsLabel.isHidden = false
                    }
                }
            }
               
                
            self.timeStamp.text = post.timeStamp.timeAgoDisplay()
            
        }

    }
    
    @objc private func playVideo() {
        if let url = self.videoUrl {
            delegate?.didPlayVideo(videoUrl: url)
        }
        
    }
    
    @objc private func replyPost() {
        if let post = self.post {
            delegate?.wantsToReplyToPost(post: post)
        }
    }
    
    @objc private func likePost() {
        if let post = self.post, let id = Constants.currentUserId {
            GlobalFunctions.playSelectionFeedbackGenerator()
            DatabaseServices.hasLikedPost(id: id, post: post) { (value) in
                if value {
                    self.likeButton.image = Constants.likeImage
                    self.likeButton.tintColor = Colors.secondaryText
                    DatabaseServices.unLikePost(id: id, post: post)
                } else {
                    self.likeButton.image = Constants.likedImage
                    self.likeButton.tintColor = .systemRed
                    DatabaseServices.likePost(id: id, post: post)
                }
            }
        }
    }
    
    @objc private func seeStats() {
        if let post = self.post {
            if post.isPlayerComparison() {
                if let firstPlayer = self.playerOne, let secondPlayer = self.playerTwo, let stats = self.statsDict {
                    GlobalFunctions.playSelectionFeedbackGenerator()
                    let convertStatsDict = GlobalFunctions.convertStatDictToListOfStats(stats: stats)
                    delegate?.didSelectPlayerStats(firstPlayer: firstPlayer, secondPlayer: secondPlayer, statsDict: convertStatsDict, indexPath: self.indexPath!)
                }
            } else {
                if let firstTeam = self.teamOne, let secondTeam = self.teamTwo, let stats = self.statsDict {
                    GlobalFunctions.playSelectionFeedbackGenerator()
                    let convertStatsDict = GlobalFunctions.convertStatDictToListOfStats(stats: stats)
                    delegate?.didSelectTeamStats(firstTeam: firstTeam, secondTeam: secondTeam, statsDict: convertStatsDict)
                }
            }
        }

    }
    

    

}
