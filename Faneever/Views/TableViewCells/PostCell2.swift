//
//  PostCell2.swift
//  Faneever
//
//  Created by James Meli on 3/3/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class PostCell2: UITableViewCell {

    @IBOutlet weak var message: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
//    open override func didMoveToSuperview() {
//        self.contentView.layoutIfNeeded()
//        self.containerView.layoutIfNeeded()
//    }
    
    override func prepareForReuse() {
        message.text = ""
    }
    
    var post: Post? {
        didSet {
            //updateView()
        }
    }
    
    func updateView(post: Post) {
        self.message.text = post.message
        
    }



}
