//
//  PostTypeCell.swift
//  Faneever
//
//  Created by James Meli on 3/1/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class PostTypeCell: UICollectionViewCell {
    
    @IBOutlet weak var postTypeLabel: PaddingLabel!
    
    
    override func awakeFromNib() {
        postTypeLabel.fillColor = Colors.secondaryBackground
        postTypeLabel.textColor = UIColor(named: "secondaryText")
        postTypeLabel.shadowColor = .black
        postTypeLabel.shadowOffset = .zero
        postTypeLabel.roundCorners()
    }
    
    var postType: String? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        postTypeLabel.text = postType!
    }
    
    func selectCell() {
        self.postTypeLabel.fillColor = .systemBlue
        self.postTypeLabel.textColor = .white
    }
    
    func unSelectCell() {
        postTypeLabel.fillColor = Colors.secondaryBackground
        postTypeLabel.textColor = UIColor(named: "secondaryText")
    }
}
