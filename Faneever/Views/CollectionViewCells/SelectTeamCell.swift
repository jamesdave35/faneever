//
//  SelectTeamCell.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import SDWebImage
import IBAnimatable

class SelectTeamCell: UICollectionViewCell {
    
    @IBOutlet weak var cellView: AnimatableView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamImage: UIImageView!
    
    override func awakeFromNib() {
        self.cellView.layer.cornerCurve = .continuous
    }
    
    var team: Team? {
        didSet {
            updateCell()
        }
    }
    
    override func prepareForReuse() {
        //
    }
    
    func resetContent() {
        self.cellView.fillColor = .clear
        self.cellView.borderColor = UIColor(hexString: "DFDFDF")
        self.teamName.textColor = .darkGray
        self.teamName.text = ""
        self.teamImage.image = nil
    }
    
    private func updateCell() {
        DispatchQueue.main.async {
            if let name = self.team?.name {
                self.teamName.text = name
            }
            
            if let urlString = self.team?.logo_path {
                let url = URL(string: urlString)
                self.teamImage.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            }
        }

    }
    
    func selectCell() {
        UIView.animate(withDuration: 0.5) {
            self.cellView.fillColor = Colors.greenBrandColor
            self.teamName.textColor = .white
            self.cellView.borderColor = .clear
        
        }
    }
    
    func unSelectCell() {
        self.cellView.fillColor = .clear
        self.cellView.borderColor = UIColor(hexString: "DFDFDF")
        self.teamName.textColor = .darkGray
       
    }
}
