//
//  TeamProfileMenuCell.swift
//  Faneever
//
//  Created by James Meli on 4/21/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamProfileMenuCell: UICollectionViewCell {
    
    @IBOutlet weak var menuLabel: PaddingLabel!
    
    override func awakeFromNib() {
        menuLabel.fillColor = .clear
        menuLabel.textColor = Colors.profilesMenu
        menuLabel.borderColor = Colors.profilesMenu
        menuLabel.borderWidth = 1
        menuLabel.cornerRadius = 17
    }
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                selectCell()
            } else {
                unSelectCell()
            }
        }
    }
    
    var menu: String? {
        didSet {
            updateView()
        }
    }
    
    var team: Team?
    
    private func updateView() {
        menuLabel.text = menu!
    }
    
    func selectCell() {
        self.menuLabel.fillColor = Colors.profilesMenu
        if let teamName = team?.name {
              let color: UIColor = {
                if #available(iOS 13, *) {
                    return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                        if UITraitCollection.userInterfaceStyle == .dark {
                            /// Return the color for Dark Mode
                            return UIColor.white
                        } else {
                            /// Return the color for Light Mode
                            return TeamColors.teamColors[teamName]!.darker(by: 10)
                        }
                    }
                } else {
                    /// Return a fallback color for iOS 12 and lower.
                    return UIColor.white
                }
            }()
            self.menuLabel.textColor = color
        }
        
    }
    
    func unSelectCell() {
        self.menuLabel.fillColor = .clear
        self.menuLabel.textColor = Colors.profilesMenu
    }
}
