//
//  TeamProfileLast5FixtureCell.swift
//  Faneever
//
//  Created by James Meli on 4/22/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamProfileLast5FixtureCell: UICollectionViewCell {
    
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var gameScore: PaddingLabel!
    
    var fixture: Fixture? {
        didSet {
            updateView()
        }
    }
    
    var team: Team?
    
    func updateView() {
        if let fixture = self.fixture {
            if let id = team?.id {
                if fixture.isTeamHomeTeam(teamId: id) {
                    if let url = fixture.getVisitorTeam()?.getLogoUrl() {
                        teamLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
                    }
                } else {
                    if let url = fixture.getLocalTeam()?.getLogoUrl() {
                        teamLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
                    }
                }
                
                switch fixture.teamMatchResult(teamId: id) {
                case "won":
                    gameScore.borderColor = .systemGreen
                    gameScore.textColor = .systemGreen
                case "lost":
                    gameScore.borderColor = .systemRed
                    gameScore.textColor = .systemRed
                default :
                    gameScore.borderColor = Colors.secondaryText
                    gameScore.textColor = Colors.secondaryText
                
                }
                
            }
            
            gameScore.text = fixture.getScoreAsString()
        }
    }
    
}
