//
//  SearchMenuCell.swift
//  Faneever
//
//  Created by James Meli on 5/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class SearchMenuCell: UICollectionViewCell {
    
    var menuLabel = PaddingLabel()

    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                menuLabel.fillColor = Colors.greenBrandColor
                menuLabel.textColor = .white
            } else {
                menuLabel.fillColor = .clear
                menuLabel.textColor = Colors.greenBrandColor
                menuLabel.borderWidth = 1
                menuLabel.borderColor = Colors.greenBrandColor
            }
        }
    }
    
    var menu: String? {
        didSet {
            if let menuOption = menu {
                menuLabel.translatesAutoresizingMaskIntoConstraints = false
                menuLabel.font = UIFont.systemFont(ofSize: 13, weight: .regular)
                let leadingConstraint = NSLayoutConstraint(item: menuLabel, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1, constant: 0)
                let trailingConstraint = NSLayoutConstraint(item: menuLabel, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1, constant: -5)
                let verticalConstraint = NSLayoutConstraint(item: menuLabel, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1, constant: 0)
                let heightConstraint = NSLayoutConstraint(item: menuLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 36)
                contentView.addSubview(menuLabel)
                contentView.addConstraints([leadingConstraint, trailingConstraint, verticalConstraint, heightConstraint])
                menuLabel.cornerRadius = 18
                menuLabel.clipsToBounds = true
                menuLabel.text = menuOption
            }
        }
    }
    
}
