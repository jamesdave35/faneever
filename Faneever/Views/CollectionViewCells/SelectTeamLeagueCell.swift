//
//  SelectTeamLeagueCell.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class SelectTeamLeagueCell: UICollectionViewCell {
    
    
    @IBOutlet weak var leagueName: PaddingLabel!
    
    override func awakeFromNib() {
        leagueName.roundCorners()

    }
    
    override func prepareForReuse() {
        //
    }
    
    var league: League? {
        didSet {
            updateView()
        }
    }
    
    func resetContent() {
        self.leagueName.fillColor = .clear
        self.leagueName.borderColor = Colors.greenBrandColor
        self.leagueName.textColor = Colors.greenBrandColor
        self.leagueName.text = ""
    }
    
    private func updateView() {
        if let name = self.league?.name {
            leagueName.text = name
        }
    }
    
    
    
    func selectCell() {
        self.leagueName.backgroundColor = Colors.greenBrandColor
        self.leagueName.borderColor = .clear
        self.leagueName.roundCorners()
        self.leagueName.textColor = .white
    }
    
    func unselectCell() {
        self.leagueName.fillColor = .clear
        self.leagueName.borderColor = Colors.greenBrandColor
        self.leagueName.textColor = Colors.greenBrandColor
    }
    
}
