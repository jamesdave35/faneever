//
//  GoalScorerCell.swift
//  Faneever
//
//  Created by James Meli on 4/4/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class GoalScorerCell: UICollectionViewCell {
    @IBOutlet weak var mainMinute: UILabel!
    @IBOutlet weak var extraMinute: UILabel!
    @IBOutlet weak var homeGoalScorer: UILabel!
    @IBOutlet weak var awayGoalScorer: UILabel!
    
    override func awakeFromNib() {
        mainMinute.textColor = Colors.secondaryText
        extraMinute.textColor = Colors.secondaryText
        homeGoalScorer.textColor = Colors.primaryText
        awayGoalScorer.textColor = Colors.primaryText
        
    }
    
    var event: EventFix? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let event = self.event {
            if event.isHomeTeamEvent {
                homeGoalScorer.isHidden = false
                
                if let min = event.fixture.minute {
                    mainMinute.text = "\(min)'"
                }
                
                if let extraMin = event.fixture.extra_minute {
                    extraMinute.isHidden = false
                    extraMinute.text = "+\(extraMin)"
                }
                
                if let goalScorer = event.fixture.player_name {
                    if let type = event.fixture.type {
                        if type == "penalty" {
                            homeGoalScorer.text = "\(goalScorer)(Penalty)"
                        } else {
                            homeGoalScorer.text = goalScorer
                        }
                    }
                    
                }
                
            } else {
                awayGoalScorer.isHidden = false
                
                if let min = event.fixture.minute {
                    mainMinute.text = "\(min)'"
                }
                
                if let extraMin = event.fixture.extra_minute {
                    extraMinute.isHidden = false
                    extraMinute.text = "+\(extraMin)"
                }
                
                if let goalScorer = event.fixture.player_name {
                    awayGoalScorer.text = goalScorer
                }

            }
        }
    }
    
}
