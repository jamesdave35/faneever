//
//  CardCell.swift
//  Faneever
//
//  Created by James Meli on 4/4/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class CardCell: UICollectionViewCell {
    @IBOutlet weak var homeMainMinute: UILabel!
    @IBOutlet weak var homeExtraMinute: UILabel!
    @IBOutlet weak var awayMainMinute: UILabel!
    @IBOutlet weak var awayExtraMinute: UILabel!
    @IBOutlet weak var awayCard: UIView!
    @IBOutlet weak var homeCard: UIView!
    @IBOutlet weak var homePlayerName: UILabel!
    @IBOutlet weak var homeReason: UILabel!
    @IBOutlet weak var awayPlayerName: UILabel!
    @IBOutlet weak var awayReason: UILabel!
    
    override func awakeFromNib() {
        homeMainMinute.textColor = Colors.primaryText
        homeExtraMinute.textColor = Colors.secondaryText
        awayMainMinute.textColor = Colors.primaryText
        awayExtraMinute.textColor = Colors.secondaryText
        homePlayerName.textColor = Colors.primaryText
        homeReason.textColor = Colors.secondaryText
        awayPlayerName.textColor = Colors.primaryText
        awayReason.textColor = Colors.secondaryText
    }
    
    override func prepareForReuse() {
        homeMainMinute.isHidden = true
        homePlayerName.isHidden = true
        homeCard.isHidden = true
        awayMainMinute.isHidden = true
        awayPlayerName.isHidden = true
        awayCard.isHidden = true
        homeExtraMinute.isHidden = true
        homeReason.isHidden = true
        awayReason.isHidden = true
        awayExtraMinute.isHidden = true
    }
    
    
    var event: EventFix? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let event = self.event {
            if event.isHomeTeamEvent {
                homeMainMinute.isHidden = false
                homePlayerName.isHidden = false
                homeCard.isHidden = false
                
                if let min = event.fixture.minute {
                    homeMainMinute.text = "\(min)'"
                }
                
                if let extraMin = event.fixture.extra_minute {
                    homeExtraMinute.isHidden = false
                    homeExtraMinute.text = "+\(extraMin)"
                }
                
                if let type = event.fixture.type {
                    if type == "yellowcard" {
                        homeCard.backgroundColor = .systemYellow
                    } else {
                        homeCard.backgroundColor = .systemRed
                    }
                }
                
                if let playerName = event.fixture.player_name {
                    homePlayerName.text = playerName
                }
                
                if let reason = event.fixture.reason {
                    homeReason.isHidden = false
                    homeReason.text = reason
                }
            } else {
                awayMainMinute.isHidden = false
                awayPlayerName.isHidden = false
                awayCard.isHidden = false
                
                if let min = event.fixture.minute {
                    awayMainMinute.text = "\(min)'"
                }
                
                if let extraMin = event.fixture.extra_minute {
                    awayExtraMinute.isHidden = false
                    awayExtraMinute.text = "+\(extraMin)"
                }
                
                if let type = event.fixture.type {
                    if type == "yellowcard" {
                        awayCard.backgroundColor = .systemYellow
                    } else {
                        awayCard.backgroundColor = .systemRed
                    }
                }
                
                if let playerName = event.fixture.player_name {
                    awayPlayerName.text = playerName
                }
                
                if let reason = event.fixture.reason {
                    awayReason.isHidden = false
                    awayReason.text = reason
                }
            }
        }
    }
    
}
