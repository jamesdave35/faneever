//
//  SubstitutionCell.swift
//  Faneever
//
//  Created by James Meli on 4/4/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class SubstitutionCell: UICollectionViewCell {
    @IBOutlet weak var homeMainMinute: UILabel!
    @IBOutlet weak var homeExtraMinute: UILabel!
    @IBOutlet weak var awayHomeMinute: UILabel!
    @IBOutlet weak var awayExtraMin: UILabel!
    @IBOutlet weak var homeSubInView: UIView!
    @IBOutlet weak var homeSubOutView: UIView!
    @IBOutlet weak var awaySubInView: UIView!
    @IBOutlet weak var awaySubOutView: UIView!
    @IBOutlet weak var homeSubInPlayer: UILabel!
    @IBOutlet weak var homeSubOutPlayer: UILabel!
    @IBOutlet weak var awaySubInPlayer: UILabel!
    @IBOutlet weak var AwaySubOutPlayer: UILabel!
    
    override func awakeFromNib() {
        homeMainMinute.textColor = Colors.primaryText
        homeExtraMinute.textColor = Colors.secondaryText
        awayHomeMinute.textColor = Colors.primaryText
        awayExtraMin.textColor = Colors.secondaryText
    }
    
    override func prepareForReuse() {
        homeMainMinute.isHidden = true
        homeSubInView.isHidden = true
        homeSubOutView.isHidden = true
        homeSubInPlayer.isHidden = true
        homeSubOutPlayer.isHidden = true
        homeExtraMinute.isHidden = true
        awayHomeMinute.isHidden = true
        awaySubInView.isHidden = true
        awaySubOutView.isHidden = true
        awaySubInPlayer.isHidden = true
        AwaySubOutPlayer.isHidden = true
        awayExtraMin.isHidden = true
    }
    
    
    var event: EventFix? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let event = self.event {
            if event.isHomeTeamEvent {
                homeMainMinute.isHidden = false
                homeSubInView.isHidden = false
                homeSubOutView.isHidden = false
                homeSubInPlayer.isHidden = false
                homeSubOutPlayer.isHidden = false
                
                if let min = event.fixture.minute {
                    homeMainMinute.text = "\(min)'"
                }
                
                if let extraMin = event.fixture.extra_minute {
                    homeExtraMinute.isHidden = false
                    homeExtraMinute.text = "+\(extraMin)"
                }
                
                if let subInPlayer = event.fixture.player_name, let subOutPlayer = event.fixture.related_player_name {
                    homeSubInPlayer.text = subOutPlayer
                    homeSubOutPlayer.text = subInPlayer
                }
            } else {
                awayHomeMinute.isHidden = false
                awaySubInView.isHidden = false
                awaySubOutView.isHidden = false
                awaySubInPlayer.isHidden = false
                AwaySubOutPlayer.isHidden = false
                
                if let min = event.fixture.minute {
                    awayHomeMinute.text = "\(min)'"
                }
                
                if let extraMin = event.fixture.extra_minute {
                    awayExtraMin.isHidden = false
                    awayExtraMin.text = "+\(extraMin)"
                }
                
                if let subInPlayer = event.fixture.player_name, let subOutPlayer = event.fixture.related_player_name {
                    awaySubInPlayer.text = subOutPlayer
                    AwaySubOutPlayer.text = subInPlayer
                }
            }
        }
    }
    
}
