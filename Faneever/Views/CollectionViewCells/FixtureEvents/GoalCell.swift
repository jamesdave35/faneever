//
//  GoalCell.swift
//  Faneever
//
//  Created by James Meli on 4/4/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class GoalCell: UICollectionViewCell {
    @IBOutlet weak var homeMainMinute: UILabel!
    @IBOutlet weak var homeExtraMinute: UILabel!
    @IBOutlet weak var awayMainMinute: UILabel!
    @IBOutlet weak var awayExtraMinute: UILabel!
    @IBOutlet weak var homeBallImage: UIImageView!
    @IBOutlet weak var awayBallImage: UIImageView!
    @IBOutlet weak var homeGoalScorer: UILabel!
    @IBOutlet weak var homeAssiter: UILabel!
    @IBOutlet weak var awayGoalscorer: UILabel!
    @IBOutlet weak var awayAssiter: UILabel!
    
    override func awakeFromNib() {
        homeMainMinute.textColor = Colors.primaryText
        homeExtraMinute.textColor = Colors.secondaryText
        awayMainMinute.textColor = Colors.primaryText
        awayExtraMinute.textColor = Colors.secondaryText
        homeGoalScorer.textColor = Colors.primaryText
        homeAssiter.textColor = Colors.secondaryText
        awayGoalscorer.textColor = Colors.primaryText
        awayAssiter.textColor = Colors.secondaryText
    }
    
    override func prepareForReuse() {
        homeMainMinute.isHidden = true
        homeBallImage.isHidden = true
        homeGoalScorer.isHidden = true
        homeExtraMinute.isHidden = true
        homeAssiter.isHidden = true
        awayMainMinute.isHidden = true
        awayBallImage.isHidden = true
        awayGoalscorer.isHidden = true
        awayExtraMinute.isHidden = true
        awayAssiter.isHidden = true
    }
    
    var event: EventFix? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let event = self.event {
            if event.isHomeTeamEvent {
                homeMainMinute.isHidden = false
                homeBallImage.isHidden = false
                homeGoalScorer.isHidden = false
                
                if let min = event.fixture.minute {
                    homeMainMinute.text = "\(min)'"
                }
                
                if let extraMin = event.fixture.extra_minute {
                    homeExtraMinute.isHidden = false
                    homeExtraMinute.text = "+\(extraMin)"
                }
                
                if let goalScorer = event.fixture.player_name {
                    if let type = event.fixture.type {
                        if type == "penalty" {
                            homeGoalScorer.text = "\(goalScorer)(Penalty)"
                        } else {
                            homeGoalScorer.text = goalScorer
                        }
                    }
                    
                }
                
                if let assister = event.fixture.related_player_name {
                    homeAssiter.isHidden = false
                    homeAssiter.text = "Assist(\(assister))"
                }
            } else {
                awayMainMinute.isHidden = false
                awayBallImage.isHidden = false
                awayGoalscorer.isHidden = false
                
                if let min = event.fixture.minute {
                    awayMainMinute.text = "\(min)'"
                }
                
                if let extraMin = event.fixture.extra_minute {
                    awayExtraMinute.isHidden = false
                    awayExtraMinute.text = "+\(extraMin)"
                }
                
                if let goalScorer = event.fixture.player_name {
                    awayGoalscorer.text = goalScorer
                }
                
                if let assister = event.fixture.related_player_name {
                    awayAssiter.isHidden = false
                    awayAssiter.text = "Assist(\(assister))"
                }
            }
        }
    }
    
}
