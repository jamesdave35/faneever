//
//  PlayerLineupCell.swift
//  Faneever
//
//  Created by James Meli on 4/5/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class PlayerLineupCell: UICollectionViewCell {
    
    @IBOutlet weak var numberOfAssists: AnimatableLabel!
    @IBOutlet weak var numberOfGoals: AnimatableLabel!
    @IBOutlet weak var cleatsImage: UIImageView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var captainLabel: AnimatableLabel!
    @IBOutlet weak var ballImage: UIImageView!
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    
    override func awakeFromNib() {
        numberOfGoals.cornerRadius = 7.5
        numberOfGoals.textColor = .white
        numberOfAssists.cornerRadius = 7.5
        numberOfAssists.textColor = .white
    }
    
    var player: LineupPlayer? {
        didSet {
            updateView()
        }
    }
    
    override func prepareForReuse() {
        playerImage.image = nil
        playerName.text = ""
        captainLabel.isHidden = true
        ballImage.isHidden = true
        cardView.isHidden = true
        cleatsImage.isHidden = true
        numberOfAssists.isHidden = true
        numberOfGoals.isHidden = true

    }
    
    private func updateView() {
        if let player = player {
            if let name = player.player?.data?.getCommonName(), let number = player.number {
                playerName.text = "\(number) \(name)"
            }
            
            if let url = player.player?.data?.getImageURL() {
                playerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
            
            if player.isCaptain() {
                captainLabel.isHidden = false
            } else {
                captainLabel.isHidden = true
            }
            
            if player.hasScored() {
                ballImage.isHidden = false
                if player.numberOfGoals() > 1 {
                    numberOfGoals.isHidden = false
                    numberOfGoals.text = "\(player.numberOfGoals())"
                } else {
                    numberOfGoals.isHidden = true
                }
            } else {
                ballImage.isHidden = true
            }
            
            if player.hasAssited() {
                cleatsImage.isHidden = false
                if player.numberOfAssits() > 1 {
                    numberOfAssists.isHidden = false
                    numberOfAssists.text = "\(player.numberOfAssits())"
                } else {
                    numberOfAssists.isHidden = true
                }
            } else {
                cleatsImage.isHidden = true
            }
            
            if player.hasYellowcard() {
                cardView.isHidden = false
                cardView.backgroundColor = .systemYellow
            } else if player.hasRedCard() {
                cardView.isHidden = false
                cardView.backgroundColor = .systemRed
            } else {
                cardView.isHidden = true
            }
        }
    }
    
}
