//
//  FixtureOptionCell.swift
//  Faneever
//
//  Created by James Meli on 3/25/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class FixtureOptionCell: UICollectionViewCell {
    
    @IBOutlet weak var optionLabel: PaddingLabel!
    
    
    override func awakeFromNib() {
        optionLabel.fillColor = Colors.secondaryBackground
        optionLabel.textColor = Colors.secondaryText
        optionLabel.shadowColor = .black
        optionLabel.shadowOffset = .zero
        optionLabel.cornerRadius = 18
    }
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                selectCell()
            } else {
                unSelectCell()
            }
        }
    }
    
    var option: String? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        optionLabel.text = option!
    }
    
    func selectCell() {
        self.optionLabel.fillColor = Colors.greenBrandColor
        self.optionLabel.textColor = .white
    }
    
    func unSelectCell() {
        self.optionLabel.fillColor = .clear
        self.optionLabel.textColor = Colors.greenBrandColor
    }
    
    
}
