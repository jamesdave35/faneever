//
//  SelectCountryCell.swift
//  Faneever
//
//  Created by James Meli on 3/1/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class SelectLeagueCell: UICollectionViewCell {
    
    @IBOutlet weak var cellView: AnimatableView!
    @IBOutlet weak var leagueLogo: UIImageView!
    @IBOutlet weak var leagueName: UILabel!
    
    override func awakeFromNib() {
        self.cellView.layer.cornerCurve = .continuous
    }
    
    var league: League? {
        didSet {
            updateCell()
        }
    }
    
    override func prepareForReuse() {
        //
    }
    
    func resetContent() {
        self.cellView.backgroundColor = .clear
        self.cellView.borderColor = UIColor(hexString: "DFDFDF")
        self.leagueName.textColor = .darkGray
        self.leagueName.text = ""
        self.leagueLogo.image = nil
    }
    
    private func updateCell() {
        DispatchQueue.main.async {
            if let name = self.league?.name {
                self.leagueName.text = name
            }
            
            if let urlString = self.league?.logo_path {
                let url = URL(string: urlString)
                self.leagueLogo.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            }
        }

    }
    
    func selectCell() {
        UIView.animate(withDuration: 0.5) {
            self.cellView.backgroundColor = Colors.greenBrandColor
            self.leagueName.textColor = .white
            self.cellView.borderColor = .clear
        
        }
    }
    
    func unSelectCell() {
        self.cellView.backgroundColor = .clear
        self.cellView.borderColor = UIColor(hexString: "DFDFDF")
        self.leagueName.textColor = .darkGray
       
    }
    
}
