//
//  TeamPostionCell.swift
//  Faneever
//
//  Created by James Meli on 3/21/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamPostionCell: UICollectionViewCell {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var numOfPts: UILabel!
    @IBOutlet weak var numOfDraw: UILabel!
    @IBOutlet weak var numOfLost: UILabel!
    @IBOutlet weak var numOfWins: UILabel!
    @IBOutlet weak var numberOfGames: UILabel!
    @IBOutlet weak var drawLabel: UILabel!
    @IBOutlet weak var ptsLabel: UILabel!
    @IBOutlet weak var lostLabel: UILabel!
    @IBOutlet weak var wonLabel: UILabel!
    @IBOutlet weak var playedLabel: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var rankLabel: UILabel!
    

    
    var standing: TeamPos? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let stan = standing {
            if stan.flag {
                stackView.isHidden = false
                topConstraint.constant = 20
            } else {
                stackView.isHidden = true
                topConstraint.constant = -5
            }
            
            if let rank = stan.standing?.position, let name = stan.standing?.team_name, let url = stan.standing?.team?.data?.getLogoUrl() {
                rankLabel.text = "\(rank)"
                teamName.text = "\(name)"
                teamLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let played = stan.standing?.overall?.games_played, let won = stan.standing?.overall?.won, let lost = stan.standing?.overall?.lost, let draws = stan.standing?.overall?.draw, let points = stan.standing?.total?.points {
                numberOfGames.text = "\(played)"
                numOfWins.text = "\(won)"
                numOfLost.text = "\(lost)"
                numOfDraw.text = "\(draws)"
                numOfPts.text = "\(points)"
            }
        }
    }

   

}

struct TeamPos {
    var standing: Standings?
    var flag = false
}
