//
//  TeamTopPlayerCell.swift
//  Faneever
//
//  Created by James Meli on 4/7/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable
import EZYGradientView

class TeamTopPlayerCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: AnimatableView!
    @IBOutlet weak var gradientView: EZYGradientView!
    @IBOutlet weak var statCategory: UILabel!
    @IBOutlet weak var topPlayerFirstName: UILabel!
    @IBOutlet weak var topPlayerLastName: UILabel!
    @IBOutlet weak var topPlayerValue: UILabel!
    @IBOutlet weak var topPlayerImage: UIImageView!
    @IBOutlet weak var secondPlayerValue: UILabel!
    @IBOutlet weak var thirdPlayerValue: UILabel!
    @IBOutlet weak var twoLabel: UILabel!
    @IBOutlet weak var secondPlayerName: UILabel!
    @IBOutlet weak var secondPlayerImage: UIImageView!
    @IBOutlet weak var threeLabel: UILabel!
    @IBOutlet weak var thirdPlayerImage: UIImageView!
    @IBOutlet weak var thirdPlayerName: UILabel!
    
    override func awakeFromNib() {

        configureGradientView()
    }
    
    var topPlayers: TopPlayerCategory? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let players = topPlayers?.players, let title = topPlayers?.title {
            statCategory.text = title
            
            if players.count > 2 {
                if let name = players[0].player?.data?.display_name {
                    let names = name.components(separatedBy: " ")
                    if names.count == 2 {
                        topPlayerFirstName.text = names[0]
                        topPlayerLastName.text = names[1]
                    } else if names.count == 1 {
                        topPlayerFirstName.text = ""
                        topPlayerLastName.text = names[0]
                    } else {
                        topPlayerFirstName.text = names[0]
                        topPlayerLastName.text = "\(names[1]) \(names[2])"
                    }

                }
                
                if let url = players[0].player?.data?.getImageURL() {
                    topPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                }
                
                if let name = players[1].player?.data?.display_name, let secondName = players[2].player?.data?.display_name {
                    secondPlayerName.text = name
                    thirdPlayerName.text = secondName
                }
                
                if let url = players[1].player?.data?.getImageURL(){
                    secondPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                }
                
                if let url = players[2].player?.data?.getImageURL(){
                    thirdPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                }
                
                switch title {
                case "Goal Scorers":
                    topPlayerValue.text = "\(players[0].getNumOfGoals())"
                    secondPlayerValue.text = "\(players[1].getNumOfGoals())"
                    thirdPlayerValue.text = "\(players[2].getNumOfGoals())"
                case "Assists":
                    topPlayerValue.text = "\(players[0].getNumOfAssists())"
                    secondPlayerValue.text = "\(players[1].getNumOfAssists())"
                    thirdPlayerValue.text = "\(players[2].getNumOfAssists())"
                case "Key Passes":
                    topPlayerValue.text = "\(players[0].getNumOfKeyPasses())"
                    secondPlayerValue.text = "\(players[1].getNumOfKeyPasses())"
                    thirdPlayerValue.text = "\(players[2].getNumOfKeyPasses())"
                case "Dribbles":
                    topPlayerValue.text = "\(players[0].getNumOfDribbles())"
                    secondPlayerValue.text = "\(players[1].getNumOfDribbles())"
                    thirdPlayerValue.text = "\(players[2].getNumOfDribbles())"
                case "Interceptions":
                    topPlayerValue.text = "\(players[0].getNumOfInterceptions())"
                    secondPlayerValue.text = "\(players[1].getNumOfInterceptions())"
                    thirdPlayerValue.text = "\(players[2].getNumOfInterceptions())"
                case "Yellow Cards":
                    topPlayerValue.text = "\(players[0].getNumOfYellowCards())"
                    secondPlayerValue.text = "\(players[1].getNumOfYellowCards())"
                    thirdPlayerValue.text = "\(players[2].getNumOfYellowCards())"
                case "Red Cards":
                    topPlayerValue.text = "\(players[0].getNumOfRedCards())"
                    secondPlayerValue.text = "\(players[1].getNumOfRedCards())"
                    thirdPlayerValue.text = "\(players[2].getNumOfRedCards())"
                default:
                    break
                }
            }
        }
    }
    
    private func configureGradientView() {
        if let teamName = Constants.currentUser?.getFavoriteTeamName() {
              let firstColor: UIColor = {
                if #available(iOS 13, *) {
                    return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                        if UITraitCollection.userInterfaceStyle == .dark {
                            /// Return the color for Dark Mode
                            return UIColor(hexString: "2D2D2D")
                        } else {
                            /// Return the color for Light Mode
                            return TeamColors.teamColors[teamName]!.darker(by: 40)
                        }
                    }
                } else {
                    /// Return a fallback color for iOS 12 and lower.
                    return UIColor(hexString: "202020")
                }
            }()
            
           let secondColor: UIColor = {
            if #available(iOS 13, *) {
                return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                    if UITraitCollection.userInterfaceStyle == .dark {
                        /// Return the color for Dark Mode
                        return UIColor(named: "secondaryBackground")!
                    } else {
                        /// Return the color for Light Mode
                        return TeamColors.teamColors[teamName]!
                    }
                }
            } else {
                /// Return a fallback color for iOS 12 and lower.
                return UIColor(hexString: "202020")
            }
        }()
            gradientView.layer.cornerRadius = 15
            gradientView.layer.cornerCurve = .continuous
            gradientView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            gradientView.firstColor = firstColor
            gradientView.secondColor = secondColor
            gradientView.angleº = 0
            gradientView.fadeIntensity = 1
            gradientView.colorRatio = 0.5
            
        }
    }
    
}

//UIColor(hexString: "141414")
