//
//  LeagueTopScorerCell.swift
//  Faneever
//
//  Created by James Meli on 5/12/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable
import EZYGradientView

class LeagueTopScorerCell: UICollectionViewCell {
    
    @IBOutlet weak var thirdPlayerStackView: UIStackView!
    @IBOutlet weak var secondPlayerStackView: UIStackView!
    @IBOutlet weak var containerView: AnimatableView!
    @IBOutlet weak var gradientView: EZYGradientView!
    @IBOutlet weak var statCategory: UILabel!
    @IBOutlet weak var topPlayerFirstName: UILabel!
    @IBOutlet weak var topPlayerLastName: UILabel!
    @IBOutlet weak var topPlayerValue: UILabel!
    @IBOutlet weak var topPlayerImage: UIImageView!
    @IBOutlet weak var secondPlayerValue: UILabel!
    @IBOutlet weak var thirdPlayerValue: UILabel!
    @IBOutlet weak var twoLabel: UILabel!
    @IBOutlet weak var secondPlayerName: UILabel!
    @IBOutlet weak var secondPlayerImage: UIImageView!
    @IBOutlet weak var threeLabel: UILabel!
    @IBOutlet weak var thirdPlayerImage: UIImageView!
    @IBOutlet weak var thirdPlayerName: UILabel!
    
    var delegate: LeagueProfileDelegate?
    
    override func awakeFromNib() {
        containerView.backgroundColor = Colors.secondaryBackground
        configureGradientView()
        statCategory.textColor = Colors.primaryText
        topPlayerFirstName.textColor = Colors.primaryText
        topPlayerLastName.textColor = Colors.primaryText
        topPlayerValue.textColor = Colors.primaryText
        twoLabel.textColor = Colors.secondaryText
        secondPlayerName.textColor = Colors.primaryText
        secondPlayerValue.textColor = Colors.primaryText
        thirdPlayerName.textColor = Colors.primaryText
        thirdPlayerValue.textColor = Colors.primaryText
        threeLabel.textColor = Colors.secondaryText
        
        let topPlayerGesture = UITapGestureRecognizer(target: self, action: #selector(goToTopPlayer))
        let secondPlayerGesture = UITapGestureRecognizer(target: self, action: #selector(goToSecondPlayer))
        let thirdPlayerGesture = UITapGestureRecognizer(target: self, action: #selector(goToThirdPlayer))
        
        gradientView.addGestureRecognizer(topPlayerGesture)
        secondPlayerStackView.addGestureRecognizer(secondPlayerGesture)
        thirdPlayerStackView.addGestureRecognizer(thirdPlayerGesture)
        
        
        
    }
    
    var topScorers: TopScorers? {
        didSet {
            updateView()
        }
    }
    
    var title: String?
    
    private func updateView() {
        if let topPlayers = topScorers {
            if let title = title {
                statCategory.text = title
                switch title {
                case "Goal Scorers":
                    if let name = topPlayers.aggregatedGoalscorers?.data![0].getPlayerName() {
                        let names = name.components(separatedBy: " ")
                        if names.count == 2 {
                            topPlayerFirstName.text = names[0]
                            topPlayerLastName.text = names[1]
                        } else if names.count == 1 {
                            topPlayerFirstName.text = ""
                            topPlayerLastName.text = names[0]
                        } else {
                            topPlayerFirstName.text = names[0]
                            topPlayerLastName.text = "\(names[1]) \(names[2])"
                        }
                        
                        if let url = topPlayers.aggregatedGoalscorers?.data![0].getPlayerUrl() {
                            topPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let value = topPlayers.aggregatedGoalscorers?.data![0].goals {
                            topPlayerValue.text = "\(value)"
                        }
                        
                        if let name = topPlayers.aggregatedGoalscorers?.data![1].getPlayerName() {
                            secondPlayerName.text = name
                        }
                        
                        if let url = topPlayers.aggregatedGoalscorers?.data![1].getPlayerUrl() {
                            secondPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let value = topPlayers.aggregatedGoalscorers?.data![1].goals {
                            secondPlayerValue.text = "\(value)"
                        }
                        
                        if let name = topPlayers.aggregatedGoalscorers?.data![2].getPlayerName() {
                            thirdPlayerName.text = name
                        }
                        
                        if let url = topPlayers.aggregatedGoalscorers?.data![2].getPlayerUrl() {
                            thirdPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let value = topPlayers.aggregatedGoalscorers?.data![2].goals {
                            thirdPlayerValue.text = "\(value)"
                        }
                    }
                    
                case "Top Assists":
                    if let name = topPlayers.aggregatedAssistscorers?.data![0].getPlayerName() {
                        let names = name.components(separatedBy: " ")
                        if names.count == 2 {
                            topPlayerFirstName.text = names[0]
                            topPlayerLastName.text = names[1]
                        } else if names.count == 1 {
                            topPlayerFirstName.text = ""
                            topPlayerLastName.text = names[0]
                        } else {
                            topPlayerFirstName.text = names[0]
                            topPlayerLastName.text = "\(names[1]) \(names[2])"
                        }
                        
                        if let url = topPlayers.aggregatedAssistscorers?.data![0].getPlayerUrl() {
                            topPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let value = topPlayers.aggregatedAssistscorers?.data![0].assists {
                            topPlayerValue.text = "\(value)"
                        }
                        
                        if let name = topPlayers.aggregatedAssistscorers?.data![1].getPlayerName() {
                            secondPlayerName.text = name
                        }
                        
                        if let url = topPlayers.aggregatedAssistscorers?.data![1].getPlayerUrl() {
                            secondPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let value = topPlayers.aggregatedAssistscorers?.data![1].assists {
                            secondPlayerValue.text = "\(value)"
                        }
                        
                        if let name = topPlayers.aggregatedAssistscorers?.data![2].getPlayerName() {
                            thirdPlayerName.text = name
                        }
                        
                        if let url = topPlayers.aggregatedAssistscorers?.data![2].getPlayerUrl() {
                            thirdPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let value = topPlayers.aggregatedAssistscorers?.data![2].assists {
                            thirdPlayerValue.text = "\(value)"
                        }
                    }
                    
                default:
                    if let name = topPlayers.aggregatedCardscorers?.data![0].getPlayerName() {
                        let names = name.components(separatedBy: " ")
                        if names.count == 2 {
                            topPlayerFirstName.text = names[0]
                            topPlayerLastName.text = names[1]
                        } else if names.count == 1 {
                            topPlayerFirstName.text = ""
                            topPlayerLastName.text = names[0]
                        } else {
                            topPlayerFirstName.text = names[0]
                            topPlayerLastName.text = "\(names[1]) \(names[2])"
                        }
                        
                        if let url = topPlayers.aggregatedCardscorers?.data![0].getPlayerUrl() {
                            topPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let value = topPlayers.aggregatedCardscorers?.data![0].yellowcards {
                            topPlayerValue.text = "\(value)"
                        }
                        
                        if let name = topPlayers.aggregatedCardscorers?.data![1].getPlayerName() {
                            secondPlayerName.text = name
                        }
                        
                        if let url = topPlayers.aggregatedCardscorers?.data![1].getPlayerUrl() {
                            secondPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let value = topPlayers.aggregatedCardscorers?.data![1].yellowcards {
                            secondPlayerValue.text = "\(value)"
                        }
                        
                        if let name = topPlayers.aggregatedCardscorers?.data![2].getPlayerName() {
                            thirdPlayerName.text = name
                        }
                        
                        if let url = topPlayers.aggregatedCardscorers?.data![2].getPlayerUrl() {
                            thirdPlayerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
                        }
                        
                        if let value = topPlayers.aggregatedCardscorers?.data![2].yellowcards {
                            thirdPlayerValue.text = "\(value)"
                        }
                    }
                }
            }
        }
    }
    
    private func configureGradientView() {
          let firstColor: UIColor = {
            if #available(iOS 13, *) {
                return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                    if UITraitCollection.userInterfaceStyle == .dark {
                        /// Return the color for Dark Mode
                        return UIColor(hexString: "2D2D2D")
                    } else {
                        /// Return the color for Light Mode
                        return UIColor(hexString: "DFDFDF")
                    }
                }
            } else {
                /// Return a fallback color for iOS 12 and lower.
                return UIColor(hexString: "202020")
            }
        }()
        
        gradientView.firstColor = Colors.secondaryBackground
        gradientView.secondColor = firstColor
        gradientView.angleº = 45
        gradientView.fadeIntensity = 1
        gradientView.colorRatio = 0.5
        

    }
    
    @objc private func goToTopPlayer() {
//        if let title = title, let players = topScorers {
//            if title == "Goal Scorers" {
//                if let player = players.aggregatedGoalscorers?.data {
//                    delegate?.didSelectPlayer(player: player)
//                }
//            } else if title == "Top Assists" {
//                if let player = players.aggregatedAssistscorers?.data![0] {
//                    delegate?.didSelectPlayer(player: player)
//                }
//            } else {
//                if let player = players.aggregatedCardscorers?.data![0] {
//                    delegate?.didSelectPlayer(player: player)
//                }
//            }
//        }
        
    }
    
    @objc private func goToSecondPlayer() {
//        if let title = title, let players = topScorers {
//            if title == "Goal Scorers" {
//                if let player = players.aggregatedGoalscorers?.data![1] {
//                    delegate?.didSelectPlayer(player: player)
//                }
//            } else if title == "Top Assists" {
//                if let player = players.aggregatedAssistscorers?.data![1] {
//                    delegate?.didSelectPlayer(player: player)
//                }
//            } else {
//                if let player = players.aggregatedCardscorers?.data![1] {
//                    delegate?.didSelectPlayer(player: player)
//                }
//            }
//        }
    }
    
    @objc private func goToThirdPlayer() {
//        if let title = title, let players = topScorers {
//            if title == "Goal Scorers" {
//                if let player = players.aggregatedGoalscorers?.data![2] {
//                    delegate?.didSelectPlayer(player: player)
//                }
//            } else if title == "Top Assists" {
//                if let player = players.aggregatedAssistscorers?.data![2] {
//                    delegate?.didSelectPlayer(player: player)
//                }
//            } else {
//                if let player = players.aggregatedCardscorers?.data![2] {
//                    delegate?.didSelectPlayer(player: player)
//                }
//            }
//        }
    }
    
    @IBAction func seeAllPressed(_ sender: Any) {
        if let title = self.title {
            delegate?.didSelectSeeAllPlayers!(header: title)
        }
    }
    
}
