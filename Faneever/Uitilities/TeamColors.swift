//
//  TeamColors.swift
//  Faneever
//
//  Created by James Meli on 4/6/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation
import UIKit

struct TeamColors {
    
    static let teamColors: [String: UIColor] = [
        "West Ham United": UIColor(hexString: "3E2723"), "Tottenham Hotspur": UIColor(hexString: "311B92"),
        "Liverpool": UIColor(hexString: "D50000"), "Manchester City": UIColor(hexString: "0091EA"),
        "Everton": UIColor(hexString: "304FFE"), "Manchester United": UIColor(hexString: "B71C1C"),
        "Aston Villa": UIColor(hexString: "4E342E"), "Chelsea": UIColor(hexString: "2962FF"),
        "Arsenal": UIColor(hexString: "D50000"), "Newcastle United": UIColor(hexString: "263238"),
        "Sheffield United": UIColor(hexString: "D32F2F"), "Watford": UIColor(hexString: "FFEA00"),
        "Burnley": UIColor(hexString: "E53935"), "Wolverhampton Wanderers": UIColor(hexString: "E65100"),
        "Norwich City": UIColor(hexString: "1B5E20"), "Leicester City": UIColor(hexString: "2962FF"),
        "Crystal Palace": UIColor(hexString: "651FFF"), "AFC Bournemouth": UIColor(hexString: "B71C1C"),
        "Southampton": UIColor(hexString: "D32F2F"), "Brighton & Hove Albion": UIColor(hexString: "0091EA"),
        "Celta de Vigo": UIColor(hexString: "00B0FF"), "SD Eibar": UIColor(hexString: "304FFE"),
        "Barcelona": UIColor(hexString: "2962FF"), "Granada": UIColor(hexString: "D32F2F"),
        "Getafe": UIColor(hexString: "0D47A1"), "Valencia": UIColor(hexString: "E65100"),
        "Real Valladolid": UIColor(hexString: "651FFF"), "Osasuna": UIColor(hexString: "B71C1C"),
        "Real Betis": UIColor(hexString: "00C853"), "Espanyol": UIColor(hexString: "0D47A1"),
        "Real Sociedad": UIColor(hexString: "2962FF"), "Mallorca": UIColor(hexString: "F44336"),
        "Sevilla": UIColor(hexString: "FF1744"), "Leganés": UIColor(hexString: "0D47A1"),
        "Deportivo Alavés": UIColor(hexString: "0D47A1"), "Levante": UIColor(hexString: "1565C0"),
        "Real Madrid": UIColor(hexString: "6200EA"), "Villarreal": UIColor.systemYellow,
        "Atlético Madrid": UIColor.systemRed, "Athletic Club": UIColor.systemBlue,
        "Bayern München": UIColor.systemRed, "Borussia Dortmund": UIColor.systemYellow, "Paris Saint Germain": UIColor(hexString: "0D47A1"),
        "Juventus": UIColor(hexString: "263238")
    ]
}
