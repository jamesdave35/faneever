//
//  Constants.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation
import Firebase
import UIKit

 class Constants {
    
    static let rapidApiKey = "641b9ef020mshfff79cb5504b6e9p1163a7jsn4036f106537f"
    static let currentUserId = Auth.auth().currentUser?.uid
    static let navBarTitleAttributes: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 17, weight: .medium), .foregroundColor: Colors.primaryText]
    static let navBarLargeTitleAttributes: [NSAttributedString.Key: Any] = [.font: UIFont(name: "Helvetica-Bold", size: 33), .foregroundColor: Colors.primaryText]
    static var currentUser: User?
    static let defaultFaceImage = UIImage(named: "default-face")
    static let defaultTeamImage = UIImage(named: "default-team")
    static let likeImage = UIImage(systemName: "heart")
    static let likedImage = UIImage(systemName: "heart.fill")
    static let storyboard = UIStoryboard(name: "Main", bundle: nil)
    static let positionMap = ["G": "Goalkeeper", "D": "Defender", "M": "Midfielder", "A": "Attacker"]
    
}
