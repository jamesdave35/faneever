//
//  LeagueColors.swift
//  Faneever
//
//  Created by James Meli on 5/12/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation
import UIKit

struct LeagueColors {
    
    static let leagueColors: [String: UIColor] = ["Premier League": UIColor(hexString: "e90052"), "La Liga": UIColor(hexString: "304FFE"), "Ligue 1": UIColor(hexString: "CDDC39").darker(by: 15), "Serie A": UIColor(hexString: "00C853").darker(by: 10), "Bundesliga": UIColor(hexString: "D50000")]
}
