//
//  GlobalFunctions.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import Firebase
import ACFloatingTextfield_Swift

 class GlobalFunctions {
    
     static func playSelectionFeedbackGenerator() {
        let selectorFeedbackGenerator = UISelectionFeedbackGenerator()
        selectorFeedbackGenerator.prepare()
        selectorFeedbackGenerator.selectionChanged()
    }
    
     static func displayError(errorMessage: String, vc: UIViewController) {
        let alertController = UIAlertController(title: "Sorry", message: errorMessage, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(dismissAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
     static func getAgeFromBirthDate(birthday: Date) -> Int {
        let now = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        if let age = ageComponents.year {
            return age
        } else {
            return 0
        }
    }
    
     static func currateListOfPlayerStats(firstStat: PlayerStatNumbers, secondStat: PlayerStatNumbers?) -> [String: [StatComparison]] {
        
        let appearances = StatComparison(firstValue: firstStat.appearences, secondValue: secondStat?.appearences, name: "Appearances")
        let started = StatComparison(firstValue: firstStat.lineups, secondValue: secondStat?.lineups, name: "Started")
        let passes = StatComparison(firstValue: firstStat.passes?.total, secondValue: secondStat?.passes?.total, name: "Passes")
        let keyPasses = StatComparison(firstValue: firstStat.passes?.key_passes, secondValue: secondStat?.passes?.key_passes, name: "Key Passes")
        let goals = StatComparison(firstValue: firstStat.goals, secondValue: secondStat?.goals, name: "Goals")
        let assists = StatComparison(firstValue: firstStat.assists, secondValue: secondStat?.assists, name: "Assists")
        let accuracy = StatComparison(firstValue: firstStat.passes?.accuracy, secondValue: secondStat?.passes?.accuracy, name: "Accuracy")
        let dribbles = StatComparison(firstValue: firstStat.dribbles?.attempts, secondValue: secondStat?.dribbles?.attempts, name: "Dribbles")
        let dribblesWon = StatComparison(firstValue: firstStat.dribbles?.success, secondValue: secondStat?.dribbles?.success, name: "Dribbles Won")
        let minutes = StatComparison(firstValue: firstStat.minutes, secondValue: secondStat?.minutes, name: "Minutes")
        let minsPerGoal = StatComparison(firstValue: firstStat.getMinsPerGoal(), secondValue: secondStat?.getMinsPerGoal(), name: "Mins/Goal")
        let crosses = StatComparison(firstValue: firstStat.crosses?.total, secondValue: secondStat?.crosses?.total, name: "Crosses")
        let accurateCrosses = StatComparison(firstValue: firstStat.crosses?.accurate, secondValue: secondStat?.crosses?.accurate, name: "Accurate Crosses")
        let interceptions = StatComparison(firstValue: firstStat.interceptions, secondValue: secondStat?.interceptions, name: "Interceptions")
        let tackles = StatComparison(firstValue: firstStat.tackles, secondValue: secondStat?.tackles, name: "Tackles")
        let blocks = StatComparison(firstValue: firstStat.blocks, secondValue: secondStat?.blocks, name: "Blocks")
        let duels = StatComparison(firstValue: firstStat.duels?.total, secondValue: secondStat?.duels?.total, name: "Duels")
        let duelsWon = StatComparison(firstValue: firstStat.duels?.won, secondValue: secondStat?.duels?.won, name: "Duels Won")
        let foulsDrawn = StatComparison(firstValue: firstStat.fouls?.drawn, secondValue: secondStat?.fouls?.drawn, name: "Fouls Drawn")
        let foulsCommited = StatComparison(firstValue: firstStat.fouls?.committed, secondValue: secondStat?.fouls?.committed, name: "Fouls Commited")
        let yellowCards = StatComparison(firstValue: firstStat.yellowcards, secondValue: secondStat?.yellowcards, name: "Yellow Cards")
        let redCards = StatComparison(firstValue: firstStat.redcards, secondValue: secondStat?.redcards, name: "Red Cards")
        let penaltiesWon = StatComparison(firstValue: firstStat.penalties?.won, secondValue: secondStat?.penalties?.won, name: "Won")
        let penaltiesScored = StatComparison(firstValue: firstStat.penalties?.scores, secondValue: secondStat?.penalties?.scores, name: "Scored")
        let penaltiesCommited = StatComparison(firstValue: firstStat.penalties?.committed, secondValue: secondStat?.penalties?.committed, name: "Committed")
        let penaltiesSaved = StatComparison(firstValue: firstStat.penalties?.saves, secondValue: secondStat?.penalties?.saves, name: "Saved")
        let penaltiesMissed = StatComparison(firstValue: firstStat.penalties?.missed, secondValue: secondStat?.penalties?.missed, name: "Missed")
        let saves = StatComparison(firstValue: firstStat.saves, secondValue: secondStat?.saves, name: "Total")
        let insideBoxSaves = StatComparison(firstValue: firstStat.inside_box_saves, secondValue: secondStat?.inside_box_saves, name: "Inside Box")
        
        let topStats = [appearances, started, goals, assists, minutes, minsPerGoal]
        let distribution = [passes, keyPasses, accuracy, crosses, accurateCrosses]
        let takeOns = [dribbles, dribblesWon, duels, duelsWon]
        let defense = [interceptions, tackles, blocks, foulsDrawn, foulsCommited]
        let discipline = [yellowCards, redCards]
        let pk = [penaltiesWon, penaltiesScored, penaltiesMissed, penaltiesCommited, penaltiesSaved]
        let gk = [saves, insideBoxSaves]
        return ["TOP STATS": topStats, "DISTRIBUTION": distribution, "TAKE ONS": takeOns, "DEFENSE": defense, "PENALTIES": pk, "SAVES": gk, "DISCIPLINE": discipline]
        
    }
    
    static func curateListOfPlayerProfileStats(player: PlayerStatNumbers) -> [String: [StatDisplay]] {
        
        let appearances = StatDisplay(statName: "Appearances", statValue: player.appearences)
        let started = StatDisplay(statName: "Started", statValue: player.lineups)
        let passes = StatDisplay(statName: "Passes", statValue: player.passes?.total)
        let keyPasses = StatDisplay(statName: "Key Passes", statValue: player.passes?.key_passes)
        let goals = StatDisplay( statName: "Goals", statValue: player.goals)
        let assists = StatDisplay(statName: "Assists", statValue: player.assists)
        let accuracy = StatDisplay(statName: "Accuracy", statValue: player.passes?.accuracy)
        let dribbles = StatDisplay(statName: "Dribbles", statValue: player.dribbles?.attempts)
        let dribblesWon = StatDisplay(statName: "Dribbles Won", statValue: player.dribbles?.success)
        let minutes = StatDisplay(statName: "Minutes", statValue: player.minutes)
        let minsPerGoal = StatDisplay(statName: "Mins/Goal", statValue: player.getMinsPerGoal())
        let crosses = StatDisplay(statName: "Crosses", statValue: player.crosses?.total)
        let accurateCrosses = StatDisplay(statName: "Accurate Crosses", statValue: player.crosses?.accurate)
        let interceptions = StatDisplay(statName: "Interceptions", statValue: player.interceptions)
        let tackles = StatDisplay(statName: "Tackles", statValue: player.tackles)
        let blocks = StatDisplay(statName: "Blocks", statValue: player.blocks)
        let duels = StatDisplay(statName: "Duels", statValue: player.duels?.total)
        let duelsWon = StatDisplay(statName: "Duels Won", statValue: player.duels?.won)
        let foulsDrawn = StatDisplay(statName: "Fouls Drawn", statValue: player.fouls?.drawn)
        let foulsCommited = StatDisplay(statName: "Fouls Commited", statValue: player.fouls?.committed)
        let yellowCards = StatDisplay(statName: "Yellow Cards", statValue: player.yellowcards)
        let redCards = StatDisplay(statName: "Red Cards", statValue: player.redcards)
        let penaltiesWon = StatDisplay(statName: "Won", statValue: player.penalties?.won)
        let penaltiesScored = StatDisplay(statName: "Scored", statValue: player.penalties?.scores)
        let penaltiesCommited = StatDisplay(statName: "Committed", statValue: player.penalties?.committed)
        let penaltiesSaved = StatDisplay(statName: "Saved", statValue: player.penalties?.saves)
        let penaltiesMissed = StatDisplay(statName: "Missed", statValue: player.penalties?.missed)
        let saves = StatDisplay(statName: "Total", statValue: player.saves)
        let insideBoxSaves = StatDisplay(statName: "Inside Box", statValue: player.inside_box_saves)
        
        let topStats = [appearances, started, goals, assists, minutes, minsPerGoal]
        let distribution = [passes, keyPasses, accuracy, crosses, accurateCrosses]
        let takeOns = [dribbles, dribblesWon, duels, duelsWon]
        let defense = [interceptions, tackles, blocks, foulsDrawn, foulsCommited]
        let discipline = [yellowCards, redCards]
        let pk = [penaltiesWon, penaltiesScored, penaltiesMissed, penaltiesCommited, penaltiesSaved]
        let gk = [saves, insideBoxSaves]
        
        return ["TOP STATS": topStats, "DISTRIBUTION": distribution, "TAKE ONS": takeOns, "DEFENSE": defense, "PENALTIES": pk, "SAVES": gk, "DISCIPLINE": discipline]
    }
    
    static func currateListOfTeamStats(firstTeamStat: TeamStatNumebrs, secondTeamStat: TeamStatNumebrs?) -> [String: [StatComparison]] {
        
        let games = StatComparison(firstValue: firstTeamStat.getTotalGames(), secondValue: secondTeamStat?.getTotalGames(), name: "Total")
        let won = StatComparison(firstValue: firstTeamStat.win?.total?.intValue, secondValue: secondTeamStat?.win?.total?.intValue, name: "Won")
        let draws = StatComparison(firstValue: firstTeamStat.draw?.total?.intValue, secondValue: secondTeamStat?.draw?.total?.intValue, name: "Draws")
        let lost = StatComparison(firstValue: firstTeamStat.lost?.total?.intValue, secondValue: secondTeamStat?.lost?.total?.intValue, name: "Lost")
        let goalsFor = StatComparison(firstValue: firstTeamStat.goals_for?.total?.intValue, secondValue: secondTeamStat?.goals_for?.total?.intValue, name: "Total")
        let goalsForHome = StatComparison(firstValue: firstTeamStat.goals_for?.home?.intValue, secondValue: secondTeamStat?.goals_for?.home?.intValue, name: "Home")
        let goalsForAway = StatComparison(firstValue: firstTeamStat.goals_for?.away?.intValue, secondValue: secondTeamStat?.goals_for?.away?.intValue, name: "Away")
        let goalsAgainst = StatComparison(firstValue: firstTeamStat.goals_against?.total?.intValue, secondValue: secondTeamStat?.goals_against?.total?.intValue, name: "Total")
        let goalsAgainstHome = StatComparison(firstValue: firstTeamStat.goals_against?.home?.intValue, secondValue: secondTeamStat?.goals_against?.home?.intValue, name: "Home")
        let goalsAgainstAway = StatComparison(firstValue: firstTeamStat.goals_against?.away?.intValue, secondValue: secondTeamStat?.goals_against?.away?.intValue, name: "Away")
        let cleanSheets = StatComparison(firstValue: firstTeamStat.clean_sheet?.total?.intValue, secondValue: secondTeamStat?.clean_sheet?.total?.intValue, name: "Total")
        let cleanSheetsHome = StatComparison(firstValue: firstTeamStat.clean_sheet?.home?.intValue, secondValue: secondTeamStat?.clean_sheet?.home?.intValue, name: "Home")
        let cleanSheetsAway = StatComparison(firstValue: firstTeamStat.clean_sheet?.away?.intValue, secondValue: secondTeamStat?.clean_sheet?.away?.intValue, name: "Away")
        
        return ["GAMES": [games, won, draws, lost], "GOALS SCORED": [goalsFor, goalsForHome, goalsForAway], "GOALS CONCEDED": [goalsAgainst, goalsAgainstHome, goalsAgainstAway], "CLEAN SHEETS": [cleanSheets, cleanSheetsHome, cleanSheetsAway]]
        
    }
    
    static func currateListOfFixtureStats(firstTeamStat: FixtureStats, secondTeamStat: FixtureStats) -> [String: [StatComparison]] {
        
        let possession = StatComparison(firstValue: firstTeamStat.possessiontime, secondValue: secondTeamStat.possessiontime, name: "Possession")
        let goals = StatComparison(firstValue: firstTeamStat.goals, secondValue: secondTeamStat.goals, name: "Goals")
        let totalShots = StatComparison(firstValue: firstTeamStat.shots?.total, secondValue: secondTeamStat.shots?.total, name: "Total Shots")
        let totalPasses = StatComparison(firstValue: firstTeamStat.passes?.total, secondValue: secondTeamStat.passes?.total, name: "Passes")
        let goalAttempts = StatComparison(firstValue: firstTeamStat.goal_attempts, secondValue: secondTeamStat.goal_attempts, name: "Goal Attempts")
        let fouls = StatComparison(firstValue: firstTeamStat.fouls, secondValue: secondTeamStat.fouls, name: "Fouls")
        let offsides = StatComparison(firstValue: firstTeamStat.offsides, secondValue: secondTeamStat.offsides, name: "Offsides")
        let freeKicks = StatComparison(firstValue: firstTeamStat.free_kick, secondValue: secondTeamStat.free_kick, name: "Free Kicks")
        let corners = StatComparison(firstValue: firstTeamStat.corners, secondValue: secondTeamStat.corners, name: "Corners")
        let penalties = StatComparison(firstValue: firstTeamStat.penalties, secondValue: secondTeamStat.penalties, name: "Penalties")
        let yellowCards = StatComparison(firstValue: firstTeamStat.yellowcards, secondValue: secondTeamStat.yellowcards, name: "Yellow Cards")
        let redCards = StatComparison(firstValue: firstTeamStat.redcards, secondValue: secondTeamStat.redcards, name: "Red Cards")
        let shotsOnGoal = StatComparison(firstValue: firstTeamStat.shots?.ongoal, secondValue: secondTeamStat.shots?.ongoal, name: "On Goal")
        let shotsOffGoal = StatComparison(firstValue: firstTeamStat.shots?.offgoal, secondValue: secondTeamStat.shots?.offgoal, name: "Off Goal")
        let shotsInsideBox = StatComparison(firstValue: firstTeamStat.shots?.insidebox, secondValue: secondTeamStat.shots?.insidebox, name: "Inside Box")
        let shotsOutsideBox = StatComparison(firstValue: firstTeamStat.shots?.outsidebox, secondValue: secondTeamStat.shots?.outsidebox, name: "Outside Box")
        let accuratePasses = StatComparison(firstValue: firstTeamStat.passes?.accurate, secondValue: secondTeamStat.passes?.accurate, name: "Accurate")
        let passesPercentage = StatComparison(firstValue: firstTeamStat.getPassPercentage(), secondValue: secondTeamStat.getPassPercentage(), name: "Percentage")
        let attacks = StatComparison(firstValue: firstTeamStat.attacks?.attacks, secondValue: firstTeamStat.attacks?.attacks, name: "Total")
        let dangerousAttacks = StatComparison(firstValue: firstTeamStat.attacks?.dangerous_attacks, secondValue: secondTeamStat.attacks?.dangerous_attacks, name: "Dangerous")
        let saves = StatComparison(firstValue: firstTeamStat.saves, secondValue: secondTeamStat.saves, name: "Saves")
        
        return ["Top Stats": [possession, goals, totalShots, totalPasses, saves, offsides], "Shots": [shotsOnGoal, shotsOffGoal, shotsInsideBox, shotsOutsideBox], "Passes": [accuratePasses, passesPercentage], "Set Pieces": [corners, freeKicks], "Attacks": [attacks, dangerousAttacks], "Discipline": [fouls, yellowCards, redCards]]
    }
    
    static func curateListOfTeamProfileStats(teamStats: TeamStatNumebrs) -> [String: [StatDisplay]] {
        
        let games = StatDisplay(statName: "Games", statValue: teamStats.getTotalGames())
        let goalsFor = StatDisplay(statName: "Goals", statValue: teamStats.goals_for?.total?.intValue)
        let goalsConceded = StatDisplay(statName: "Goals Conceded", statValue: teamStats.goals_against?.total?.intValue)
        let averagePossession = StatDisplay(statName: "Average Possession", statValue: teamStats.avg_ball_possession_percentage?.intValue)
        let gamesWon = StatDisplay(statName: "Won", statValue: teamStats.win?.total?.intValue)
        let gamesLost = StatDisplay(statName: "Lost", statValue: teamStats.lost?.total?.intValue)
        let gamesDraw = StatDisplay(statName: "Draw", statValue: teamStats.draw?.total?.intValue)
        let goalsHome = StatDisplay(statName: "Home", statValue: teamStats.goals_for?.home?.intValue)
        let goalsAway = StatDisplay(statName: "Away", statValue: teamStats.goals_for?.away?.intValue)
        let avgGoalsPerGameScored =  StatDisplay(statName: "Avg goals/Game", statValue: teamStats.avg_goals_per_game_scored?.total?.intValue)
        let avgGoalsPerGameConceded = StatDisplay(statName: "Avg goals conceded/Game", statValue: teamStats.avg_goals_per_game_conceded?.total?.intValue)
        let attacks = StatDisplay(statName: "Attacks", statValue: teamStats.attacks?.intValue)
        let dangerousAttacks = StatDisplay(statName: "Dangerous Attacks", statValue: teamStats.dangerous_attacks?.intValue)
        let shotsOnTarget = StatDisplay(statName: "Shots on target", statValue: teamStats.shots_on_target?.intValue)
        let shotsOffTarget = StatDisplay(statName: "Shots off target", statValue: teamStats.shots_off_target?.intValue)
        let avgShotsOnTarget = StatDisplay(statName: "Avg shots on/Game", statValue: teamStats.avg_shots_on_target_per_game?.intValue)
        let avgShotsOffTarget = StatDisplay(statName: "Avg shots off/Game", statValue: teamStats.avg_shots_off_target_per_game?.intValue)
        let offsides = StatDisplay(statName: "Offsides", statValue: teamStats.offsides?.intValue)
        let corners = StatDisplay(statName: "Corners", statValue: teamStats.avg_corners?.intValue)
        let fouls = StatDisplay(statName: "Fouls", statValue: teamStats.fouls?.intValue)
        let avgFouls = StatDisplay(statName: "Avg fouls/Game", statValue: teamStats.avg_fouls_per_game?.intValue)
        let shotsBlocked = StatDisplay(statName: "Shots Blocked", statValue: teamStats.shots_blocked?.intValue)
        let redcards = StatDisplay(statName: "Red Cards", statValue: teamStats.redcards?.intValue)
        let yellowcards = StatDisplay(statName: "Yellow Cards", statValue: teamStats.yellowcards?.intValue)
        return ["TOP STATS": [games, goalsFor, goalsConceded, averagePossession], "GAMES": [gamesWon, gamesDraw, gamesLost], "GOALS": [goalsHome, goalsAway, avgGoalsPerGameScored, avgGoalsPerGameConceded], "OFFENSE": [attacks, dangerousAttacks, shotsOnTarget, avgShotsOnTarget, shotsOffTarget, avgShotsOffTarget, offsides, corners], "DEFENSE": [shotsBlocked, fouls, avgFouls], "DISCIPLINE": [yellowcards, redcards]]
    }
    
     static func convertStatDictToListOfStats(stats: NSDictionary) ->  [String: [StatComparison]] {
        var finalDict = [String: [StatComparison]]()
        if let statsData = stats as? [String: [NSDictionary]] {
            for key in statsData.keys {
                var statsArray = [StatComparison]()
                if let listOfStats = stats[key] as? [NSDictionary] {
                    for stat in listOfStats {
                        let statToAdd = StatComparison(data: stat)
                        statsArray.append(statToAdd)
                    }
                    
                    finalDict[key] = statsArray
                }
            }
        }
        
        return finalDict

    }
    
    static func currentDatePlusMonths(months: Int) -> Date? {
        return Calendar.current.date(byAdding: .month, value: months, to: Date())
    }
    
    static func currentDateMinusMonths(months: Int) -> Date? {
        return Calendar.current.date(byAdding: .month, value: months, to: Date())
    }
    
    static func dateAsStringForFixtures(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let dateAsString = dateFormatter.string(from: date)
        let components = dateAsString.components(separatedBy: "/")
        return "\(components[2])-\(components[0])-\(components[1])"
    }
    
    
    
    

}
