//
//  Protocols.swift
//  Faneever
//
//  Created by James Meli on 3/3/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

protocol PostCellDelegate {
    func didPlayVideo(videoUrl: URL)
    func didSelectPlayerStats(firstPlayer: Player, secondPlayer: Player, statsDict: [String: [StatComparison]], indexPath: IndexPath)
    func didSelectTeamStats(firstTeam: Team, secondTeam: Team, statsDict: [String: [StatComparison]])
    func wantsToReplyToPost(post: Post)
}

protocol ComparePlayerDelegate {
    func didPickPlayer(player: Player)
    func wantsToChangeFirstPlayerSeason(playerId: Int)
    func wantsToChangeSecondPlayerSeason(playerId: Int)
    func didChangeFirstPlayerSeason(playerStats: PlayerStatNumbers)
    func didChangeSecondPlayerSeason(playerStats: PlayerStatNumbers)
    func wantsToChangePlayer()
}

protocol CompareTeamDelegate {
    func didPickTeam(team: Team)
    func wantsToChangeTeam()
}

protocol CreatePostDelegate {
    func didFinishChoosingPlayersToCompare(firstPlayer: Player, secondPlayer: Player, playerStats: [String: [StatComparison]])
    func didFinishChoosingTeamsToCompare(firstTeam: Team, secondTeam: Team, teamStats: [String: [StatComparison]])
}

protocol FollowingDelegate {
    func didSelectFixture(fixture: Fixture)
    func didSelectLeague(league: League)
    func didSelectTeam(teamId: Int)
    
}

@objc protocol LeagueProfileDelegate {
    @objc optional func didSelectSeeAllPlayers(header: String)
    @objc optional func didSelectPlayer(player: PlayerStatNumbers)
    @objc optional func didSelectTeam(teamId: Int)
}

protocol TeamProfileDelegate {
    func didSelectFixture(fixture: Fixture)
    func didSelectLeague(league: League)
    func didSelectTeam(teamId: Int)
}

protocol SearchDelegate {
    func didTapOnSearchResult(player: Player?, team: Team?, league: League?) 

    
}
