//
//  SettingsVC.swift
//  Faneever
//
//  Created by James Meli on 3/16/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import Firebase

class SettingsVC: UITableViewController {

    @IBOutlet weak var logOutText: UILabel!
    @IBOutlet weak var cell1: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        configureUiElements()
    }
    
    private func configureNavBar() {
        navigationController?.navigationBar.barTintColor = Colors.secondaryBackground
        navigationItem.title = "Settings"
        navigationController?.navigationBar.titleTextAttributes = Constants.navBarTitleAttributes
        let dismissButton = UIBarButtonItem(image: UIImage(systemName: "xmark"), style: .done, target: self, action: #selector(dismissView))
        dismissButton.tintColor = Colors.primaryText
        navigationItem.leftBarButtonItem = dismissButton
    }
    
    private func configureUiElements() {
        tableView.backgroundColor = Colors.alternatePrimaryBackground
        cell1.contentView.backgroundColor = Colors.secondaryBackground
        logOutText.textColor = UIColor(hexString: "FF1744")
    }
    
    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
    private func logOutUser() {
        do {
            try Auth.auth().signOut()
            let welcomeVC = storyboard?.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
            welcomeVC.modalPresentationStyle = .fullScreen
            welcomeVC.modalTransitionStyle = .crossDissolve
            UserDefaults.standard.set(false, forKey: "isLoggedIn")
            present(welcomeVC, animated: true, completion: nil)
        } catch let logOutError {
            print("Error logging out: \(logOutError.localizedDescription)")
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        logOutUser()
    }
    
    



}
