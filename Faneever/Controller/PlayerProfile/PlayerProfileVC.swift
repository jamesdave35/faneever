//
//  PlayerProfileVC.swift
//  Faneever
//
//  Created by James Meli on 5/7/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import EZYGradientView

class PlayerProfileVC: UIViewController {

    @IBOutlet weak var seasonLabel: PaddingLabel!
    @IBOutlet weak var profileLabel: PaddingLabel!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var headerView: EZYGradientView!
    
    weak var playerInfoVC: PlayerInfoVC!
    weak var playerSeasonVC: PlayerSeasonVC!
    weak var pageViewController: PlayerProfilePVC!
    
    var viewControllers = [UIViewController]()
    
    var currentIndex = 0
    var player: PlayerStatNumbers?
    var alternatePlayer: Player?
    var followButton: UIImageView!
    var notificationButton: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        configureUiElements()
        setUpPlayerInfo()
        configurePageViewController()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = Colors.secondaryBackground
    }
    
    private func configureNavBar() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
        followButton = UIImageView(image: UIImage(systemName: "heart", withConfiguration: UIImage.SymbolConfiguration(pointSize: 22, weight: .light)))
        followButton.contentMode = .scaleAspectFit
        followButton.tintColor = .white
        notificationButton = UIImageView(image: UIImage(systemName: "bell", withConfiguration: UIImage.SymbolConfiguration(pointSize: 22, weight: .light)))
        notificationButton.tintColor = .white
        notificationButton.contentMode = .scaleAspectFit
        let stackView = UIStackView(arrangedSubviews: [followButton, notificationButton])
        stackView.distribution = .equalSpacing
        stackView.axis = .horizontal
        stackView.spacing = 20
        let rightBarButtonItem = UIBarButtonItem(customView: stackView)
        navigationItem.rightBarButtonItem = rightBarButtonItem
        
    }
    
    private func configureUiElements() {
        view.backgroundColor = Colors.alternatePrimaryBackground
        playerName.textColor = Colors.primaryText
        profileLabel.textColor = .white
        profileLabel.fillColor = Colors.greenBrandColor
        profileLabel.cornerRadius = 16
        seasonLabel.textColor = Colors.greenBrandColor
        seasonLabel.fillColor = .clear
        seasonLabel.borderColor = Colors.greenBrandColor
        profileLabel.borderWidth = 1
        seasonLabel.borderWidth = 1
        seasonLabel.cornerRadius = 16
        
        headerView.firstColor = Colors.playerProfileHeader
        headerView.secondColor = Colors.secondaryBackground
        headerView.angleº = 0
        headerView.colorRatio = 0.3
        headerView.fadeIntensity = 1
        
        headerView.applyShadow(cornerRadius: 0, shadowOpacity: 0.1, radius: 7, offset: .zero)
        
        let profileGesture = UITapGestureRecognizer(target: self, action: #selector(goToPlayerProfile))
        let seasonGesture = UITapGestureRecognizer(target: self, action: #selector(goToPlayerSeason))
        profileLabel.addGestureRecognizer(profileGesture)
        seasonLabel.addGestureRecognizer(seasonGesture)

    }
    
    private func setUpPlayerInfo() {
        if let player = player {
            if let name = player.player?.data?.display_name {
                playerName.text = name
            }
            
            if let url = player.player?.data?.getImageURL() {
                playerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
        } else if let altPlayer = alternatePlayer {
            if let name = altPlayer.display_name {
                playerName.text = name
            }
            
            if let url = altPlayer.getImageURL() {
                playerImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
        }

    }
    
    private func configurePageViewController() {
         pageViewController = storyboard?.instantiateViewController(withIdentifier: "PlayerProfilePVC") as! PlayerProfilePVC
         pageViewController.delegate = self
         pageViewController.dataSource = self
         addChild(pageViewController)
         pageViewController.didMove(toParent: self)
         pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
         view.addSubview(pageViewController.view)
         view.bringSubviewToFront(headerView)
         let topConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .top, relatedBy: .equal, toItem: self.headerView, attribute: .bottom, multiplier: 1, constant: 0)
         let bottomConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
         let leftConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
         let rightConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
         self.view.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
        
         playerInfoVC = storyboard?.instantiateViewController(withIdentifier: "PlayerInfoVC") as! PlayerInfoVC
         playerSeasonVC = storyboard?.instantiateViewController(withIdentifier: "PlayerSeasonVC") as! PlayerSeasonVC
        
         if let player = player {
             playerInfoVC.player = player
             playerSeasonVC.player = player
         }
        
         viewControllers = [playerInfoVC, playerSeasonVC]
        
         pageViewController.setViewControllers([playerInfoVC], direction: .forward, animated: true, completion: nil)
    }
    
    private func jump(to: Int) {
         let toVC = viewControllers[to]
         var direction: UIPageViewController.NavigationDirection = .forward

        if currentIndex < to {
            direction = .forward
        } else {
            direction = .reverse
        }

        pageViewController.setViewControllers([toVC], direction: direction, animated: true) { _ in
            DispatchQueue.main.async {
                self.pageViewController.setViewControllers([toVC], direction: direction, animated: true, completion: nil)
            }
        }
    }
    
    private func configureSelectedLabel(label: PaddingLabel) {
        label.fillColor = Colors.greenBrandColor
        label.textColor = .white
        label.borderColor = .clear
    }
    
    private func configureUnSelectedLabel(label: PaddingLabel) {
        label.fillColor = .clear
        label.textColor = Colors.greenBrandColor
        label.borderColor = Colors.greenBrandColor
    }
    
    @objc private func goToPlayerProfile() {
        jump(to: 0)
        configureSelectedLabel(label: profileLabel)
        configureUnSelectedLabel(label: seasonLabel)
        currentIndex = 0
    }
    
    @objc private func goToPlayerSeason() {
        jump(to: 1)
        configureSelectedLabel(label: seasonLabel)
        configureUnSelectedLabel(label: profileLabel)
        currentIndex = 1
    }
    



}
extension PlayerProfileVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if viewController is PlayerInfoVC {
            return nil
        } else {
            playerInfoVC = storyboard?.instantiateViewController(withIdentifier: "PlayerInfoVC") as! PlayerInfoVC
            if let player = self.player {
                playerInfoVC.player = player
            }
            configureUnSelectedLabel(label: seasonLabel)
            configureSelectedLabel(label: profileLabel)
            currentIndex = 0
            return playerInfoVC
        }

    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        if viewController is PlayerInfoVC {
            playerSeasonVC = storyboard?.instantiateViewController(withIdentifier: "PlayerSeasonVC") as! PlayerSeasonVC
            if let player = player {
                playerSeasonVC.player = player
            }
            
            configureUnSelectedLabel(label: profileLabel)
            configureSelectedLabel(label: seasonLabel)
            currentIndex = 1
            return playerSeasonVC
        } else {
            return nil
        }
        
    }
}
