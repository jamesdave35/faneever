//
//  PlayerInfoVC.swift
//  Faneever
//
//  Created by James Meli on 5/7/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class PlayerInfoVC: UITableViewController {

    @IBOutlet weak var shadowViewTwo: AnimatableView!
    @IBOutlet weak var shadowViewOne: AnimatableView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var positionLabel: PaddingLabel!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var holderViewTwo: UIView!
    @IBOutlet weak var shirtLabel: PaddingLabel!
    @IBOutlet weak var shirt: UILabel!
    @IBOutlet weak var ageLabel: PaddingLabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var weightLabel: PaddingLabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var heightLabel: PaddingLabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var secondContainerView: UIView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var teamsLabel: UILabel!
    @IBOutlet weak var holderViewOne: UIView!
    @IBOutlet weak var firstContainerView: UIView!
    
    var player: PlayerStatNumbers?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        setPlayerInfo()
        
    }
    
    private func configureUiElements() {
        tableView.backgroundColor = Colors.primaryBackground
        tableView.tableFooterView = UIView()
        firstContainerView.layer.cornerRadius = 20
        secondContainerView.layer.cornerRadius = 20
        shadowViewOne.layer.cornerCurve = .continuous
        shadowViewTwo.layer.cornerCurve = .continuous
        firstContainerView.layer.cornerCurve = .continuous
        secondContainerView.layer.cornerCurve = .continuous
        firstContainerView.backgroundColor = Colors.secondaryBackground
        secondContainerView.backgroundColor = Colors.secondaryBackground
        //applyShadows()
        teamsLabel.textColor = Colors.primaryText
        teamName.textColor = Colors.secondaryText
        countryName.textColor = Colors.secondaryText
        holderViewTwo.backgroundColor = Colors.tertiaryBackgroundColor
        holderViewOne.backgroundColor = Colors.tertiaryBackgroundColor
        infoLabel.textColor = Colors.primaryText
        heightLabel.textColor = Colors.secondaryText
        heightLabel.fillColor = Colors.tertiaryBackgroundColor
        weightLabel.textColor = Colors.secondaryText
        weightLabel.fillColor = Colors.tertiaryBackgroundColor
        ageLabel.textColor = Colors.secondaryText
        ageLabel.fillColor = Colors.tertiaryBackgroundColor
        shirtLabel.textColor = Colors.secondaryText
        shirtLabel.fillColor = Colors.tertiaryBackgroundColor
        positionLabel.textColor = Colors.secondaryText
        positionLabel.fillColor = Colors.tertiaryBackgroundColor
        height.textColor = Colors.primaryText
        weight.textColor = Colors.primaryText
        age.textColor = Colors.primaryText
        shirt.textColor = Colors.primaryText
        position.textColor = Colors.primaryText
        
        let goToTeamGestureOne = UITapGestureRecognizer(target: self, action: #selector(goToTeamProfile))
        let goToTeamGestureTwo = UITapGestureRecognizer(target: self, action: #selector(goToTeamProfile))
        teamLogo.isUserInteractionEnabled = true
        teamName.isUserInteractionEnabled = true
        teamLogo.addGestureRecognizer(goToTeamGestureOne)
        teamName.addGestureRecognizer(goToTeamGestureTwo)
        
        
    }
    
    private func applyShadows() {
        shadowViewOne.applyShadow(cornerRadius: 20, shadowOpacity: 0.1, radius: 7, offset: CGSize(width: 3, height: 3))
        shadowViewTwo.applyShadow(cornerRadius: 20, shadowOpacity: 0.1, radius: 7, offset: CGSize(width: 3, height: 3))
    }
    
    private func setPlayerInfo() {
        if let player = player {
            if let name = player.getPlayer()?.getTeamName(), let teamLogoUrl = player.getPlayer()?.getTeamLogoUrl() {
                
                teamName.text = name
                teamLogo.sd_setImage(with: teamLogoUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let name = player.getPlayer()?.getCountryName(), let countryFlagUrl = player.getPlayer()?.getCountryFlagUrl() {
                countryName.text = name
                countryFlag.sd_setImage(with: countryFlagUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let pHeight = player.getPlayer()?.height, let pWeight = player.getPlayer()?.weight, let pAge = player.getPlayer()?.getPlayerAge(), let pPosition = player.getPlayer()?.getPositionName() {
                
                height.text = pHeight
                weight.text = pWeight
                age.text = "\(pAge)"
                position.text = pPosition
                shirt.text = "10"
                
            }
        }
    }
    
    @objc private func goToTeamProfile() {
        let teamProfileVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileVC") as! TeamProfileVC
        teamProfileVC.teamId = player?.getPlayer()?.team_id
        navigationController?.pushViewController(teamProfileVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 190
        } else {
            return 290
        }
    }



}
