//
//  PlayerSeasonVC.swift
//  Faneever
//
//  Created by James Meli on 5/7/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class PlayerSeasonVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var player: PlayerStatNumbers?
    var statsDict: [String: [StatDisplay]] = [:]
    var league: League?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        displayPlayerStats()

    }
    
    private func configureUiElements() {
        view.backgroundColor = Colors.alternatePrimaryBackground
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func displayPlayerStats() {
        if let playerId = player?.player?.data?.player_id, let seasonId = player?.player?.data?.team?.data?.current_season_id {
            FootballApi.getPlayerSeasonStats(playerId: playerId, seasonId: seasonId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let playerStats):
                    self?.statsDict = GlobalFunctions.curateListOfPlayerProfileStats(player: playerStats)
                    self?.league = playerStats.league?.data
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
            
        }
    }
    



}

extension PlayerSeasonVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return statsDict.keys.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            let keys = rearrangeKeys(keys: Array(statsDict.keys))
            if let count = statsDict[keys[section - 1].key]?.count {
                return count + 1
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerSeasonLeagueCell", for: indexPath) as! PlayerSeasonLeagueCell
            if let league = self.league {
                cell.league = league
            }
            return cell
        } else {
            let keys = rearrangeKeys(keys: Array(statsDict.keys))
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerSeasonHeaderCell", for: indexPath) as! PlayerSeasonHeaderCell
                cell.header = keys[indexPath.section - 1].key
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerSeasonStatCell", for: indexPath) as! PlayerSeasonStatCell
                if let stats = statsDict[keys[indexPath.section - 1].key] {
                    cell.stat = stats[indexPath.row - 1]
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 60
        } else {
            if indexPath.row == 0 {
                return 55
            } else {
                return 68
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let leagueProfileVC = storyboard?.instantiateViewController(withIdentifier: "LeagueProfileVC") as! LeagueProfileVC
            leagueProfileVC.league = self.league
            navigationController?.pushViewController(leagueProfileVC, animated: true)
            
        }
    }
    
}

extension PlayerSeasonVC {
    
    func rearrangeKeys(keys: Array<String>) -> [StatKey] {
        var newKeys = [StatKey]()
        for key in keys {
            if key == "TOP STATS" {
                newKeys.append(StatKey(key: key, priority: 0))
            } else if key == "DISTRIBUTION" {
                newKeys.append(StatKey(key: key, priority: 1))
            } else if key == "TAKE ONS" {
                newKeys.append(StatKey(key: key, priority: 2))
            } else if key == "DEFENSE" {
                newKeys.append(StatKey(key: key, priority: 3))
            } else if key == "PENALTIES" {
                newKeys.append(StatKey(key: key, priority: 4))
            } else if key == "SAVES" {
                newKeys.append(StatKey(key: key, priority: 5))
            } else {
                newKeys.append(StatKey(key: key, priority: 6))
            }
        }
        
        return newKeys.sorted(by: {$0.priority < $1.priority})
    }
}
