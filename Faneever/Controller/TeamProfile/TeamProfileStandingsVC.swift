//
//  TeamProfileStandingsVC.swift
//  Faneever
//
//  Created by James Meli on 5/7/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class TeamProfileStandingsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: AnimatableView!
    @IBOutlet weak var teamLabel: PaddingLabel!
    @IBOutlet weak var wLabel: UILabel!
    @IBOutlet weak var plLabel: UILabel!
    @IBOutlet weak var dLabel: UILabel!
    @IBOutlet weak var lLabel: UILabel!
    @IBOutlet weak var ptslabel: UILabel!

    var standings = [Standings]()
    var team: Team?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        getLeagueStandings()
        
    }
    
    private func configureUiElements() {
        self.view.backgroundColor = Colors.primaryBackground
        headerView.backgroundColor = Colors.primaryBackground
        teamLabel.textColor = Colors.secondaryText
        teamLabel.fillColor = Colors.tertiaryBackgroundColor
        wLabel.textColor = Colors.secondaryText
        plLabel.textColor = Colors.secondaryText
        dLabel.textColor = Colors.secondaryText
        lLabel.textColor = Colors.secondaryText
        ptslabel.textColor = Colors.secondaryText
        tableView.separatorColor = Colors.seperatorColor
        tableView.delegate = self
        tableView.dataSource = self

    }
    
    private func getLeagueStandings() {
        if let id = team?.current_season_id {
            FootballApi.getLeagueStandings(seasonId: id) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let stans):
                    self?.standings = stans
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        }
    }
    

    

}

extension TeamProfileStandingsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return standings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamProfileStandingsCell", for: indexPath) as! TeamProfileStandingsCell
        cell.standing = standings[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let teamProfileVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileVC") as! TeamProfileVC
        teamProfileVC.teamId = standings[indexPath.row].team_id
        navigationController?.pushViewController(teamProfileVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = Colors.primaryBackground
        let greenView = UIView()
        let yellowView = UIView()
        let redView = UIView()
        greenView.translatesAutoresizingMaskIntoConstraints = false
        yellowView.translatesAutoresizingMaskIntoConstraints = false
        redView.translatesAutoresizingMaskIntoConstraints = false
        let firstHeightConstraint = NSLayoutConstraint(item: greenView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 15)
        let firstWidthConstraint = NSLayoutConstraint(item: greenView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 15)
        let secondHeightConstraint = NSLayoutConstraint(item: yellowView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 15)
        let secondWidthConstraint = NSLayoutConstraint(item: yellowView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 15)
        let thirdHeightConstraint = NSLayoutConstraint(item: redView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 15)
        let thirdWidthConstraint = NSLayoutConstraint(item: redView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 15)
        greenView.layer.cornerRadius = 7.5
        greenView.backgroundColor = .systemGreen
        yellowView.layer.cornerRadius = 7.5
        yellowView.backgroundColor = .systemYellow
        redView.layer.cornerRadius = 7.5
        redView.backgroundColor = .systemRed
        let championsLeagueLabel = UILabel()
        championsLeagueLabel.font = UIFont(name: "Helvetica-Bold", size: 13)
        championsLeagueLabel.textColor = Colors.secondaryText
        championsLeagueLabel.text = "Champions League"
        let europaLeagueLabel = UILabel()
        europaLeagueLabel.font = UIFont(name: "Helvetica-Bold", size: 13)
        europaLeagueLabel.textColor = Colors.secondaryText
        europaLeagueLabel.text = "Europa League"
        let relegationLabel = UILabel()
        relegationLabel.font = UIFont(name: "Helvetica-Bold", size: 13)
        relegationLabel.textColor = Colors.secondaryText
        relegationLabel.text = "Relegation"
        let firstStackView = UIStackView(arrangedSubviews: [greenView, championsLeagueLabel])
        firstStackView.axis = .horizontal
        firstStackView.spacing = 5
        let secondStackView = UIStackView(arrangedSubviews: [yellowView, europaLeagueLabel])
        secondStackView.axis = .horizontal
        secondStackView.spacing = 5
        let thirdStackView = UIStackView(arrangedSubviews: [redView, relegationLabel])
        thirdStackView.axis = .horizontal
        thirdStackView.spacing = 5
        let finalStackView = UIStackView(arrangedSubviews: [firstStackView, secondStackView, thirdStackView])
        finalStackView.axis = .vertical
        finalStackView.spacing = 15
        finalStackView.translatesAutoresizingMaskIntoConstraints = false
        let leadingConstraint = NSLayoutConstraint(item: finalStackView, attribute: .leading, relatedBy: .equal, toItem: footerView, attribute: .leading, multiplier: 1, constant: 15)
        let verticalConstraint = NSLayoutConstraint(item: finalStackView, attribute: .centerY, relatedBy: .equal, toItem: footerView, attribute: .centerY, multiplier: 1, constant: 0)
        footerView.addSubview(finalStackView)
        footerView.addConstraints([leadingConstraint, verticalConstraint, firstWidthConstraint, firstHeightConstraint, secondWidthConstraint, secondHeightConstraint, thirdWidthConstraint, thirdHeightConstraint])
        return footerView
    }
    
}
