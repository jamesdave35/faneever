//
//  TeamProfileVC.swift
//  Faneever
//
//  Created by James Meli on 4/21/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import EZYGradientView

class TeamProfileVC: UIViewController, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var headerView: EZYGradientView!
    
    weak var pageViewController: TeamProfilePVC!
    weak var teamOverviewVC: TeamOverviewVC!
    weak var teamFixturesVC: TeamFixturesVC!
    weak var teamSquadVC: TeamSquadVC!
    weak var teamStatsVC: TeamStatsVC!
    weak var teamStandingsVC: TeamProfileStandingsVC!
    
    var viewControllers = [UIViewController]()
    var currentIndex = 0
    var team: Team?
    var teamId: Int?
    var followButton: UIImageView!
    var notificationButton: UIImageView!
    var selectedMenu = "Overview"
    
    var menu = ["Overview", "Fixtures", "Squad", "Stats", "Table", "Transfers", "Buzz"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        setUpTeamProfile()
        setCellLayout()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = Colors.secondaryBackground
    }
    
    private func configureNavBar() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
        
        followButton = UIImageView(image: UIImage(systemName: "heart", withConfiguration: UIImage.SymbolConfiguration(pointSize: 22, weight: .light)))
        followButton.tintColor = .white
        followButton.contentMode = .scaleAspectFit
        notificationButton = UIImageView(image: UIImage(systemName: "bell", withConfiguration: UIImage.SymbolConfiguration(pointSize: 22, weight: .light)))
        notificationButton.contentMode = .scaleAspectFit
        notificationButton.tintColor = .white
        let stackView = UIStackView(arrangedSubviews: [followButton, notificationButton])
        stackView.distribution = .equalSpacing
        stackView.axis = .horizontal
        stackView.spacing = 20
        let rightBarButtonItem = UIBarButtonItem(customView: stackView)
        navigationItem.rightBarButtonItem = rightBarButtonItem
        
    }
    
    private func configureUiElements() {
        view.backgroundColor = Colors.alternatePrimaryBackground
        collectionView.delegate = self
        collectionView.dataSource = self

    }
    
    private func setCellLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        collectionView.collectionViewLayout = layout
    }
    
    private func configurePageViewController() {
        pageViewController = storyboard?.instantiateViewController(withIdentifier: "TeamProfilePVC") as! TeamProfilePVC
        pageViewController.delegate = self
        pageViewController.dataSource = self
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageViewController.view)
        let topConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .top, relatedBy: .equal, toItem: self.headerView, attribute: .bottom, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        let rightConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        self.view.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
        
        teamOverviewVC = storyboard?.instantiateViewController(withIdentifier: "TeamOverviewVC") as! TeamOverviewVC
        teamFixturesVC = storyboard?.instantiateViewController(withIdentifier: "TeamFixturesVC") as! TeamFixturesVC
        teamSquadVC = storyboard?.instantiateViewController(withIdentifier: "TeamSquadVC") as! TeamSquadVC
        teamStatsVC = storyboard?.instantiateViewController(withIdentifier: "TeamStatsVC") as! TeamStatsVC
        teamStandingsVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileStandingsVC") as! TeamProfileStandingsVC
        if let team = self.team {
            teamOverviewVC.team = team
            teamFixturesVC.team = team
            teamSquadVC.team = team
            teamStatsVC.team = team
            teamStandingsVC.team = team
        }
        
        viewControllers = [teamOverviewVC, teamFixturesVC, teamSquadVC, teamStatsVC, teamStandingsVC]
        
        pageViewController.setViewControllers([teamOverviewVC], direction: .forward, animated: true, completion: nil)
    }
    
    private func jump(to: Int) {
         let toVC = viewControllers[to]
         var direction: UIPageViewController.NavigationDirection = .forward

        if currentIndex < to {
            direction = .forward
        } else {
            direction = .reverse
        }

        pageViewController.setViewControllers([toVC], direction: direction, animated: true) { _ in
            DispatchQueue.main.async {
                self.pageViewController.setViewControllers([toVC], direction: direction, animated: true, completion: nil)
            }
        }
    }
    
    private func setUpTeamProfile() {
        if let name = self.team?.name, let teamUrl = self.team?.getLogoUrl(), let team = self.team {
            createHeaderViewGradient(teamName: name)
            teamName.text = name
            teamLogo.sd_setImage(with: teamUrl, placeholderImage: Constants.defaultTeamImage)
            configureUiElements()
            configurePageViewController()
        } else if let id = teamId {
            FootballApi.getTeamById(id: id) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let teamReturned):
                    DispatchQueue.main.async {
                        if let name = teamReturned?.name, let teamUrl = teamReturned?.getLogoUrl(), let teamToBeAssigned = teamReturned {
                            self?.team = teamToBeAssigned
                            self?.createHeaderViewGradient(teamName: name)
                            self?.teamName.text = name
                            self?.teamLogo.sd_setImage(with: teamUrl, placeholderImage: Constants.defaultTeamImage)
                            self?.configureUiElements()
                            self?.configurePageViewController()
                            
                        }
                    }
                }
            }
        }
    }
    
    @objc private func followButtonPressed() {
        
    }
    
    @objc private func notificationButtonPressed() {
        
    }
    
    private func createHeaderViewGradient(teamName: String) {
          let colorOne: UIColor = {
            if #available(iOS 13, *) {
                return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                    if UITraitCollection.userInterfaceStyle == .dark {
                        /// Return the color for Dark Mode
                        return UIColor(hexString: "2D2D2D")
                    } else {
                        /// Return the color for Light Mode
                        return TeamColors.teamColors[teamName]!.darker(by: 10)
                    }
                }
            } else {
                /// Return a fallback color for iOS 12 and lower.
                return UIColor(hexString: "2D2D2D")
            }
        }()
        
        let colorTwo: UIColor = {
            if #available(iOS 13, *) {
                return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                    if UITraitCollection.userInterfaceStyle == .dark {
                        /// Return the color for Dark Mode
                        return UIColor(hexString: "141414")
                    } else {
                        /// Return the color for Light Mode
                        return TeamColors.teamColors[teamName]!.darker(by: 10)
                    }
                }
            } else {
                /// Return a fallback color for iOS 12 and lower.
                return UIColor(hexString: "202020")
            }
        }()
        
        headerView.firstColor = colorOne
        headerView.secondColor = colorTwo
        headerView.angleº = 0
        headerView.colorRatio = 0.3
        headerView.fadeIntensity = 1
        
    }
    



}

extension TeamProfileVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeamProfileMenuCell", for: indexPath) as! TeamProfileMenuCell
        cell.menu = menu[indexPath.row]
        cell.team = team
        if cell.menu == selectedMenu {
            cell.isSelected = true
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        } else {
            cell.isSelected = false
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalFunctions.playSelectionFeedbackGenerator()
        selectedMenu = menu[indexPath.row]
        jump(to: indexPath.row)
        currentIndex = indexPath.row
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
}

extension TeamProfileVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        if viewController is TeamFixturesVC {
            teamOverviewVC = storyboard?.instantiateViewController(withIdentifier: "TeamOverviewVC") as! TeamOverviewVC
            if let team = self.team {
                teamOverviewVC.team = team
            }
            return teamOverviewVC
        } else if viewController is TeamSquadVC {
            teamFixturesVC = storyboard?.instantiateViewController(withIdentifier: "TeamFixturesVC") as! TeamFixturesVC
            if let team = self.team {
                teamFixturesVC.team = team
            }
            
            return teamFixturesVC
        } else if viewController is TeamStatsVC {
            teamSquadVC = storyboard?.instantiateViewController(withIdentifier: "TeamSquadVC") as! TeamSquadVC
             if let team = team {
                 teamSquadVC.team = team
             }
             return teamSquadVC
        } else if viewController is TeamProfileStandingsVC {
            teamStatsVC = storyboard?.instantiateViewController(withIdentifier: "TeamStatsVC") as! TeamStatsVC
            if let team = team {
                teamStatsVC.team = team
            }
            return teamStatsVC
        } else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        if viewController is TeamOverviewVC {
            teamFixturesVC = storyboard?.instantiateViewController(withIdentifier: "TeamFixturesVC") as! TeamFixturesVC
            if let team = self.team {
                teamFixturesVC.team = team
            }
            
            return teamFixturesVC
        } else if viewController is TeamFixturesVC {
            teamSquadVC = storyboard?.instantiateViewController(withIdentifier: "TeamSquadVC") as! TeamSquadVC
            if let team = team {
                teamSquadVC.team = team
            }
            return teamSquadVC
        } else if viewController is TeamSquadVC {
            teamStatsVC = storyboard?.instantiateViewController(withIdentifier: "TeamStatsVC") as! TeamStatsVC
            if let team = team {
                teamStatsVC.team = team
            }
            return teamStatsVC
        } else if viewController is TeamStatsVC {
            teamStandingsVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileStandingsVC") as! TeamProfileStandingsVC
            if let team = team {
                teamStandingsVC.team = team
            }
            return teamStandingsVC
        } else {
            return nil
        }
        
    }
}
