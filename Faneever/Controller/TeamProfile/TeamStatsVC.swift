//
//  TeamStatsVC.swift
//  Faneever
//
//  Created by James Meli on 5/7/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamStatsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var team: Team?
    
    var statsDict: [String: [StatDisplay]] = [:]
    var statNumbers: TeamStatNumebrs?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        getTeamMainSeasonStats()

    }
    
    private func configureUiElements() {
        view.backgroundColor = Colors.alternatePrimaryBackground
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func getTeamMainSeasonStats() {
        if let teamId = team?.id {
            FootballApi.getTeamMainSeasonStats(teamId: teamId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let statNumbers):
                    self?.statsDict = GlobalFunctions.curateListOfTeamProfileStats(teamStats: statNumbers)
                    self?.statNumbers = statNumbers
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        }
    }





}

extension TeamStatsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return statsDict.keys.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            let keys = rearrangeKeys(keys: Array(statsDict.keys))
            if let count = statsDict[keys[section - 1].key]?.count {
                return count + 1
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamProfileStatLeagueCell", for: indexPath) as! TeamProfileStatLeagueCell
            if let league = self.statNumbers?.season?.data?.league?.data {
                cell.league = league
            }
            return cell
        } else {
            let keys = rearrangeKeys(keys: Array(statsDict.keys))
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TeamProfileStatHeaderCell", for: indexPath) as! TeamProfileStatHeaderCell
                cell.header = keys[indexPath.section - 1].key
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TeamProfileStatCell", for: indexPath) as! TeamProfileStatCell
                if let stats = statsDict[keys[indexPath.section - 1].key] {
                    cell.stat = stats[indexPath.row - 1]
                }
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 60
        } else {
            if indexPath.row == 0 {
                return 55
            } else {
                return 68
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let leagueProfileVC = storyboard?.instantiateViewController(withIdentifier: "LeagueProfileVC") as! LeagueProfileVC
            leagueProfileVC.league = self.self.statNumbers?.season?.data?.league?.data
            navigationController?.pushViewController(leagueProfileVC, animated: true)
            
        }
    }
}

extension TeamStatsVC {
    
    func rearrangeKeys(keys: Array<String>) -> [StatKey] {
        var newKeys = [StatKey]()
        for key in keys {
            if key == "TOP STATS" {
                newKeys.append(StatKey(key: key, priority: 0))
            } else if key == "GAMES" {
                newKeys.append(StatKey(key: key, priority: 1))
            } else if key == "GOALS" {
                newKeys.append(StatKey(key: key, priority: 2))
            } else if key == "OFFENSE" {
                newKeys.append(StatKey(key: key, priority: 3))
            } else if key == "DEFENSE" {
                newKeys.append(StatKey(key: key, priority: 4))
            } else {
                newKeys.append(StatKey(key: key, priority: 5))
            }
        }
        
        return newKeys.sorted(by: {$0.priority < $1.priority})
    }
}
