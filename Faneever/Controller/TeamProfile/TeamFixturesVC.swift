//
//  TeamFixturesVC.swift
//  Faneever
//
//  Created by James Meli on 4/22/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamFixturesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var pastFixtures = [Fixture]()
    var upcomingFixtures = [Fixture]()
    var team: Team?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        getFixtures()
    }
    
    private func configureUiElements() {
        self.view.backgroundColor = Colors.primaryBackground
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = Colors.seperatorColor
    }
    
    private func getFixtures() {
        if let teamId = team?.id {
            FootballApi.getTeamFixtures(teamId: teamId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let fixs):
                    for fix in fixs {
                        if let date = fix.getSchedule()?.getDate() {
                            if date >= Date() {
                                self?.upcomingFixtures.append(fix)
                            } else {
                                self?.pastFixtures.append(fix)
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                        self?.tableView.scrollToRow(at: IndexPath(row: 0, section: 1), at: .none, animated: true)
                    }
                }
            }
        }
    }
    


}

extension TeamFixturesVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return pastFixtures.count
        } else {
            return upcomingFixtures.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamProfileFixtureCell", for: indexPath) as! TeamProfileFixtureCell
        if indexPath.section == 0 {
            cell.fixture = pastFixtures[indexPath.row]
        } else {
            cell.fixture = upcomingFixtures[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = Colors.alternatePrimaryBackground
        let headerLabel = UILabel()
        headerLabel.textColor = Colors.secondaryText
        headerLabel.font = UIFont(name: "Helvetica-Bold", size: 13)
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        let verticalConstraint = NSLayoutConstraint(item: headerLabel, attribute: .centerY, relatedBy: .equal, toItem: headerView, attribute: .centerY, multiplier: 1, constant: 0)
        let leadingConstraint = NSLayoutConstraint(item: headerLabel, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1, constant: 15)
        headerView.addSubview(headerLabel)
        headerView.addConstraints([verticalConstraint, leadingConstraint])
        if section == 0 {
            headerLabel.text = "PAST"
        } else {
            headerLabel.text = "UPCOMING"
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fixtureVC = storyboard?.instantiateViewController(withIdentifier: "FixtureVC") as! FixtureVC
        let cell = tableView.cellForRow(at: indexPath) as! TeamProfileFixtureCell
        fixtureVC.fixtureId = cell.fixture?.id
        fixtureVC.fixture = cell.fixture
        navigationController?.pushViewController(fixtureVC, animated: true)
    }
}
