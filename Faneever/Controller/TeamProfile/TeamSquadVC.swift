//
//  TeamSquadVC.swift
//  Faneever
//
//  Created by James Meli on 4/23/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class TeamSquadVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var team: Team?
    var goalkeepers = [PlayerStatNumbers]()
    var defenders = [PlayerStatNumbers]()
    var midfielders = [PlayerStatNumbers]()
    var attackers = [PlayerStatNumbers]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUiElements()
        getTeamSquad()
    }
    
    private func configureUiElements() {
        self.view.backgroundColor = Colors.alternatePrimaryBackground
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    private func getTeamSquad() {
        if let teamId = team?.id, let seasonId = team?.current_season_id {
            FootballApi.getTeamSquad(teamId: teamId, seasonId: seasonId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let players):
                    for player in players {
                        if player.isGoalkeeper() {
                            self?.goalkeepers.append(player)
                        } else if player.isDefender() {
                            self?.defenders.append(player)
                        } else if player.isMidfielder() {
                            self?.midfielders.append(player)
                        } else {
                            self?.attackers.append(player)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        }
    }
    


}

extension TeamSquadVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return goalkeepers.count
        case 1:
            return defenders.count
        case 2:
            return midfielders.count
        default:
            return attackers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamSquadCell", for: indexPath) as! TeamSquadCell
        switch indexPath.section {
        case 0:
            cell.player = goalkeepers[indexPath.row]
        case 1:
            cell.player = defenders[indexPath.row]
        case 2:
            cell.player = midfielders[indexPath.row]
        default:
            cell.player = attackers[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
       let headerView = UIView()
       headerView.backgroundColor = Colors.alternatePrimaryBackground
       let headerLabel = UILabel()
       headerLabel.textColor = Colors.secondaryText
       headerLabel.font = UIFont(name: "Helvetica-Bold", size: 13)
        switch section {
        case 0:
            headerLabel.text = "GOALKEEPERS"
        case 1:
            headerLabel.text = "DEFENDERS"
        case 2:
            headerLabel.text = "MIDFIELDERS"
        default:
            headerLabel.text = "ATTACKERS"
        }
       
       headerLabel.translatesAutoresizingMaskIntoConstraints = false
       let verticalConstraint = NSLayoutConstraint(item: headerLabel, attribute: .centerY, relatedBy: .equal, toItem: headerView, attribute: .centerY, multiplier: 1, constant: 0)
       let leadingConstraint = NSLayoutConstraint(item: headerLabel, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1, constant: 15)
       headerView.addSubview(headerLabel)
       headerView.addConstraints([verticalConstraint, leadingConstraint])
       return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playerProfileVC = storyboard?.instantiateViewController(withIdentifier: "PlayerProfileVC") as! PlayerProfileVC
        var player: PlayerStatNumbers!
        switch indexPath.section {
        case 0:
            player = goalkeepers[indexPath.row]
        case 1:
            player = defenders[indexPath.row]
        case 2:
            player = midfielders[indexPath.row]
        default:
            player = attackers[indexPath.row]
        }
        
        playerProfileVC.player = player
        navigationController?.pushViewController(playerProfileVC, animated: true)
    }
}
