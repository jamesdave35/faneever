//
//  ViewController.swift
//  Faneever
//
//  Created by James Meli on 2/27/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable


class WelcomeVC: UIViewController {
    
    @IBOutlet weak var joinNowButton: AnimatableButton!
    @IBOutlet weak var signInButton: AnimatableButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        configureUiElements()
       
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func configureUiElements() {
        joinNowButton.roundCorners()
        
    }
    

    
    private func presentSignUpVC() {
        GlobalFunctions.playSelectionFeedbackGenerator()
        let signUpVc = storyboard?.instantiateViewController(withIdentifier: "CreateAccountVC") as! CreateAccountVC
        signUpVc.modalPresentationStyle = .currentContext
        self.present(signUpVc, animated: true, completion: nil)
        
    }
    
    private func presentSignInVC() {
        GlobalFunctions.playSelectionFeedbackGenerator()
        let signInVc = storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        signInVc.modalPresentationStyle = .currentContext
        self.present(signInVc, animated: true, completion: nil)
        
    }
    


    @IBAction func joinNowPressed(_ sender: Any) {
        presentSignUpVC()
    }
    
    @IBAction func signInPressed(_ sender: Any) {
        presentSignInVC()
    }
}

