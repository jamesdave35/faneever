//
//  CreateAccountVC.swift
//  Faneever
//
//  Created by James Meli on 2/27/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import IBAnimatable
import FirebaseAuth

class CreateAccountVC: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var signUpButton: AnimatableButton!
    @IBOutlet weak var passwordTextField: ACFloatingTextfield!
    @IBOutlet weak var emailTextField: ACFloatingTextfield!
    @IBOutlet weak var usernameTextField: ACFloatingTextfield!
    @IBOutlet weak var cancelButton: UIImageView!
    @IBOutlet weak var displayNameTextField: ACFloatingTextfield!
    @IBOutlet weak var termsLabel: UILabel!
    
    var userId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        
    }
    
    private func configureUiElements() {
        let cancelGesture = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        let dismissKeyboardGesture = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        cancelButton.addGestureRecognizer(cancelGesture)
        self.view.addGestureRecognizer(dismissKeyboardGesture)
        displayNameTextField.delegate = self
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        emailTextField.delegate = self
        displayNameTextField.addTarget(self, action: #selector(textFieldChangedEditing), for: .editingChanged)
        usernameTextField.addTarget(self, action: #selector(textFieldChangedEditing), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(textFieldChangedEditing), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldChangedEditing), for: .editingChanged)
        signUpButton.roundCorners()

    }
    
    @objc private func textFieldChangedEditing() {
        if allRequiredFieldsFilled() {
            enableSignUpButton()
        } else {
            disableSignUpButton()
        }
    }
    
    @objc private func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func enableSignUpButton() {
        signUpButton.isEnabled = true
        signUpButton.fillColor = Colors.greenBrandColor
    }
    
    private func disableSignUpButton() {
        signUpButton.isEnabled = false
        signUpButton.fillColor = Colors.disabledButtonColor
    }
    
    private func showActivityIndicator() {
        signUpButton.setTitle("", for: .normal)
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    private func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        signUpButton.setTitle("SIGN UP", for: .normal)
    }
    
    private func allRequiredFieldsFilled() -> Bool {
        if displayNameTextField.text! == "" || usernameTextField.text! == "" || emailTextField.text == "" || passwordTextField.text!.count < 6 {
            return false
        } else {
            return true
        }
    }
    
    private func presentAddPhotoVC() {
        let addPhotoVC = storyboard?.instantiateViewController(withIdentifier: "AddPhotoVC") as! AddPhotoVC
        addPhotoVC.modalPresentationStyle = .currentContext
        addPhotoVC.userId = self.userId
        self.present(addPhotoVC, animated: true, completion: nil)
    }
    
    private func saveUserToDatabase() {
        self.view.endEditing(true)
        showActivityIndicator()
        AuthServices.createUser(withEmail: emailTextField.text!, andPassword: passwordTextField.text!) { (result, error) in
            if error != nil {
                self.hideActivityIndicator()
                if let errCode = AuthErrorCode(rawValue: error!._code) {
                    switch errCode {
                    case .userNotFound:
                        self.emailTextField.showErrorWithText(errorText: "User not found")
                    case .invalidEmail:
                        self.emailTextField.showErrorWithText(errorText: "Invalid Email")
                    case .wrongPassword:
                        self.passwordTextField.showErrorWithText(errorText: "Wrong password")
                    case .emailAlreadyInUse:
                        self.emailTextField.showErrorWithText(errorText: "Email already in use")
                    case .networkError:
                        self.emailTextField.showErrorWithText(errorText: "Network error")
                    default:
                        print("unknown error")
                        print(error)
                    }
                }
            } else if let user = result?.user {
                let userToAdd = User(displayName: self.displayNameTextField.text!, username: self.usernameTextField.text!, emailAddress: self.emailTextField.text!, profileImageUrl: "default", favoriteTeam: [:], followingTeams: [], followingLeagues: [], uid: user.uid)
                self.userId = userToAdd.uid
                DatabaseServices.saveUserToDatabase(user: userToAdd) { (error) in
                    if error != nil {
                        self.hideActivityIndicator()
                        GlobalFunctions.displayError(errorMessage: error!.localizedDescription, vc: self)
                    } else {
                        self.hideActivityIndicator()
                        UserDefaults.standard.set(true, forKey: "isLoggedIn")
                        self.presentAddPhotoVC()
                    }
                }
            }
        }
    }
    
    
    
    @IBAction func signUpPressed(_ sender: Any) {
        saveUserToDatabase()
        //self.presentAddPhotoVC()
    }
    

 

}

extension CreateAccountVC: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case displayNameTextField:
            usernameTextField.becomeFirstResponder()
        case usernameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    
}
