//
//  SignInVC.swift
//  Faneever
//
//  Created by James Meli on 3/16/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import IBAnimatable
import Firebase

class SignInVC: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loginButton: AnimatableButton!
    @IBOutlet weak var passwordTextField: ACFloatingTextfield!
    @IBOutlet weak var cancelButton: UIImageView!
    @IBOutlet weak var emailTextField: ACFloatingTextfield!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUiElements()

    }
    
    private func configureUiElements() {
        let cancelGesture = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        let dismissKeyboardGesture = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        cancelButton.addGestureRecognizer(cancelGesture)
        self.view.addGestureRecognizer(dismissKeyboardGesture)
        passwordTextField.delegate = self
        emailTextField.delegate = self
        emailTextField.addTarget(self, action: #selector(textFieldChangedEditing), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldChangedEditing), for: .editingChanged)
        loginButton.roundCorners()

    }
    
    @objc private func textFieldChangedEditing() {
        if allRequiredFieldsFilled() {
            enableLogInButton()
        } else {
            disableLogInButton()
        }
    }
    
    @objc private func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func enableLogInButton() {
        loginButton.isEnabled = true
        loginButton.fillColor = Colors.greenBrandColor
    }
    
    private func disableLogInButton() {
        loginButton.isEnabled = false
        loginButton.fillColor = Colors.disabledButtonColor
    }
    
    private func showActivityIndicator() {
        loginButton.setTitle("", for: .normal)
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    private func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        loginButton.setTitle("LOG IN", for: .normal)
    }
    
    private func allRequiredFieldsFilled() -> Bool {
        if emailTextField.text == "" || passwordTextField.text!.count < 6 {
            return false
        } else {
            return true
        }
    }
    
    private func presentTabbar() {
        let tabbarVC = storyboard?.instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
        tabbarVC.modalPresentationStyle = .currentContext
        tabbarVC.modalTransitionStyle = .crossDissolve
        present(tabbarVC, animated: true, completion: nil)
    }
    
    private func signInUser() {
        showActivityIndicator()
        AuthServices.signInUser(withEmail: emailTextField.text!, andPassword: passwordTextField.text!) { (result, error) in
            if error != nil {
                self.hideActivityIndicator()
                if let errCode = AuthErrorCode(rawValue: error!._code) {
                    switch errCode {
                    case .userNotFound:
                        self.emailTextField.showErrorWithText(errorText: "User not found")
                    case .invalidEmail:
                        self.emailTextField.showErrorWithText(errorText: "Invalid Email")
                    case .wrongPassword:
                        self.passwordTextField.showErrorWithText(errorText: "Wrong password")
                    case .emailAlreadyInUse:
                        self.emailTextField.showErrorWithText(errorText: "Email already in use")
                    case .networkError:
                        self.emailTextField.showErrorWithText(errorText: "Network error")
                    default:
                        print("unknown error")
                        print(error)
                    }
                }
            } else {
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                self.presentTabbar()
            }
        }
    }
    

    @IBAction func logInPressed(_ sender: Any) {
        signInUser()
    }
    

}

extension SignInVC: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            passwordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    
}
