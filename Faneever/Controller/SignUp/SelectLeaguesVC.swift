//
//  SelectLeaguesVC.swift
//  Faneever
//
//  Created by James Meli on 3/1/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class SelectLeaguesVC: UIViewController, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var leagueCollectionView: UICollectionView!
    @IBOutlet weak var nextButton: AnimatableButton!
    
    var popularLeagues = [League]()
    var followingLeagues = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        fetchPopularLeagues()
        setCellLayout()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    private func configureUiElements() {
        leagueCollectionView.delegate = self
        leagueCollectionView.dataSource = self
        leagueCollectionView.allowsMultipleSelection = true
        nextButton.roundCorners()
    }
    
    private func setCellLayout() {
        let firstLayout = ColumnFlowLayout(cellsPerRow: 3, minimumInteritemSpacing: 6, minimumLineSpacing: 10, sectionInset: UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5))
        firstLayout.scrollDirection = .vertical
        leagueCollectionView.collectionViewLayout = firstLayout
    }
    
    private func fetchPopularLeagues() {
        FootballApi.getPopularLeagues { [weak self] result in
            switch result {
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
            case .success(let leagues):
                self?.popularLeagues = leagues
                DispatchQueue.main.async {
                    self?.leagueCollectionView.reloadData()
                }
                
            }
            
        }
    }
    
    private func presentTabbar() {
        let tabbarVC = storyboard?.instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
        tabbarVC.modalPresentationStyle = .currentContext
        tabbarVC.modalTransitionStyle = .crossDissolve
        present(tabbarVC, animated: true, completion: nil)
    }
    
    @IBAction func nextPressed(_ sender: Any) {
        DatabaseServices.saveFollowingLeaguesToDatabase(leagues: followingLeagues) { (error) in
            self.presentTabbar()        }
    }
    

}

extension SelectLeaguesVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return popularLeagues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = leagueCollectionView.dequeueReusableCell(withReuseIdentifier: "SelectLeagueCell", for: indexPath) as! SelectLeagueCell
        cell.resetContent()
        if let id = popularLeagues[indexPath.row].id {
            if followingLeagues.contains(id) {
                cell.selectCell()
                leagueCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
            } else {
                cell.unSelectCell()
            }
        }
        cell.league = popularLeagues[indexPath.row]
        return cell

    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalFunctions.playSelectionFeedbackGenerator()
        let cell = leagueCollectionView.cellForItem(at: indexPath) as! SelectLeagueCell
        cell.selectCell()
        if let id = popularLeagues[indexPath.row].id {
            self.followingLeagues.append(id)
            print("Following leagues: \(followingLeagues)")
            

        }

    }
    

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = leagueCollectionView.cellForItem(at: indexPath) as? SelectLeagueCell {
            cell.unSelectCell()
            if let id = popularLeagues[indexPath.row].id {
                if followingLeagues.contains(id) {
                    if let index = followingLeagues.index(of: id) {
                        followingLeagues.remove(at: index)
                        print("Following leagues: \(followingLeagues)")
                    }
                }
            }

        }
        
    }
    
    
}
