//
//  SelectTeamsVC.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class SelectTeamsVC: UIViewController, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var leagueCollectionView: UICollectionView!
    @IBOutlet weak var teamCollectionView: UICollectionView!
    @IBOutlet weak var nextButton: AnimatableButton!
    
    var popularLeagues = [League]()
    var leagueTeams = [Team]()
    var favoriteTeam: NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

         configureUiElements()
         //fetchPopularLeagues()
         setCellLayout()
         fetchPopularLeagues()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    private func configureUiElements() {
        leagueCollectionView.delegate = self
        leagueCollectionView.dataSource = self
        leagueCollectionView.allowsMultipleSelection = false
        teamCollectionView.delegate = self
        teamCollectionView.dataSource = self
        teamCollectionView.tag = 2
        leagueCollectionView.tag = 1
        nextButton.roundCorners()
    }
    
    private func setCellLayout() {
        let firstLayout = ColumnFlowLayout(cellsPerRow: 3, minimumInteritemSpacing: 6, minimumLineSpacing: 10, sectionInset: UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5))
        firstLayout.scrollDirection = .vertical
        teamCollectionView.collectionViewLayout = firstLayout
        let secondLayout = UICollectionViewFlowLayout()
        secondLayout.scrollDirection = .horizontal
        secondLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        leagueCollectionView.collectionViewLayout = secondLayout
    }
    
    private func enableNextButton() {
        nextButton.isEnabled = true
        nextButton.fillColor = Colors.greenBrandColor
    }
    
    private func disableNextButton() {
        nextButton.isEnabled = false
        nextButton.fillColor = Colors.disabledButtonColor
    }
    
    
    private func fetchPopularLeagues() {
        FootballApi.getPopularLeagues { [weak self] result in
            switch result {
            case .failure(let error):
                print("Error: \(error.localizedDescription)")
            case .success(let leagues):
                self?.popularLeagues = leagues
                DispatchQueue.main.async {
                    self?.leagueCollectionView.reloadData()
                }
                
                if let id = self?.popularLeagues[0].current_season_id {
                    FootballApi.getTeamsWithSeasonId(seasonId: id) { [weak self] result in
                        switch result {
                        case .failure(let error):
                            print("Error fetching teams")
                        case .success(let teams):
                            self?.leagueTeams = teams
                            DispatchQueue.main.async {
                                self?.teamCollectionView.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func presentSelectTeamsVC() {
        let selectFollowingTeamsVC = storyboard?.instantiateViewController(withIdentifier: "SelectFollowingTeamsVC") as! SelectFollowingTeamsVC
        selectFollowingTeamsVC.modalPresentationStyle = .currentContext
        selectFollowingTeamsVC.modalTransitionStyle = .crossDissolve
        present(selectFollowingTeamsVC, animated: true, completion: nil)
    }
    
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        DatabaseServices.saveFavoriteTeamToDatabase(team: favoriteTeam) { (error) in
            self.presentSelectTeamsVC()
        }
        
    }
    



}

extension SelectTeamsVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
            return popularLeagues.count
        } else {
            return leagueTeams.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1 {
            let cell = leagueCollectionView.dequeueReusableCell(withReuseIdentifier: "SelectTeamLeagueCell", for: indexPath) as! SelectTeamLeagueCell
            cell.resetContent()
            cell.league = popularLeagues[indexPath.row]
            return cell
        } else {
            let cell = teamCollectionView.dequeueReusableCell(withReuseIdentifier: "SelectTeamCell", for: indexPath) as! SelectTeamCell
            cell.resetContent()
            if let id = self.favoriteTeam["id"] as? Int {
                if id == leagueTeams[indexPath.row].id {
                    cell.selectCell()
                    collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
                } else {
                    cell.unSelectCell()
                }
            }

            cell.team = leagueTeams[indexPath.row]
            return cell
        }

    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalFunctions.playSelectionFeedbackGenerator()
        if collectionView.tag == 1 {
            let cell = leagueCollectionView.cellForItem(at: indexPath) as! SelectTeamLeagueCell
            cell.selectCell()
            if let id = popularLeagues[indexPath.row].current_season_id {
                FootballApi.getTeamsWithSeasonId(seasonId: id) { [weak self] result in
                    switch result {
                    case .failure(let error):
                        print("Error getting teams")
                    case .success(let teams):
                        self?.leagueTeams = teams
                        DispatchQueue.main.async {
                            self?.teamCollectionView.reloadData()
                        }
                    }
                }
            }
        } else {
            let cell = teamCollectionView.cellForItem(at: indexPath) as! SelectTeamCell
            cell.selectCell()
            if let favoriteTeamName = leagueTeams[indexPath.row].name, let id = leagueTeams[indexPath.row].id, let logoUrl = leagueTeams[indexPath.row].logo_path, let seasonId = leagueTeams[indexPath.row].current_season_id {
                self.favoriteTeam = ["name": favoriteTeamName, "id": id, "logoUrl": logoUrl, "currentSeasonId": seasonId]
                print("Favorite team: \(favoriteTeam)")
                self.enableNextButton()

            }
            
        }

    }
    

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            if let cell = leagueCollectionView.cellForItem(at: indexPath) as? SelectTeamLeagueCell {
               cell.unselectCell()
            }
        } else {
            if let cell = teamCollectionView.cellForItem(at: indexPath) as? SelectTeamCell {
                cell.unSelectCell()

            }
        }

        
    }
}
