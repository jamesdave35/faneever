//
//  AddPhotoVC.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import BottomPopup
import IBAnimatable

class AddPhotoVC: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var savePhotoButton: AnimatableButton!
    @IBOutlet weak var profilePhoto: UIImageView!
    
    var userId: String?
    
    private let imagePicker = UIImagePickerController()
    private var photoIsSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiProperties()
        configureImagePicker()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    

    
    private func configureUiProperties() {
        savePhotoButton.roundCorners()
        let selectPhotoGesture = UITapGestureRecognizer(target: self, action: #selector(presentImagePickerAlertController))
        self.profilePhoto.addGestureRecognizer(selectPhotoGesture)
    }
    
    private func configureImagePicker() {
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
    }
    
    @objc private func presentImagePickerAlertController() {
        let alertController = UIAlertController(title: "Add a photo", message: "Choose From", preferredStyle: .actionSheet)
        
        let photosLibraryAction = UIAlertAction(title: NSLocalizedString("Photos Library", comment: "photos"), style: .default) { (action) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        
        let savedPhotosAction = UIAlertAction(title: NSLocalizedString("Saved Photos Album", comment: "direction"), style: .default) { (action) in
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        
        let removePhotoAction = UIAlertAction(title: "Remove photo", style: .destructive) { (action) in
            self.photoIsSelected = false
            self.profilePhoto.contentMode = .scaleAspectFit
            self.profilePhoto.layer.cornerRadius = 0
            self.profilePhoto.image = UIImage(systemName: "person.crop.circle.fill.badge.plus")
            self.savePhotoButton.isEnabled = false
            self.savePhotoButton.fillColor = Colors.disabledButtonColor
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "dismiss"), style: .cancel, handler: nil)
        
        if self.photoIsSelected {
            alertController.addAction(photosLibraryAction)
            alertController.addAction(savedPhotosAction)
            alertController.addAction(removePhotoAction)
        } else {
            alertController.addAction(photosLibraryAction)
            alertController.addAction(savedPhotosAction)
            
        }
        
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    private func showActivityIndicator() {
        savePhotoButton.setTitle("", for: .normal)
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
    }
    
    private func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        savePhotoButton.setTitle("SAVE PHOTO", for: .normal)
    }
    
    private func presentSelectTeamsVC() {
        let selectTeamsVC = storyboard?.instantiateViewController(withIdentifier: "SelectTeamsVC") as! SelectTeamsVC
        selectTeamsVC.modalPresentationStyle = .currentContext
        present(selectTeamsVC, animated: true, completion: nil)
    }
    
    private func saveUserPhoto() {
        showActivityIndicator()
        if let image = self.profilePhoto.image, let id = self.userId {
            StorageServices.saveUserProfilePhoto(image: image, id: id) { (error, url) in
                if error != nil {
                    GlobalFunctions.displayError(errorMessage: error!.localizedDescription, vc: self)
                } else if let url = url {
                    DatabaseServices.addProfilePhotoUrlToDatabase(id: id, url: url)
                    self.presentSelectTeamsVC()
                }
            }
        }
    }
    

    @IBAction func skipPressed(_ sender: Any) {
        presentSelectTeamsVC()
    }
    
    @IBAction func savePhotoPressed(_ sender: Any) {
        saveUserPhoto()
    }
    
}

extension AddPhotoVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
              fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        self.photoIsSelected = true
        self.profilePhoto.layer.cornerRadius = 65
        self.profilePhoto.contentMode = .scaleAspectFill
        self.profilePhoto.image = selectedImage
        self.savePhotoButton.isEnabled = true
        self.savePhotoButton.fillColor = Colors.greenBrandColor
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
