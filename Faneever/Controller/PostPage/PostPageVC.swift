//
//  PostPageVC.swift
//  Faneever
//
//  Created by James Meli on 3/12/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable
import Firebase

class PostPageVC: UIViewController {

    @IBOutlet weak var replyPostView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var post: Post?
    var replyPosts = [Post]()
    
    private var reference: CollectionReference?
    private var messageListener: ListenerRegistration?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        configureUiElements()
        getReplyPosts()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if navigationController!.isNavigationBarHidden {
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
        tabBarController?.tabBar.isHidden = true
        navigationController?.hidesBarsOnSwipe = false
        
    }
    
    private func configureNavBar() {
        //navigationController?.navigationBar.barTintColor = Colors.secondaryBackgroundColor
        navigationItem.title = "Post"
        navigationController?.navigationBar.titleTextAttributes = Constants.navBarTitleAttributes
    }
    
    private func configureUiElements() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(presentReplyVC))
        replyPostView.addGestureRecognizer(tapGesture)
    }
    
   @objc private func presentReplyVC() {
        GlobalFunctions.playSelectionFeedbackGenerator()
        let replyVC = storyboard?.instantiateViewController(withIdentifier: "NewPostVC") as! NewPostVC
        replyVC.isReply = true
        replyVC.replyPost = self.post
        let navController = UINavigationController(rootViewController: replyVC)
        present(navController, animated: true, completion: nil)
    }
    
    private func getReplyPosts() {
        if let post = self.post {
            reference = Firestore.firestore().collection("Posts").document(post.postId).collection("Replies")
            messageListener = reference?.addSnapshotListener({ (querySnapshot, error) in
                guard let snapshot = querySnapshot  else {
                    print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
                    return
                    
                }
                
                snapshot.documentChanges.forEach { (change) in
                    guard var data = change.document.data() as? NSDictionary else { return }
                    
                    let post = Post(data: data)
                    switch change.type {
                    case .added:
                        self.replyPosts.append(post)
                        
                    default:
                        break
                    }
                }
                
                self.replyPosts.sort(by: {$0.timeStamp < $1.timeStamp})
                self.tableView.reloadData()
                
                    
                
            })
        }
    }
    
    
    

    
    deinit {
        messageListener?.remove()
    }

}

extension PostPageVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return replyPosts.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostPageCell", for: indexPath) as! PostPageCell
            cell.resetContent()
            if let post = post {
                cell.updateView(post: post)
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostReplyCell", for: indexPath) as! PostReplyCell
            cell.resetContent()
            cell.updateView(post: replyPosts[indexPath.row])
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(named: "alternatePrimaryBackground")
        if section == 0 {
            return headerView
        } else {
            let headerLabel = PaddingLabel()
            headerLabel.text = "REPLIES"
            headerLabel.textColor = Colors.secondaryText
            headerLabel.fillColor = Colors.secondaryBackground
            headerLabel.clipsToBounds = true
            headerLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            headerLabel.numberOfLines = 0
            headerLabel.cornerRadius = 15
            if self.replyPosts.isEmpty {
                headerLabel.isHidden = true
            }
            
            headerLabel.translatesAutoresizingMaskIntoConstraints = false
            let verticalConstraint = NSLayoutConstraint(item: headerLabel, attribute: .bottom, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1, constant: -12)
            let leadingConstraint = NSLayoutConstraint(item: headerLabel, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1, constant: 15)
            let heightConstraint = NSLayoutConstraint(item: headerLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 30)
            headerView.addSubview(headerLabel)
            headerView.addConstraints([verticalConstraint, leadingConstraint, heightConstraint])
    
            return headerView
        }
    }
}
