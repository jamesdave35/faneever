//
//  ProfileVC.swift
//  Faneever
//
//  Created by James Meli on 3/1/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class ProfileVC: UITableViewController {

    @IBOutlet weak var settingsButton: UIImageView!
    @IBOutlet weak var sixthContainerView: AnimatableView!
    @IBOutlet weak var fifthContainerView: AnimatableView!
    @IBOutlet weak var fourthContainerView: AnimatableView!
    @IBOutlet weak var thirdContainerView: AnimatableView!
    @IBOutlet weak var secondContainerView: AnimatableView!
    @IBOutlet weak var editProfileButton: AnimatableButton!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var displayNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var firstContainerView: AnimatableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavBar()
        configureUiElements()
        setUpUserProfile()
    }
    
    private func configureNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    private func configureUiElements() {
        tableView.backgroundColor = Colors.primaryBackground
        firstContainerView.backgroundColor = Colors.secondaryBackground
        secondContainerView.backgroundColor = Colors.secondaryBackground
        thirdContainerView.backgroundColor = Colors.secondaryBackground
        fourthContainerView.backgroundColor = Colors.secondaryBackground
        fifthContainerView.backgroundColor = Colors.secondaryBackground
        sixthContainerView.backgroundColor = Colors.secondaryBackground
        displayNameLabel.textColor = Colors.primaryText
        userNameLabel.textColor = Colors.secondaryText
        editProfileButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(presentSettingsVC))
        settingsButton.addGestureRecognizer(tapGesture)

    }
    
    private func setUpUserProfile() {
        if let user = Constants.currentUser {
            if let url = user.getProfileImageURL() {
                profileImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
            }
            
            self.displayNameLabel.text = user.displayName
            self.userNameLabel.text = "@\(user.username)"
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 300
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                return 120
            } else {
                return 70
            }
        }
        
        return 0
    }
    
    @objc private func presentSettingsVC() {
        let settingsVC = storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        let navController = UINavigationController(rootViewController: settingsVC)
        navController.modalPresentationStyle = .fullScreen
        present(navController, animated: true, completion: nil)
    }

    

    @IBAction func editProfilePressed(_ sender: Any) {

    }
    

}
