//
//  SearchVC.swift
//  Faneever
//
//  Created by James Meli on 5/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {

    var searchController: UISearchController?
    var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    var searchCategory = "Teams"
    var menu = ["Teams", "Players", "Leagues"]
    var searchString = ""
    var players = [Player]()
    var teams = [Team]()
    var leagues = [League]()
    
    var delegate: SearchDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .onDrag
    }
    
    private func configureNavBar() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            searchController = UISearchController(searchResultsController: nil)
            searchController?.searchBar.tintColor = Colors.greenBrandColor
            searchController?.searchBar.keyboardAppearance = .default
            searchController?.searchBar.delegate = self
            searchController?.obscuresBackgroundDuringPresentation = false
            searchController?.searchBar.placeholder = "search players,teams and leagues"
            searchController?.searchBar.barStyle = .default
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            
        }
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissView))
        cancelButton.tintColor = Colors.greenBrandColor
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.title = "Search"
    }
    
    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
    func performSearch(searchText: String) {
        if searchCategory == "Players" {
            if searchText.count >= 4 {
                FootballApi.getPlayersFromSearch(searchString: searchText) { [weak self] result in
                    switch result {
                    case .failure(let error):
                        print("Error")
                    case .success(let players):
                        self?.players = players
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    }
                }
            }
        } else if searchCategory == "Teams" {
            if searchText.count >= 4 {
                FootballApi.getTeamsFromSearch(searchString: searchText) { [weak self] result in
                    switch result {
                    case .failure(let error):
                        print("Error")
                    case .success(let teams):
                        self?.teams = teams
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    }
                }
            }
        } else {
            if searchText.count >= 4 {
                FootballApi.getLeaguesFromSearch(searchString: searchText) { [weak self] result in
                    switch result {
                    case .failure(let error):
                        print("Error")
                    case .success(let leagues):
                        self?.leagues = leagues
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    



}

extension SearchVC: UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegateFlowLayout {
    
 
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchCategory == "Players" {
            return players.count
        } else if searchCategory == "Teams" {
            return teams.count
        } else {
            return leagues.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchCategory == "Players" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerSearchCell", for: indexPath) as! PlayerSearchCell
            cell.player = players[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamOrLeagueSearchCell", for: indexPath) as! TeamOrLeagueSearchCell
            if searchCategory == "Teams" {
                cell.team = teams[indexPath.row]
            } else {
                cell.league = leagues[indexPath.row]
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = Colors.alternatePrimaryBackground
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout)
        collectionView.backgroundColor = .clear
        collectionView.register(SearchMenuCell.self, forCellWithReuseIdentifier: "SearchMenuCell")

        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        let verticalConstraint = NSLayoutConstraint(item: collectionView, attribute: .centerY, relatedBy: .equal, toItem: headerView, attribute: .centerY, multiplier: 1, constant: 0)
        let leadingConstraint = NSLayoutConstraint(item: collectionView, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: collectionView, attribute: .trailing, relatedBy: .equal, toItem: headerView, attribute: .trailing, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: collectionView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 60)
        headerView.addSubview(collectionView)
        headerView.addConstraints([verticalConstraint, leadingConstraint, trailingConstraint, heightConstraint])
        collectionView.delegate = self
        collectionView.dataSource = self
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchController?.dismiss(animated: true, completion: {
            self.dismiss(animated: true) {
                if self.searchCategory == "Players" {
                    self.delegate?.didTapOnSearchResult(player: self.players[indexPath.row], team: nil, league: nil)
                } else if self.searchCategory == "Teams" {
                    self.delegate?.didTapOnSearchResult(player: nil, team: self.teams[indexPath.row], league: nil)
                } else {
                    self.delegate?.didTapOnSearchResult(player: nil, team: nil, league: self.leagues[indexPath.row])
                }
                
            }
        })


    }
    
}

extension SearchVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchMenuCell", for: indexPath) as! SearchMenuCell
        cell.menu = menu[indexPath.row]
        if cell.menu == searchCategory {
            cell.isSelected = true
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        } else {
            cell.isSelected = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalFunctions.playSelectionFeedbackGenerator()
        let cell = collectionView.cellForItem(at: indexPath) as! SearchMenuCell
        cell.isSelected = true
        searchCategory = menu[indexPath.row]
        performSearch(searchText: self.searchString)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SearchMenuCell
        cell.isSelected = false
    }
    
}

extension SearchVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchString = searchText
        performSearch(searchText: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
         players = []
         teams = []
         leagues = []
         tableView.reloadData()
    }
}
