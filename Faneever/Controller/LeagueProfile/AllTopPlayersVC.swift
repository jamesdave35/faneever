//
//  AllTopPlayersVC.swift
//  Faneever
//
//  Created by James Meli on 5/14/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class AllTopPlayersVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var topPlayers: TopScorers?
    var header: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.barTintColor = nil
        navigationController?.navigationBar.tintColor = Colors.greenBrandColor
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = .clear
    }
    
    
    
    private func configureUiElements() {
        view.backgroundColor = Colors.alternatePrimaryBackground
        if let header = header {
            navigationItem.title = header
        }
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    

}

extension AllTopPlayersVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let header = header {
            if header == "Goal Scorers" {
                if let scorers = topPlayers?.aggregatedGoalscorers?.data {
                    return scorers.count
                }
            } else if header == "Top Assists" {
                if let assists = topPlayers?.aggregatedAssistscorers?.data {
                    return assists.count
                }
            } else if header == "Yellow Card" {
                if let cards = topPlayers?.aggregatedCardscorers?.data {
                    return cards.count
                }
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AllTopPlayersFirstCell", for: indexPath) as! AllTopPlayersFirstCell
            if header == "Goal Scorers" {
                if let topScorers = topPlayers?.aggregatedGoalscorers?.data {
                    cell.topScorer = topScorers[indexPath.row]
                }
                
                return cell
            } else if header == "Top Assists" {
                if let assists = topPlayers?.aggregatedAssistscorers?.data {
                    cell.topAssist = assists[indexPath.row]
                }
                
                return cell
            } else if header == "Yellow Card" {
                if let cards = topPlayers?.aggregatedCardscorers?.data {
                    cell.topYellowCard = cards[indexPath.row]
                }
                
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AllTopPlayersCell", for: indexPath) as! AllTopPlayersCell
            if header == "Goal Scorers" {
                if let topScorers = topPlayers?.aggregatedGoalscorers?.data {
                    cell.topScorer = topScorers[indexPath.row]
                }
                
                return cell
            } else if header == "Top Assists" {
                if let assists = topPlayers?.aggregatedAssistscorers?.data {
                    cell.topAssist = assists[indexPath.row]
                }
                
                return cell
            } else if header == "Yellow Card" {
                if let cards = topPlayers?.aggregatedCardscorers?.data {
                    cell.topYellowCard = cards[indexPath.row]
                }
                
                return cell
            }
        }
        
        return tableView.dequeueReusableCell(withIdentifier: "AllTopPlayersCell", for: indexPath) as! AllTopPlayersCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 165
        } else {
            return 85
        }
    }
}
