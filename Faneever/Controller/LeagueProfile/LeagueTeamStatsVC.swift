//
//  LeagueTeamStatsVC.swift
//  Faneever
//
//  Created by James Meli on 5/14/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class LeagueTeamStatsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var league: League?
    var seasonDict: [String: [TeamStats]] = [:]
    var statKeys = [StatKey]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Colors.alternatePrimaryBackground
        tableView.delegate = self
        tableView.dataSource = self
        getSeasonTeamStats()
    }
    
    private func getSeasonTeamStats() {
        if let seasonId = league?.current_season_id {
            FootballApi.getLeagueTeamStats(seasonId: seasonId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let stats):
                    self?.seasonDict = ["Goals Scored": stats.getTopGoals(seasonId: seasonId), "Avg Goals/Game": stats.getTopAvgGoalsPerGame(seasonId: seasonId), "Goals Conceded": stats.getTopGoalsConceded(seasonId: seasonId), "Wins": stats.getTopWins(seasonId: seasonId), "Draws": stats.getTopDraws(seasonId: seasonId), "Lost": stats.getTopLost(seasonId: seasonId), "Avg Possesion/Game": stats.getTopAvgPossession(seasonId: seasonId), "Avg Shots on Target/Game": stats.getTopAvgShotsOnTgt(seasonId: seasonId), "Cleen Sheets": stats.getTopCleenSheats(seasonId: seasonId), "Corners": stats.getTopCorners(seasonId: seasonId), "Offsides": stats.getTopOffsides(seasonId: seasonId), "Fouls": stats.getTopFouls(seasonId: seasonId), "Yellow Cards": stats.getTopYellowCards(seasonId: seasonId), "Red Cards": stats.getTopRedCards(seasonId: seasonId)]
                    self?.dictKeys(keys: Array((self?.seasonDict.keys)!))
                    
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func dictKeys(keys: Array<String>) {
        
        for key in keys {
            switch key {
            case "Goals Scored":
                statKeys.append(StatKey(key: key, priority: 0))
            case "Avg Goals/Game":
                statKeys.append(StatKey(key: key, priority: 1))
            case "Goals Conceded":
                statKeys.append(StatKey(key: key, priority: 2))
            case "Wins":
                statKeys.append(StatKey(key: key, priority: 3))
            case "Draws":
                statKeys.append(StatKey(key: key, priority: 4))
            case "Lost":
                statKeys.append(StatKey(key: key, priority: 5))
            case "Avg Possesion/Game":
                statKeys.append(StatKey(key: key, priority: 6))
            case "Avg Shots on Target/Game":
               statKeys.append(StatKey(key: key, priority: 7))
            case "Cleen Sheets":
               statKeys.append(StatKey(key: key, priority: 8))
            case "Corners":
               statKeys.append(StatKey(key: key, priority: 9))
            case "Offsides":
               statKeys.append(StatKey(key: key, priority: 10))
            case "Fouls":
               statKeys.append(StatKey(key: key, priority: 11))
            case "Yellow Cards":
                statKeys.append(StatKey(key: key, priority: 12))
            default:
               statKeys.append(StatKey(key: key, priority: 13))
        }
        
      }
        
        statKeys.sort(by: {$0.priority < $1.priority})
        
    }
    



}

extension LeagueTeamStatsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !statKeys.isEmpty {
            return statKeys.count
        }

        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTeamStatsCell", for: indexPath) as! LeagueTeamStatsCell
        cell.delegate = self
        cell.seasonId = league?.current_season_id
        cell.statName = statKeys[indexPath.row].key
        cell.stats = seasonDict[statKeys[indexPath.row].key]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 265
    }
    
    
}

extension LeagueTeamStatsVC: LeagueProfileDelegate {
    
    func didSelectTeam(teamId: Int) {
        let teamProfileVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileVC") as! TeamProfileVC
        teamProfileVC.teamId = teamId
        navigationController?.pushViewController(teamProfileVC, animated: true)
    }
}


