//
//  LeagueProfileFixturesVC.swift
//  Faneever
//
//  Created by James Meli on 5/12/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class LeagueProfileFixturesVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var league: League?
    var fixtureDict: [String: [Fixture]] = [:]
    var keys = [String]()
    var dateKeys = [Date]()
    var activeIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        getLeagueFixture()
        
    }
    
    private func configureUiElements() {
        self.view.backgroundColor = Colors.primaryBackground
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = Colors.seperatorColor
    }
    
    private func getLeagueFixture() {
        if let id = league?.current_season_id {
            FootballApi.getLeagueFixtures(seasonId: id) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let fixtures):
                    self?.fixtureDict = Dictionary(grouping: fixtures, by: {($0.time?.starting_at?.getSecondDateString())!})
                    self?.createDictKeys(keys: Array((self?.fixtureDict.keys)!))
                    self?.activeIndex = self?.getActiveIndex(dates: self?.dateKeys ?? [])
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                        if let index = self?.activeIndex {
                            self?.tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: .none, animated: true)
                        }
                    }
                    
                }
            }
        }
    }
    


}

extension LeagueProfileFixturesVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fixtureDict.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = fixtureDict[self.keys[section]]?.count {
            return count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueProfileFixtureCell", for: indexPath) as! LeagueProfileFixtureCell
        cell.fixture = fixtureDict[self.keys[indexPath.section]]![indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fixtureVC = storyboard?.instantiateViewController(withIdentifier: "FixtureVC") as! FixtureVC
        let cell = tableView.cellForRow(at: indexPath) as! LeagueProfileFixtureCell
        fixtureVC.fixtureId = cell.fixture?.id
        fixtureVC.fixture = cell.fixture
        navigationController?.pushViewController(fixtureVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = Colors.alternatePrimaryBackground
        let headerLabel = UILabel()
        headerLabel.textColor = Colors.secondaryText
        headerLabel.font = UIFont(name: "Helvetica-Bold", size: 13)
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        let verticalConstraint = NSLayoutConstraint(item: headerLabel, attribute: .centerY, relatedBy: .equal, toItem: headerView, attribute: .centerY, multiplier: 1, constant: 0)
        let leadingConstraint = NSLayoutConstraint(item: headerLabel, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1, constant: 15)
        headerView.addSubview(headerLabel)
        headerView.addConstraints([verticalConstraint, leadingConstraint])
        headerLabel.text = self.keys[section]
        
        return headerView
    }
}

extension LeagueProfileFixturesVC {
    func createDictKeys(keys: Array<String>)  {
        for key in keys {
            if let dateToAdd = key.convertToDate(format: "MMM d, yyyy") {
                self.dateKeys.append(dateToAdd)
            }
        }
        
        dateKeys.sort(by: {$0 < $1})
        
        for date in dateKeys {
            if let keyToAdd = date.convertToString(withFormat: "MMM d, yyyy") {
                self.keys.append(keyToAdd)
            }
        }
        
    }
    
    func getActiveIndex(dates: [Date]) -> Int? {
        let currentDate = Date()
        
        if dates.contains(currentDate) {
            return dates.index(of: currentDate)!
        } else {
            var tempDates = dates.filter({$0 > currentDate})
            tempDates.sort(by: {$0 < $1})
            if !tempDates.isEmpty {
                return dates.index(of: tempDates[0])!
            } else {
                return nil
            }
            
        }

    }
}
