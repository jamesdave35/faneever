//
//  LeagueProfileVC.swift
//  Faneever
//
//  Created by James Meli on 5/12/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import EZYGradientView

class LeagueProfileVC: UIViewController, UICollectionViewDelegateFlowLayout {

    
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var leagueName: UILabel!
    @IBOutlet weak var leagueLogo: UIImageView!
    @IBOutlet weak var headerView: EZYGradientView!
    
    weak var pageViewController: LeagueProfilePVC!
    weak var leagueTableVC: LeagueProfileTableVC!
    weak var leagueFixturesVC: LeagueProfileFixturesVC!
    weak var leagueTopPlayersVC: LeaguePlayerStatsVC!
    weak var leagueTeamStatsVC: LeagueTeamStatsVC!
    
    var viewControllers = [UIViewController]()
    var currentIndex = 0
    var league: League?
    var followButton: UIImageView!
    var notificationButton: UIImageView!
    var selectedMenu = "Table"
    var menu = ["Table", "Fixtures", "Player Stats", "Team Stats", "Buzz"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        configureUiElements()
        setLeagueInfo()
        getLeagueProfile()
        configurePageViewController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = Colors.secondaryBackground
    }
    
    private func configureNavBar() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
        
        followButton = UIImageView(image: UIImage(systemName: "heart", withConfiguration: UIImage.SymbolConfiguration(pointSize: 22, weight: .light)))
        followButton.tintColor = .white
        followButton.contentMode = .scaleAspectFit
        notificationButton = UIImageView(image: UIImage(systemName: "bell", withConfiguration: UIImage.SymbolConfiguration(pointSize: 22, weight: .light)))
        notificationButton.contentMode = .scaleAspectFit
        notificationButton.tintColor = .white
        let stackView = UIStackView(arrangedSubviews: [followButton, notificationButton])
        stackView.distribution = .equalSpacing
        stackView.axis = .horizontal
        stackView.spacing = 20
        let rightBarButtonItem = UIBarButtonItem(customView: stackView)
        navigationItem.rightBarButtonItem = rightBarButtonItem
        
    }
    
    private func configureUiElements() {
        view.backgroundColor = Colors.alternatePrimaryBackground
        collectionView.delegate = self
        collectionView.dataSource = self

    }
    
    private func setCellLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 10)
        collectionView.collectionViewLayout = layout
    }
    
    private func configurePageViewController() {
        pageViewController = storyboard?.instantiateViewController(withIdentifier: "LeagueProfilePVC") as! LeagueProfilePVC
        pageViewController.delegate = self
        pageViewController.dataSource = self
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageViewController.view)
        let topConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .top, relatedBy: .equal, toItem: self.headerView, attribute: .bottom, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        let rightConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        self.view.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
        
        leagueTableVC = storyboard?.instantiateViewController(withIdentifier: "LeagueProfileTableVC") as! LeagueProfileTableVC
        leagueFixturesVC = storyboard?.instantiateViewController(withIdentifier: "LeagueProfileFixturesVC") as! LeagueProfileFixturesVC
        leagueTopPlayersVC = storyboard?.instantiateViewController(withIdentifier: "LeaguePlayerStatsVC") as! LeaguePlayerStatsVC
        leagueTeamStatsVC = storyboard?.instantiateViewController(withIdentifier: "LeagueTeamStatsVC") as! LeagueTeamStatsVC
        
        if let league = self.league {
            leagueTableVC.league = league
            leagueFixturesVC.league = league
            leagueTopPlayersVC.league = league
            leagueTeamStatsVC.league = league
        }
        
        viewControllers = [leagueTableVC, leagueFixturesVC, leagueTopPlayersVC, leagueTeamStatsVC]
        pageViewController.setViewControllers([leagueTableVC], direction: .forward, animated: true, completion: nil)
    }
    
    private func jump(to: Int) {
         let toVC = viewControllers[to]
         var direction: UIPageViewController.NavigationDirection = .forward

        if currentIndex < to {
            direction = .forward
        } else {
            direction = .reverse
        }

        pageViewController.setViewControllers([toVC], direction: direction, animated: true) { _ in
            DispatchQueue.main.async {
                self.pageViewController.setViewControllers([toVC], direction: direction, animated: true, completion: nil)
            }
        }
    }
    
    private func getLeagueProfile() {
        if let id = league?.id {
            FootballApi.getLeagueById(leagueId: id) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let leagueReturned):
                    self?.league = leagueReturned
                    
                }
            }
        }
    }
    
    private func setLeagueInfo() {
        if let name = self.league?.name {
            leagueName.text = name
            createHeaderViewGradient(leagueName: name)
        }
        
        if let url = self.league?.getLogoURL() {
            leagueLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
        }
        
        if let url = self.league?.country?.data?.getLogoUrl() {
            countryFlag.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
        }
    }
    
    private func createHeaderViewGradient(leagueName: String) {
          let colorOne: UIColor = {
            if #available(iOS 13, *) {
                return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                    if UITraitCollection.userInterfaceStyle == .dark {
                        /// Return the color for Dark Mode
                        return UIColor(hexString: "2D2D2D")
                    } else {
                        /// Return the color for Light Mode
                        return LeagueColors.leagueColors[leagueName]!
                    }
                }
            } else {
                /// Return a fallback color for iOS 12 and lower.
                return UIColor(hexString: "2D2D2D")
            }
        }()
        
        let colorTwo: UIColor = {
            if #available(iOS 13, *) {
                return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                    if UITraitCollection.userInterfaceStyle == .dark {
                        /// Return the color for Dark Mode
                        return UIColor(hexString: "141414")
                    } else {
                        /// Return the color for Light Mode
                        return LeagueColors.leagueColors[leagueName]!
                    }
                }
            } else {
                /// Return a fallback color for iOS 12 and lower.
                return UIColor(hexString: "202020")
            }
        }()
        
        headerView.firstColor = colorOne
        headerView.secondColor = colorTwo
        headerView.angleº = 0
        headerView.colorRatio = 0.3
        headerView.fadeIntensity = 1
        
    }
    
    @objc private func followButtonPressed() {
        
    }
    
    @objc private func notificationButtonPressed() {
        
    }
    


}

extension LeagueProfileVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueProfileMenu", for: indexPath) as! LeagueProfileMenu
        cell.menu = menu[indexPath.row]
        cell.league = league
        if cell.menu == selectedMenu {
            cell.isSelected = true
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        } else {
            cell.isSelected = false
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalFunctions.playSelectionFeedbackGenerator()
        selectedMenu = menu[indexPath.row]
        jump(to: indexPath.row)
        currentIndex = indexPath.row
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
}

extension LeagueProfileVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        if viewController is LeagueProfileFixturesVC {
            leagueTableVC = storyboard?.instantiateViewController(withIdentifier: "LeagueProfileTableVC") as! LeagueProfileTableVC
            if let league = self.league {
                leagueTableVC.league = league
            }
            
            return leagueTableVC
        } else if viewController is LeaguePlayerStatsVC {
            leagueFixturesVC = storyboard?.instantiateViewController(withIdentifier: "LeagueProfileFixturesVC") as! LeagueProfileFixturesVC
            if let league = self.league {
                leagueFixturesVC.league = league
            }
            
            return leagueFixturesVC
        } else if viewController is LeagueTeamStatsVC {
            leagueTopPlayersVC = storyboard?.instantiateViewController(withIdentifier: "LeaguePlayerStatsVC") as! LeaguePlayerStatsVC
            leagueTopPlayersVC.league = league
            return leagueTopPlayersVC
        } else {
            return nil
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        if viewController is LeagueProfileTableVC {
            leagueFixturesVC = storyboard?.instantiateViewController(withIdentifier: "LeagueProfileFixturesVC") as! LeagueProfileFixturesVC
            if let league = self.league {
                leagueFixturesVC.league = league
            }
            
            return leagueFixturesVC
        } else if viewController is LeagueProfileFixturesVC {
            leagueTopPlayersVC = storyboard?.instantiateViewController(withIdentifier: "LeaguePlayerStatsVC") as! LeaguePlayerStatsVC
            leagueTopPlayersVC.league = league
            return leagueTopPlayersVC
        } else if viewController is LeaguePlayerStatsVC {
            leagueTeamStatsVC = storyboard?.instantiateViewController(withIdentifier: "LeagueTeamStatsVC") as! LeagueTeamStatsVC
            leagueTeamStatsVC.league = self.league
            return leagueTeamStatsVC
        } else {
            return nil
        }
        
    }
}
