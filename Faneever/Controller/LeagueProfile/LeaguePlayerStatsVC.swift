//
//  LeaguePlayerStatsVC.swift
//  Faneever
//
//  Created by James Meli on 5/12/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class LeaguePlayerStatsVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var league: League?
    var topScorers: TopScorers?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        getLeagueTopScorers()
        
    }
    
    private func configureUiElements() {
        view.backgroundColor = Colors.primaryBackground
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    private func getLeagueTopScorers() {
        if let seasonId = league?.current_season_id {
            FootballApi.getLeagueTopPlayers(seasonId: seasonId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let topPlayers):
                    self?.topScorers = topPlayers
                    
                    DispatchQueue.main.async {
                        self?.collectionView.reloadData()
                    }
                }
            }
        }
    }
    



}

extension LeaguePlayerStatsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LeagueTopScorerCell", for: indexPath) as! LeagueTopScorerCell
        cell.delegate = self
        if indexPath.row == 0 {
            cell.title = "Goal Scorers"
            cell.topScorers = self.topScorers
        } else if indexPath.row == 1 {
            cell.title = "Top Assists"
            cell.topScorers = self.topScorers
        } else {
            cell.title = "Yellow Card"
            cell.topScorers = self.topScorers
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 330)
    }
    
    
    
    
}

extension LeaguePlayerStatsVC: LeagueProfileDelegate {
    
    func didSelectSeeAllPlayers(header: String) {
        let allPlayersVC = storyboard?.instantiateViewController(withIdentifier: "AllTopPlayersVC") as! AllTopPlayersVC
        allPlayersVC.header = header
        allPlayersVC.topPlayers = self.topScorers
        navigationController?.pushViewController(allPlayersVC, animated: true)
    }
    
    func didSelectPlayer(player: PlayerStatNumbers) {
        let playerProfileVC = storyboard?.instantiateViewController(withIdentifier: "PlayerProfileVC") as! PlayerProfileVC
        playerProfileVC.player = player
        navigationController?.pushViewController(playerProfileVC, animated: true)
    }
    
 
}
