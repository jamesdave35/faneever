//
//  NewPostVC.swift
//  Faneever
//
//  Created by James Meli on 3/1/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable
import AVFoundation
import EZYGradientView
import MobileCoreServices

class NewPostVC: UIViewController, UICollectionViewDelegateFlowLayout {


    @IBOutlet weak var versusLabel: PaddingLabel!
    @IBOutlet weak var secondCompareLabel: UILabel!
    @IBOutlet weak var secondCompareImage: UIImageView!
    @IBOutlet weak var firstCompareLabel: UILabel!
    @IBOutlet weak var firstCompareImage: UIImageView!
    @IBOutlet weak var compareView: EZYGradientView!
    @IBOutlet weak var playVideoButton: UIImageView!
    @IBOutlet weak var xImage: UIImageView!
    @IBOutlet weak var removeImageButton: UIView!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var stackViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var gifButton: UIImageView!
    @IBOutlet weak var addImageButton: UIImageView!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var cameraButton: UIImageView!
    @IBOutlet weak var postTextView: AnimatableTextView!
    
    private let postTypes = ["Chatter", "Player Comparison", "Team Comparison", "Player Appreciation"]
    private var postType = "Chatter"
    private var imagePicker = UIImagePickerController()
    private var postVideoUrl: URL?
    private var videoPicked = false
    private var playerCompDict: NSDictionary?
    private var teamCompDict: NSDictionary?
    
    var isReply = false
    var replyPost: Post?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        configureNavBar()
        setUpView()
        setUpNotificationObservers()
        setCellLayout()
        adjustTextViewHeight()
        configureImagePicker()
    }
    
    private func configureNavBar() {
        navigationController?.navigationBar.tintColor = Colors.greenBrandColor
        if isReply {
            navigationItem.title = "Reply"
        } else {
           navigationItem.title = "New Post"
        }
        
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.titleTextAttributes = Constants.navBarTitleAttributes
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissView))
        cancelButton.tintColor = Colors.greenBrandColor
        navigationItem.leftBarButtonItem = cancelButton
        
    }
    
    private func configureUiElements() {
        postButton.roundCorners()
        postTextView.placeholderColor = UIColor(named: "secondaryText")
        postTextView.becomeFirstResponder()
        collectionView.delegate = self
        collectionView.dataSource = self
        textViewHeightConstraint.isActive = true
        postTextView.delegate = self
        postImage.layer.cornerCurve = .continuous
        compareView.layer.cornerCurve = .continuous
        versusLabel.fillColor = UIColor(named: "tertiaryBackground")
        let presentImagePickerGesture = UITapGestureRecognizer(target: self, action: #selector(presentImagePicker))
        let removePostMedia = UITapGestureRecognizer(target: self, action: #selector(removePostMedias))
        removeImageButton.addGestureRecognizer(removePostMedia)
        addImageButton.addGestureRecognizer(presentImagePickerGesture)
    }
    
    private func setCellLayout() {
        let secondLayout = UICollectionViewFlowLayout()
        secondLayout.scrollDirection = .horizontal
        secondLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        collectionView.collectionViewLayout = secondLayout
    }
    
    private func adjustTextViewHeight() {
        let fixedWidth = postTextView.frame.size.width
        let newSize = postTextView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        self.textViewHeightConstraint.constant = newSize.height
        self.view.layoutIfNeeded()
    }
    
    private func setUpNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue
        stackViewBottomConstraint.constant = keyboardFrame.height - 20

        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue
        stackViewBottomConstraint.constant = 5

    }
    
    
    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
    private func configureImagePicker() {
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
    }
    
    @objc private func presentImagePicker() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc private func removePostMedias() {
        postImage.image = nil
        postVideoUrl = nil
        postImage.isHidden = true
        removeImageButton.isHidden = true
        xImage.isHidden = true
        playVideoButton.isHidden = true
        videoPicked = false
    }
    
    private func enablePostButton() {
        postButton.backgroundColor = Colors.greenBrandColor
        postButton.isEnabled = true
    }
    
    private func disablePostButton() {
        postButton.backgroundColor = Colors.disabledButtonColor
        postButton.isEnabled = false
    }
    
    private func setUpView() {
        if let urlString = Constants.currentUser?.profileImageUrl {
            let url = URL(string: urlString)
            profilePhoto.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
        }
    }
    
    private func setUpCompareViewForPlayers() {
        compareView.firstColor = Colors.comparePlayers
        compareView.secondColor = Colors.secondaryBackground
        compareView.angleº = 0
        compareView.colorRatio = 0.3
        compareView.fadeIntensity = 1
    }
    
    private func setUpCompareViewForTeams() {
        compareView.firstColor = Colors.compareTeams
        compareView.secondColor = Colors.secondaryBackground
        compareView.angleº = 0
        compareView.colorRatio = 0.3
        compareView.fadeIntensity = 1
    }
    
    
    private func createPost() {
        let randomUniqueString = UUID().uuidString
        if let id = Constants.currentUserId {
            StorageServices.savePostImage(image: self.postImage.image, id: randomUniqueString, videoUrl: self.postVideoUrl, videoPicked: self.videoPicked) { (error, url, videoUrlToPass) in
                let postToSave = Post(message: self.postTextView.text!, type: self.postType, imageUrl: url, videoUrl: videoUrlToPass, postId: randomUniqueString, userId: id, timeStamp: Date(), isReply: nil, likes: [], playerCompDict: self.playerCompDict, teamCompDict: self.teamCompDict)
                if !self.isReply {
                    DatabaseServices.savePostToDatabase(post: postToSave) { (error) in
                        if error != nil {
                            print("Error creating post: \(error?.localizedDescription)")
                            self.dismissView()
                        } else {
                            print("Success")
                            self.dismissView()
                        }
                    }
                } else {
                    if let post = self.replyPost {
                        DatabaseServices.savePostReply(reply: postToSave, isReplyTo: post) { (error) in
                            if error != nil {
                                print("Error creating post: \(error?.localizedDescription)")
                                self.dismissView()
                            } else {
                                print("Success")
                                self.dismissView()
                            }
                        }
                    }
                }

            }

        }
    }

    @IBAction func postPressed(_ sender: Any) {
        createPost()
    }
    

}

extension NewPostVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        adjustTextViewHeight()
        if textView.text == "" {
            disablePostButton()
        } else {
            enablePostButton()
        }
    }
}

extension NewPostVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage  {
            playVideoButton.isHidden = true
            postImage.isHidden = false
            postImage.image = selectedImage
            removeImageButton.isHidden = false
            xImage.isHidden = false
            videoPicked = false
        } else if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            videoPicked = true
            postVideoUrl = videoUrl
            let asset = AVURLAsset(url: videoUrl, options: nil)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            do {
                let cgImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                postImage.isHidden = false
                postImage.image = thumbnail
                removeImageButton.isHidden = false
                xImage.isHidden = false
                playVideoButton.isHidden = false
            } catch let error {
                print("Error: \(error.localizedDescription)")
            }

        }
        

        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension NewPostVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostTypeCell", for: indexPath) as! PostTypeCell
        cell.postType = postTypes[indexPath.row]
        if postType == cell.postType {
            cell.selectCell()
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        } else {
            cell.unSelectCell()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalFunctions.playSelectionFeedbackGenerator()
        let cell = collectionView.cellForItem(at: indexPath) as! PostTypeCell
        cell.selectCell()
        self.postType = postTypes[indexPath.row]
        if postTypes[indexPath.row] == "Player Comparison" {
            let pcVC = storyboard?.instantiateViewController(withIdentifier: "ComparePlayerVC") as! ComparePlayerVC
            pcVC.delegate = self
            pcVC.canChoosePlayer = true
            let navController = UINavigationController(rootViewController: pcVC)
            navController.modalPresentationStyle = .currentContext
            present(navController, animated: true, completion: nil)
        } else if postTypes[indexPath.row] == "Team Comparison" {
            let teamCompVC = storyboard?.instantiateViewController(withIdentifier: "TeamComparisonVC") as! TeamComparisonVC
            teamCompVC.delegate = self
            teamCompVC.canChooseTeam = true
            let navController = UINavigationController(rootViewController: teamCompVC)
            present(navController, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? PostTypeCell {
            cell.unSelectCell()
        }
    }
}

extension NewPostVC: CreatePostDelegate {
    
    func didFinishChoosingPlayersToCompare(firstPlayer: Player, secondPlayer: Player, playerStats: [String : [StatComparison]]) {
        setUpCompareViewForPlayers()
        if let firstName = firstPlayer.display_name, let secondName = secondPlayer.display_name {
            firstCompareLabel.text = firstName
            secondCompareLabel.text = secondName
        }
        
        if let url = firstPlayer.getImageURL() {
            firstCompareImage.layer.cornerRadius = 32.5
            firstCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
        }
        
        if let url = secondPlayer.getImageURL() {
            secondCompareImage.layer.cornerRadius = 32.5
            secondCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultFaceImage)
        }
        compareView.isHidden = false
        
        var statsDict = [String: [NSDictionary]]()
        var holderDict = [NSDictionary]()
        for key in playerStats.keys {
            for stat in playerStats[key]! {
                holderDict.append(stat.toDictionary() as NSDictionary)
            }
            
            statsDict[key] = holderDict
            holderDict.removeAll()
        }
        
        self.playerCompDict = ["firstPlayer": firstPlayer.toDictionary(), "secondPlayer": secondPlayer.toDictionary(), "stats": statsDict]
    }
    
    func didFinishChoosingTeamsToCompare(firstTeam: Team, secondTeam: Team, teamStats: [String : [StatComparison]]) {
        setUpCompareViewForTeams()
        if let firstName = firstTeam.name, let secondName = secondTeam.name {
            firstCompareLabel.text = firstName
            secondCompareLabel.text = secondName
        }
        
        if let url = firstTeam.getLogoUrl() {
            firstCompareImage.layer.cornerRadius = 0
            firstCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
        }
        
        if let url = secondTeam.getLogoUrl() {
            secondCompareImage.layer.cornerRadius = 0
            secondCompareImage.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
        }
        
        self.compareView.isHidden = false
        
        var statsDict = [String: [NSDictionary]]()
        var holderDict = [NSDictionary]()
        for key in teamStats.keys {
            for stat in teamStats[key]! {
                holderDict.append(stat.toDictionary() as NSDictionary)
            }
            
            statsDict[key] = holderDict
            holderDict.removeAll()
        }
        
        self.teamCompDict = ["firstTeam": firstTeam.toDictionary(), "secondTeam": secondTeam.toDictionary(), "stats": statsDict]
        
        
    }
    
}
