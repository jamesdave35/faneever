//
//  ChooseStatisticVC.swift
//  Faneever
//
//  Created by James Meli on 3/5/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class ChooseStatisticVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        configureUiElements()
       
    }
    
    private func configureNavBar() {
        navigationController?.navigationBar.barStyle = .default
        navigationItem.title = "Compare Players"
        navigationController?.navigationBar.titleTextAttributes = Constants.navBarTitleAttributes
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissView))
        cancelButton.tintColor = Colors.greenBrandColor
        navigationItem.leftBarButtonItem = cancelButton
        
    }
    
    private func configureUiElements() {
        self.view.backgroundColor = Colors.alternatePrimaryBackground
        
        let searchPlayersVC = storyboard?.instantiateViewController(withIdentifier: "PlayerSearchComparisonVC") as! PlayerSearchComparisonVC
        
        self.present(searchPlayersVC, animated: true, completion: nil)

    }
    
    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    



}
