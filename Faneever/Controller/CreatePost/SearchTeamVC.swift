//
//  SearchTeamVC.swift
//  Faneever
//
//  Created by James Meli on 3/10/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class SearchTeamVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var searchController: UISearchController?
    var teams = [Team]()
    var delegate: CompareTeamDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

       configureUiElements()
       configureNavBar()
    }
    
    private func configureNavBar() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            searchController = UISearchController(searchResultsController: nil)
            searchController?.searchBar.tintColor = Colors.greenBrandColor
            searchController?.searchBar.keyboardAppearance = .default
            searchController?.searchBar.delegate = self
            searchController?.obscuresBackgroundDuringPresentation = false
            searchController?.searchBar.placeholder = "search team"
            searchController?.searchBar.barStyle = .default
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            
        }
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissView))
        cancelButton.tintColor = Colors.greenBrandColor
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.title = "Search Team"
        
    }
    
    private func configureUiElements() {
        self.view.backgroundColor = Colors.secondaryBackground
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .onDrag
    }
    
    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
    
    



}

extension SearchTeamVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count >= 3 {
            FootballApi.getTeamsFromSearch(searchString: searchText) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let teams):
                    self?.teams = teams
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                    
                }
            }
        } else {
            self.teams = []
            self.tableView.reloadData()
        }

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.teams = []
        self.tableView.reloadData()
    }
}

extension SearchTeamVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        teams.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTeamCell", for: indexPath) as! SearchTeamCell
        cell.resetContent()
        cell.team = teams[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let team = teams[indexPath.row]
        delegate?.didPickTeam(team: team)
        self.searchController?.dismiss(animated: true, completion: nil)
        dismissView()
    }
}
