//
//  TeamComparisonVC.swift
//  Faneever
//
//  Created by James Meli on 3/10/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class TeamComparisonVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButton: AnimatableButton!
    
    var firstTeam: Team?
    var secondTeam: Team?
    var firstTeamStats: TeamStatNumebrs?
    
    var statsDict = [String: [StatComparison]]()
    var delegate: CreatePostDelegate?
    
    var canChooseTeam = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        confirgureNavBar()
        configureUiElements()
    }
    
    private func confirgureNavBar() {
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.titleTextAttributes = Constants.navBarTitleAttributes
        if canChooseTeam {
            navigationItem.title = "Compare Teams"
        } else {
            if let firstTeamName = self.firstTeam?.name, let secondTeamName = self.secondTeam?.name {
                self.navigationItem.title = "\(firstTeamName) VS \(secondTeamName)"
            }
        }
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissView))
        cancelButton.tintColor = Colors.greenBrandColor
        navigationItem.leftBarButtonItem = cancelButton
    }
    
    private func configureUiElements() {
        doneButton.roundCorners()
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        if canChooseTeam {
            presentSearchTeamVC()
        }
        
        
    }
    
    private func presentSearchTeamVC() {
        let searchTeamVC = storyboard?.instantiateViewController(withIdentifier: "SearchTeamVC") as! SearchTeamVC
        searchTeamVC.delegate = self
        let navController = UINavigationController(rootViewController: searchTeamVC)
        present(navController, animated: true, completion: nil)
    }
    
    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func donePressed(_ sender: Any) {
        if let firstTeam = self.firstTeam, let secondTeam = self.secondTeam {
            delegate?.didFinishChoosingTeamsToCompare(firstTeam: firstTeam, secondTeam: secondTeam, teamStats: statsDict)
        }
        
        dismissView()
    }
    


}

extension TeamComparisonVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        statsDict.keys.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            let keys = rearrangeKeys(keys: Array(statsDict.keys))
            if let count = statsDict[keys[section - 1].key]?.count {
                return count + 1
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FirstTeamStatCell", for: indexPath) as! FirstTeamStatCell
            cell.canChooseTeam = self.canChooseTeam
            cell.firstTeam = firstTeam
            cell.secondTeam = secondTeam
            cell.delegate = self
            return cell
        } else {
            let keys = rearrangeKeys(keys: Array(statsDict.keys))
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TeamStatHeaderCell", for: indexPath) as! TeamStatHeaderCell
                cell.header = keys[indexPath.section - 1].key
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SecondTeamStatCell", for: indexPath) as! SecondTeamStatCell
                cell.resetContent()
                if let stats = statsDict[keys[indexPath.section - 1].key] {
                    cell.teamStat = stats[indexPath.row - 1]
                }
                return cell
            }
            
            
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 240
        } else {
            if indexPath.row == 0 {
                return 50
            } else {
                return 90
            }
            
        }
        
    }
    
    
}

extension TeamComparisonVC: CompareTeamDelegate {
    
    func didPickTeam(team: Team) {
        if self.firstTeam == nil {
            self.firstTeam = team
            if let teamId = self.firstTeam?.id {
                FootballApi.getTeamMainSeasonStats(teamId: teamId) { [weak self] result in
                    switch result {
                    case .failure(let error):
                        print("Error")
                    case .success(let statNumbers):
                        self?.firstTeamStats = statNumbers
                        print("StatNumbers: \(statNumbers)")
                        self?.statsDict = GlobalFunctions.currateListOfTeamStats(firstTeamStat: statNumbers, secondTeamStat: nil)
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    }
                }

            }
        } else {
            self.secondTeam = team
            if let teamId = self.secondTeam?.id, let firstTeamStats = self.firstTeamStats {
                FootballApi.getTeamMainSeasonStats(teamId: teamId) { [weak self] result in
                    switch result {
                    case .failure(let error):
                        print("Error")
                    case .success(let statNumbers):
                        self?.statsDict = GlobalFunctions.currateListOfTeamStats(firstTeamStat: firstTeamStats, secondTeamStat: statNumbers)
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                            self?.doneButton.isHidden = false
                            if let firstTeamName = self?.firstTeam?.short_code, let secondTeamName = self?.secondTeam?.short_code {
                                self?.navigationItem.title = "\(firstTeamName) VS \(secondTeamName)"
                            }
                        }
                    }
                }
            }
        }
        
        //tableView.reloadData()
    }
    
    func wantsToChangeTeam() {
        presentSearchTeamVC()
    }
}

extension TeamComparisonVC {
    
    func rearrangeKeys(keys: Array<String>) -> [StatKey] {
        var newKeys = [StatKey]()
        for key in keys {
            if key == "GAMES" {
                newKeys.append(StatKey(key: key, priority: 0))
            } else if key == "GOALS SCORED" {
                newKeys.append(StatKey(key: key, priority: 1))
            } else if key == "GOALS CONCEDED" {
                newKeys.append(StatKey(key: key, priority: 2))
            } else if key == "CLEAN SHEETS" {
                newKeys.append(StatKey(key: key, priority: 3))
            }
        }
        
        return newKeys.sorted(by: {$0.priority < $1.priority})
    }
}
