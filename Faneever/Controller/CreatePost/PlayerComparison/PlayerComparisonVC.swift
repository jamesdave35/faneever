//
//  PlayerComparisonVC.swift
//  Faneever
//
//  Created by James Meli on 3/4/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import BottomPopup

class PlayerSearchComparisonVC: UIViewController {
    

    @IBOutlet weak var playerSearchTableView: UITableView!
    
    var searchController: UISearchController?
    var players = [Player]()
    var delegate: ComparePlayerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavBar()
        configureUiElements()
        
    }
    

    
    private func configureNavBar() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
            searchController = UISearchController(searchResultsController: nil)
            searchController?.searchBar.tintColor = Colors.greenBrandColor
            searchController?.searchBar.keyboardAppearance = .default
            searchController?.searchBar.delegate = self
            searchController?.obscuresBackgroundDuringPresentation = false
            searchController?.searchBar.placeholder = "search player by last name"
            searchController?.searchBar.barStyle = .default
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            
        }
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissView))
        cancelButton.tintColor = Colors.greenBrandColor
        navigationItem.leftBarButtonItem = cancelButton
        navigationItem.title = "Search Player"
        
    }
    
    
    private func configureUiElements() {
        playerSearchTableView.delegate = self
        playerSearchTableView.dataSource = self
        playerSearchTableView.tableFooterView = UIView()
        playerSearchTableView.keyboardDismissMode = .onDrag
    }
    
    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
}
    

    





extension PlayerSearchComparisonVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count >= 4 {
            FootballApi.getPlayersFromSearch(searchString: searchText) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let players):
                    self?.players = players
                    DispatchQueue.main.async {
                        self?.playerSearchTableView.reloadData()
                    }
                }
            }

        } else {
            self.players = []
            self.playerSearchTableView.reloadData()
        }

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.players = []
        self.playerSearchTableView.reloadData()
    }
}

extension PlayerSearchComparisonVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        players.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = playerSearchTableView.dequeueReusableCell(withIdentifier: "PlayerCompSearchCell", for: indexPath) as! PlayerCompSearchCell
        cell.resetContent()
        cell.player = players[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let player = players[indexPath.row]
        delegate?.didPickPlayer(player: player)
        self.searchController?.dismiss(animated: true, completion: nil)
        dismissView()
    }
}
