//
//  ComparePlayerVC.swift
//  Faneever
//
//  Created by James Meli on 3/5/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import BottomPopup
import EZYGradientView
import Hero

class ComparePlayerVC: UITableViewController {
    
    var firstPlayer: Player?
    var secondPlayer: Player?

    
    var statsDict = [String: [StatComparison]]()
    var firstPlayerStat: PlayerStatNumbers?
    var secondPlayerStat: PlayerStatNumbers?
    var doneButton = UIBarButtonItem()
    var delegate: CreatePostDelegate?
    
    var canChoosePlayer = false
    
    var heroId: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        configureUiElements()
        
        if let id = heroId {
            //self.tableView.hero.id = id
        }
    }
    
    private func configureNavBar() {
        navigationController?.navigationBar.barStyle = .default
        if let firstName = self.firstPlayer?.getCommonName(), let secondName = self.secondPlayer?.getCommonName() {
            navigationItem.title = "\(firstName) vs \(secondName)"
        } else {
            navigationItem.title = "Compare Players"
        }
        
        navigationController?.navigationBar.titleTextAttributes = Constants.navBarTitleAttributes
        doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneSelectingPlayers))
        doneButton.tintColor = Colors.secondaryText
        doneButton.isEnabled = false
        if canChoosePlayer {
            navigationItem.rightBarButtonItem = doneButton
        }
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissView))
        cancelButton.tintColor = Colors.greenBrandColor
        navigationItem.leftBarButtonItem = cancelButton
        
    }
    
    private func configureUiElements() {
        tableView.tableFooterView = UIView()
        if canChoosePlayer {
            presentSearchPlayerVC()
        } 
        


    }
    
    @objc private func doneSelectingPlayers() {
        if let playerOne = firstPlayer, let playerTwo = secondPlayer {
            delegate?.didFinishChoosingPlayersToCompare(firstPlayer: playerOne, secondPlayer: playerTwo, playerStats: statsDict)
        }
        dismissView()
    }
    
    
    @objc private func presentSearchPlayerVC() {
        let searchPlayerVC = storyboard?.instantiateViewController(withIdentifier: "PlayerSearchComparisonVC") as! PlayerSearchComparisonVC
        searchPlayerVC.delegate = self
        let navController = UINavigationController(rootViewController: searchPlayerVC)
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc private func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    

    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return statsDict.keys.count + 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 || section == 2 {
            return 1
        } else {
          let keys = rearrangeKeys(keys: Array(statsDict.keys))
          if let count = statsDict[keys[section - 3].key]?.count {
              return count + 1
          } else {
              return 0
          }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FirstStatCell", for: indexPath) as! FirstStatCell
            cell.firstPlayer = firstPlayer
            cell.secondPlayer = secondPlayer
            cell.canChoosePlayer = canChoosePlayer
            cell.delegate = self
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CompPlayerLeagueCell", for: indexPath) as! CompPlayerLeagueCell
            cell.delegate = self
            cell.firstPlayerLeague = firstPlayerStat?.league?.data
            cell.firstPlayerSeason = firstPlayerStat?.season?.data
            cell.secondPlayerSeason = secondPlayerStat?.season?.data
            cell.secondPlayerLeague = secondPlayerStat?.league?.data
            cell.firstPlayerId = firstPlayer?.player_id
            cell.secondPlayerId = secondPlayer?.player_id
            return cell
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SecondStatCell", for: indexPath) as! SecondStatCell
            cell.firstPlayer = firstPlayer
            cell.secondPlayer = secondPlayer
            return cell
        } else {
            let keys = rearrangeKeys(keys: Array(statsDict.keys))
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "StatHeaderCell", for: indexPath) as! StatHeaderCell
                cell.header = keys[indexPath.section - 3].key
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ThirdStatcell", for: indexPath) as! ThirdStatcell
                cell.resetContent()
                if let stats = statsDict[keys[indexPath.section - 3].key] {
                    cell.playerStat = stats[indexPath.row - 1]
                }
                
                return cell
            }


            
            
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 260
        } else if indexPath.section == 1 {
            return 80
        } else if indexPath.section == 2 {
            return 280
        } else {
            if indexPath.row == 0 {
                return 50
            } else {
                return 90
            }
            
        }
  
    }



}

extension ComparePlayerVC: ComparePlayerDelegate {
    
    func didPickPlayer(player: Player) {
        if self.firstPlayer == nil {
            firstPlayer = player
            firstPlayer?.getTeam(completion: { [weak self] result in
                switch result {
                case .success(let team):
                    if let seasonId = team?.current_season_id, let playerId = self?.firstPlayer?.player_id {
                        FootballApi.getPlayerSeasonStats(playerId: playerId, seasonId: seasonId) { [weak self] result in
                            switch result {
                            case .failure(let error):
                                print("Error")
                            case .success(let statNumbers):
                                self?.firstPlayerStat = statNumbers
                                self?.statsDict = GlobalFunctions.currateListOfPlayerStats(firstStat: statNumbers, secondStat: nil)
                                self?.tableView.reloadData()
                            }
                        }
                    }
                case .failure(let error):
                    print("Error")
                }
            })
           
        } else {
            secondPlayer = player
            secondPlayer?.getTeam(completion: { [weak self] result in
                switch result {
                case .success(let team):
                    if let seasonId = team?.current_season_id, let playerId = self?.secondPlayer?.player_id, let firstStat = self?.firstPlayerStat {
                        FootballApi.getPlayerSeasonStats(playerId: playerId, seasonId: seasonId) { [weak self] result in
                            switch result {
                            case .failure(let error):
                                print("Error")
                            case .success(let statNumbers):
                                self?.secondPlayerStat = statNumbers
                                self?.statsDict = GlobalFunctions.currateListOfPlayerStats(firstStat: firstStat, secondStat: statNumbers)
                                self?.tableView.reloadData()
                                self?.doneButton.isEnabled = true
                                self?.doneButton.tintColor = Colors.greenBrandColor
                                if let firstName = self?.firstPlayer?.getCommonName(), let secondName = self?.secondPlayer?.getCommonName() {
                                    self?.navigationItem.title = "\(firstName) vs \(secondName)"
                                }
                            }
                        }

                    }
                case .failure(let error):
                    print("Error")
                }
            })
            
        }
    }
    
    func wantsToChangePlayer() {
        self.presentSearchPlayerVC()
    }
    
    func wantsToChangeFirstPlayerSeason(playerId: Int) {
        let chooseSeasonVC = storyboard?.instantiateViewController(withIdentifier: "ChooseSeasonVC") as! ChooseSeasonVC
        chooseSeasonVC.delegate = self
        chooseSeasonVC.playerFlag = true
        chooseSeasonVC.playerId = playerId
        self.present(chooseSeasonVC, animated: true, completion: nil)
        
    }
    
    func wantsToChangeSecondPlayerSeason(playerId: Int) {
        let chooseSeasonVC = storyboard?.instantiateViewController(withIdentifier: "ChooseSeasonVC") as! ChooseSeasonVC
        chooseSeasonVC.playerId = playerId
        chooseSeasonVC.playerFlag = false
        chooseSeasonVC.delegate = self
        self.present(chooseSeasonVC, animated: true, completion: nil)
        
    }
    
    func didChangeFirstPlayerSeason(playerStats: PlayerStatNumbers) {
        firstPlayerStat = playerStats
        statsDict = GlobalFunctions.currateListOfPlayerStats(firstStat: playerStats, secondStat: secondPlayerStat)
        tableView.reloadData()
    }
    
    func didChangeSecondPlayerSeason(playerStats: PlayerStatNumbers) {
        secondPlayerStat = playerStats
        statsDict = GlobalFunctions.currateListOfPlayerStats(firstStat: firstPlayerStat!, secondStat: playerStats)
        tableView.reloadData()
    }
}

extension ComparePlayerVC {
    
    func rearrangeKeys(keys: Array<String>) -> [StatKey] {
        var newKeys = [StatKey]()
        for key in keys {
            if key == "TOP STATS" {
                newKeys.append(StatKey(key: key, priority: 0))
            } else if key == "DISTRIBUTION" {
                newKeys.append(StatKey(key: key, priority: 1))
            } else if key == "TAKE ONS" {
                newKeys.append(StatKey(key: key, priority: 2))
            } else if key == "DEFENSE" {
                newKeys.append(StatKey(key: key, priority: 3))
            } else if key == "PENALTIES" {
                newKeys.append(StatKey(key: key, priority: 4))
            } else if key == "SAVES" {
                newKeys.append(StatKey(key: key, priority: 5))
            } else {
                newKeys.append(StatKey(key: key, priority: 6))
            }
        }
        
        return newKeys.sorted(by: {$0.priority < $1.priority})
    }
}
