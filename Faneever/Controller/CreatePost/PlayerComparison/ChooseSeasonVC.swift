//
//  ChooseSeasonVC.swift
//  Faneever
//
//  Created by James Meli on 5/31/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation
import UIKit
import BottomPopup

class ChooseSeasonVC: BottomPopupViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var playerId: Int?
    var playerSeasons = [PlayerStatNumbers]()
    
    var playerFlag: Bool?
    
    var delegate: ComparePlayerDelegate?
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        getPlayerSeasonsStats()
    }
    
    override var popupHeight: CGFloat {
        return 500
    }
    
    private func getPlayerSeasonsStats() {
        if let id = playerId {
            FootballApi.getPlayerAllSeasonsStats(playerId: id) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let playerSeasonsStats):
                    self?.playerSeasons = playerSeasonsStats
                    
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    
    
}

extension ChooseSeasonVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerSeasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseCompPlayerSeasonCell", for: indexPath) as! ChooseCompPlayerSeasonCell
        cell.playerLeague = playerSeasons[indexPath.row].league?.data
        cell.playerSeason = playerSeasons[indexPath.row].season?.data
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let flag = playerFlag {
            let stats = playerSeasons[indexPath.row]
            if flag {
                delegate?.didChangeFirstPlayerSeason(playerStats: stats)
            } else {
                delegate?.didChangeSecondPlayerSeason(playerStats: stats)
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
}
