//
//  HomeVC.swift
//  Faneever
//
//  Created by James Meli on 3/1/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Firebase
import Hero

class HomeVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createPostButton: UIView!
    
    private var posts = [Post]()
    private var reference: CollectionReference?
    private var messageListener: ListenerRegistration?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        configureUiElements()
        getCurrentUser()
        listenForPosts()
        getPosts()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    

    
    private func configureNavBar() {
        navigationController?.navigationBar.barTintColor = UIColor(named: "secondaryBackground")
        navigationController?.navigationBar.tintColor = Colors.greenBrandColor
        navigationItem.title = "Chatter"
        navigationController?.navigationBar.titleTextAttributes = Constants.navBarTitleAttributes
        tabBarController?.delegate = self
       // addNavBarImage()
    }
    
    private func addNavBarImage() {
        
        let image = UIImage(named: "smallLogo")
        let faneeverLogo = UIImageView(image: image)
        
        let bannerWidth = navigationController!.navigationBar.frame.size.width
        let bannerHeight = navigationController!.navigationBar.frame.size.height
        
        let bannerX = bannerWidth / 2 - image!.size.width / 2
        let bannerY = bannerHeight / 2 - image!.size.height / 2
        
        faneeverLogo.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        faneeverLogo.contentMode = .scaleAspectFit
        
        navigationItem.titleView = faneeverLogo
        
    }
    
    private func configureUiElements() {
        tableView.separatorColor = Colors.seperatorColor.withAlphaComponent(0.5)
        createPostButton.applyShadow(cornerRadius: createPostButton.bounds.height / 2, shadowOpacity: 0.25, radius: 4, offset: .zero)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(presentNewPostVC))
        createPostButton.addGestureRecognizer(tapGesture)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()


    }
    
    @objc private func presentNewPostVC() {
        GlobalFunctions.playSelectionFeedbackGenerator()
        let newPostVC = storyboard?.instantiateViewController(withIdentifier: "NewPostVC") as! NewPostVC
        let navController = UINavigationController(rootViewController: newPostVC)
        navController.modalPresentationStyle = .pageSheet
        present(navController, animated: true, completion: nil)
    }
    
    private func getCurrentUser() {
        DatabaseServices.getCurrentUser { (user) in
            if let user = user {
                Constants.currentUser = user
            }
        }
    }
    
    private func listenForPosts() {
        reference = Firestore.firestore().collection("Posts")
        messageListener = reference?.addSnapshotListener({ (querySnapshot, error) in
            guard let snapshot = querySnapshot  else {
                print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
                return
                
            }
            
            snapshot.documentChanges.forEach { (change) in
                guard var data = change.document.data() as? NSDictionary else { return }
                
                let post = Post(data: data)
                post.setPostData { (success) in
                 switch change.type {
                    case .added:
                        self.posts.append(post)
                        self.posts.sort(by: {$0.timeStamp > $1.timeStamp})
                        self.tableView.reloadData()
                    default:
                        break
                    }
                }

            }
            

            
                
            
        })
    }
    
    private func getPosts() {
        DatabaseServices.getPosts { (userPosts, error) in
            if error != nil {
                print("Error getting posts: \(error?.localizedDescription)")
            } else {
                self.posts = userPosts.sorted(by: {$0.timeStamp > $1.timeStamp})
                self.tableView.reloadData()

                
            }
        }
    }
    
    deinit {
        messageListener?.remove()
    }
    


}

extension HomeVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = posts[indexPath.row]
        if post.isTextPost() {
          let cell = tableView.dequeueReusableCell(withIdentifier: "ChatterPostCell", for: indexPath) as! ChatterPostCell
            cell.resetContent()
            cell.post = post
            
            return cell
        } else if post.hasImage() {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImagePostCell", for: indexPath) as! ImagePostCell
            cell.resetContent()
            cell.post = post
            
            return cell
        } else if post.hasVideo() {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoPostCell", for: indexPath) as! VideoPostCell
            cell.resetContent()
            cell.videoPlayer.playerLayer.player?.play()
            cell.delegate = self
            cell.post = post
            return cell
        } else if post.isPlayerComparison() {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCompPostCell", for: indexPath) as! PlayerCompPostCell
            cell.resetContent()
            cell.delegate = self
            cell.post = post
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCompCell", for: indexPath) as! TeamCompCell
            cell.resetContent()
            cell.delegate = self
            cell.post = post
            return cell
        }
        

    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = posts[indexPath.row]
        let postPageVC = storyboard?.instantiateViewController(withIdentifier: "PostPageVC") as! PostPageVC
        postPageVC.post = post
        self.navigationController?.pushViewController(postPageVC, animated: true)
    }
    

}

extension HomeVC: UITabBarControllerDelegate {
    
//    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//
//    }
    
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        let tabbarIndex = tabBarController.selectedIndex
//
//        if tabbarIndex == 0 {
//            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
//
//        }
//
//        return true
//    }
}



extension HomeVC: PostCellDelegate {
    
    func didPlayVideo(videoUrl: URL) {
        let player = AVPlayer(url: videoUrl)
        let vc = AVPlayerViewController()
        vc.player = player
        present(vc, animated: true) {
            vc.player?.play()
        }
    }
    
    func wantsToReplyToPost(post: Post) {
        GlobalFunctions.playSelectionFeedbackGenerator()
        let newPostVC = storyboard?.instantiateViewController(withIdentifier: "NewPostVC") as! NewPostVC
        newPostVC.isReply = true
        newPostVC.replyPost = post
        let navController = UINavigationController(rootViewController: newPostVC)
        navController.modalPresentationStyle = .pageSheet
        present(navController, animated: true, completion: nil)
    }
    
    func didSelectPlayerStats(firstPlayer: Player, secondPlayer: Player, statsDict: [String : [StatComparison]], indexPath: IndexPath) {
        let comparePlayerVC = storyboard?.instantiateViewController(withIdentifier: "ComparePlayerVC") as! ComparePlayerVC
        let cell = tableView.cellForRow(at: indexPath) as! PlayerCompPostCell
        let heroId = UUID().uuidString
       // cell.compareView.hero.id = heroId
        //comparePlayerVC.hero.isEnabled = true
        //comparePlayerVC.heroId = heroId
        comparePlayerVC.canChoosePlayer = false
        comparePlayerVC.firstPlayer = firstPlayer
        comparePlayerVC.secondPlayer = secondPlayer
        comparePlayerVC.statsDict = statsDict
        let navController = UINavigationController(rootViewController: comparePlayerVC)
       // navController.hero.isEnabled = true
        
        present(navController, animated: true, completion: nil)
    }
    
    func didSelectTeamStats(firstTeam: Team, secondTeam: Team, statsDict: [String : [StatComparison]]) {
        let teamCompVC = storyboard?.instantiateViewController(withIdentifier: "TeamComparisonVC") as! TeamComparisonVC
        teamCompVC.canChooseTeam = false
        teamCompVC.firstTeam = firstTeam
        teamCompVC.secondTeam = secondTeam
        teamCompVC.statsDict = statsDict
        let navController = UINavigationController(rootViewController: teamCompVC)
        present(navController, animated: true, completion: nil)
    }
}
