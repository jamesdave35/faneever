//
//  FixtureOverviewVC.swift
//  Faneever
//
//  Created by James Meli on 3/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class FixtureOverviewVC: UITableViewController {

    @IBOutlet weak var fixtureStations: UILabel!
    @IBOutlet weak var tvLabel: UILabel!
    @IBOutlet weak var tvIcon: UIImageView!
    @IBOutlet weak var iconViewFive: UIView!
    @IBOutlet weak var fixtureDate: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var clockIcon: UIImageView!
    @IBOutlet weak var iconViewFour: UIView!
    @IBOutlet weak var iconViewThree: UIView!
    @IBOutlet weak var iconViewTwo: UIView!
    @IBOutlet weak var iconViewOne: UIView!
    @IBOutlet weak var fourthContainerView: AnimatableView!
    @IBOutlet weak var goalScorerCollectionView: UICollectionView!
    @IBOutlet weak var matchEventsLabel: UILabel!
    @IBOutlet weak var thirdContainerView: AnimatableView!
    @IBOutlet weak var eventsCollectionView: UICollectionView!
    @IBOutlet weak var formLabelTen: AnimatableLabel!
    @IBOutlet weak var formLabelNine: AnimatableLabel!
    @IBOutlet weak var formLabelEight: AnimatableLabel!
    @IBOutlet weak var formLabelSeven: AnimatableLabel!
    @IBOutlet weak var formLabel6: AnimatableLabel!
    @IBOutlet weak var formLabelFive: AnimatableLabel!
    @IBOutlet weak var formLabel4: AnimatableLabel!
    @IBOutlet weak var formLabelThree: AnimatableLabel!
    @IBOutlet weak var formLabelTwo: AnimatableLabel!
    @IBOutlet weak var formLabelOne: AnimatableLabel!
    @IBOutlet weak var teamFormLabel: UILabel!
    @IBOutlet weak var refereeLabel: UILabel!
    @IBOutlet weak var stadiumLabel: UILabel!
    @IBOutlet weak var refereeName: UILabel!
    @IBOutlet weak var venueName: UILabel!
    @IBOutlet weak var roundName: PaddingLabel!
    @IBOutlet weak var leagueName: UILabel!
    @IBOutlet weak var leagueLogo: UIImageView!
    @IBOutlet weak var secondContainerView: AnimatableView!
    @IBOutlet weak var whoWollWinLabel: UILabel!
    @IBOutlet weak var awayTeamName: UILabel!
    @IBOutlet weak var thirdPredicationButton: AnimatableButton!
    @IBOutlet weak var drawLabel: UILabel!
    @IBOutlet weak var secondPredictionButton: AnimatableButton!
    @IBOutlet weak var homeTeamName: UILabel!
    @IBOutlet weak var firstPredictionButton: AnimatableButton!
    @IBOutlet weak var firstContainerView: AnimatableView!
    
    var fixture: Fixture?
    var events = [FixtureEvent]()
    var goalEvents = [FixtureEvent]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        setFixtureOverViewData()
        
    }
    
    private func configureUiElements() {
        tableView.backgroundColor = Colors.primaryBackground
        firstContainerView.backgroundColor = Colors.secondaryBackground
        secondContainerView.backgroundColor = Colors.secondaryBackground
        thirdContainerView.backgroundColor = Colors.secondaryBackground
        fourthContainerView.backgroundColor = Colors.secondaryBackground
        matchEventsLabel.textColor = Colors.secondaryText
        whoWollWinLabel.textColor = Colors.primaryText
        iconViewOne.backgroundColor = Colors.tertiaryBackgroundColor
        iconViewTwo.backgroundColor = Colors.tertiaryBackgroundColor
        iconViewThree.backgroundColor = Colors.tertiaryBackgroundColor
        iconViewFour.backgroundColor = Colors.tertiaryBackgroundColor
        iconViewFive.backgroundColor = Colors.tertiaryBackgroundColor
        clockIcon.image = UIImage(systemName: "clock.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 12, weight: .light))
        tvIcon.image = UIImage(systemName: "tv.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 12, weight: .light))
        firstPredictionButton.setTitleColor(Colors.secondaryText, for: .normal)
        firstPredictionButton.borderColor = Colors.secondaryText
        secondPredictionButton.setTitleColor(Colors.secondaryText, for: .normal)
        secondPredictionButton.borderColor = Colors.secondaryText
        thirdPredicationButton.setTitleColor(Colors.secondaryText, for: .normal)
        thirdPredicationButton.borderColor = Colors.secondaryText
        homeTeamName.textColor = Colors.primaryText
        awayTeamName.textColor = Colors.primaryText
        drawLabel.textColor = Colors.primaryText
        leagueName.textColor = Colors.primaryText
        roundName.textColor = Colors.secondaryText
        roundName.fillColor = Colors.tertiaryBackgroundColor
        stadiumLabel.textColor = Colors.primaryText
        refereeLabel.textColor = Colors.primaryText
        dateLabel.textColor = Colors.primaryText
        tvLabel.textColor = Colors.primaryText
        fixtureDate.textColor = Colors.secondaryText
        fixtureStations.textColor = Colors.secondaryText
        teamFormLabel.textColor = Colors.primaryText
        venueName.textColor = Colors.secondaryText
        refereeName.textColor = Colors.secondaryText
        eventsCollectionView.tag = 1
        goalScorerCollectionView.tag = 2
        eventsCollectionView.delegate = self
        eventsCollectionView.dataSource = self
        goalScorerCollectionView.delegate = self
        goalScorerCollectionView.dataSource = self

        
    }
    
    private func setFixtureOverViewData() {
        if let fix = self.fixture {
            if let homeName = fix.getLocalTeam()?.short_code, let awayName = fix.getVisitorTeam()?.short_code {
                homeTeamName.text = homeName
                awayTeamName.text = awayName
            }
            
            if let date = fix.getSchedule()?.getDateString(), let time = fix.getSchedule()?.getTimeString() {
                fixtureDate.text = "\(date), \(time)"
            }
            
            fixtureStations.text = "Fox Sports, NBC Sports"
            
            if let url = fix.getLeagueLogo(), let name = fix.getLeagueName() {
                leagueName.text = name
                leagueLogo.sd_setImage(with: url, placeholderImage: Constants.defaultTeamImage)
            }
        
            if let round = fix.getRoundName() {
                roundName.text = "Round \(round)"
            } else {
                if let stage = fix.getStageName() {
                    roundName.text = stage
                }
            }
            
            if let name = fix.getVenueName() {
                venueName.text = name
                
            }
            
            if let referee = fix.getRefereeName() {
                refereeName.text = referee
            }
            
            fix.getLocalTeam()?.getTeamForm(completion: { [weak self] form in
                self?.setHomeTeamFormLabel(team: form)
            })
            
            fix.getVisitorTeam()?.getTeamForm(completion: { [weak self] form in
                self?.setAwayTeamFormLabel(team: form)
            })
            
            if let fixEvents = fix.events?.data {
                self.events = fixEvents
                self.goalEvents = fixEvents.filter({$0.eventType() == "goal"})
                eventsCollectionView.reloadData()
                goalScorerCollectionView.reloadData()
               // tableView.reloadRows(at: [IndexPath(row: 0, section: 0), IndexPath(row: 1, section: 0)], with: .none)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            if self.goalEvents.isEmpty {
                return 0
            } else {
                return 118 + CGFloat(goalEvents.count * 18)
            }
        case 1:
            if self.events.isEmpty {
                return 0
            } else {
                return 60 + CGFloat((events.count * 66) + 10)
            }
            
            return 0
        case 2:
            return 215
        case 3:
            return 390
        default:
            return 200
        }

    }
    
    
    

    @IBAction func homeTeamWinPressed(_ sender: Any) {
        
    }
    
    @IBAction func drawPressed(_ sender: Any) {
        
    }
    
    @IBAction func awayTeamWinPressed(_ sender: Any) {
        
    }
    
    
}

extension FixtureOverviewVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
           return events.count
        } else {
            return goalEvents.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 1 {
            switch events[indexPath.row].eventType() {
            case "goal":
                let cell = eventsCollectionView.dequeueReusableCell(withReuseIdentifier: "GoalCell", for: indexPath) as! GoalCell
                let event = EventFix(fixture: events[indexPath.row], isHomeTeamEvent: events[indexPath.row].isHomeTeamEvent(fixture: self.fixture!))
                cell.event = event
                return cell
            case "card":
                let cell = eventsCollectionView.dequeueReusableCell(withReuseIdentifier: "CardCell", for: indexPath) as! CardCell
                let event = EventFix(fixture: events[indexPath.row], isHomeTeamEvent: events[indexPath.row].isHomeTeamEvent(fixture: self.fixture!))
                cell.event = event
                return cell
            case "substitution":
                let cell = eventsCollectionView.dequeueReusableCell(withReuseIdentifier: "SubstitutionCell", for: indexPath) as! SubstitutionCell
                let event = EventFix(fixture: events[indexPath.row], isHomeTeamEvent: events[indexPath.row].isHomeTeamEvent(fixture: self.fixture!))
                cell.event = event
                return cell
            default:
                let cell = eventsCollectionView.dequeueReusableCell(withReuseIdentifier: "GoalCell", for: indexPath) as! GoalCell
                return cell
            }
        } else {
            let cell = goalScorerCollectionView.dequeueReusableCell(withReuseIdentifier: "GoalScorerCell", for: indexPath) as! GoalScorerCell
            let event = EventFix(fixture: goalEvents[indexPath.row], isHomeTeamEvent: goalEvents[indexPath.row].isHomeTeamEvent(fixture: self.fixture!))
            cell.event = event
            return cell
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1 {
            return CGSize(width: collectionView.bounds.width, height: 50)
        } else {
            return CGSize(width: collectionView.bounds.width, height: 13)
        }
        
    }
}

extension FixtureOverviewVC {
    
    func setHomeTeamFormLabel(team: String) {
        let teamForm = Array(team)
        if !teamForm.isEmpty {
            switch teamForm[0] {
            case "W":
                formLabelOne.text = "W"
                formLabelOne.textColor = .systemGreen
                formLabelOne.borderColor = .systemGreen
            case "L":
                formLabelOne.text = "L"
                formLabelOne.textColor = .systemRed
                formLabelOne.borderColor = .systemRed
            case "D":
                formLabelOne.text = "D"
                formLabelOne.textColor = Colors.secondaryText
                formLabelOne.borderColor = Colors.secondaryText
            default:
                break
            }
            
            switch teamForm[1] {
            case "W":
                formLabelTwo.text = "W"
                formLabelTwo.textColor = .systemGreen
                formLabelTwo.borderColor = .systemGreen
            case "L":
                formLabelTwo.text = "L"
                formLabelTwo.textColor = .systemRed
                formLabelTwo.borderColor = .systemRed
            case "D":
                formLabelTwo.text = "D"
                formLabelTwo.textColor = Colors.secondaryText
                formLabelTwo.borderColor = Colors.secondaryText
            default:
                break
            }
            
            switch teamForm[2] {
            case "W":
                formLabelThree.text = "W"
                formLabelThree.textColor = .systemGreen
                formLabelThree.borderColor = .systemGreen
            case "L":
                formLabelThree.text = "L"
                formLabelThree.textColor = .systemRed
                formLabelThree.borderColor = .systemRed
            case "D":
                formLabelThree.text = "D"
                formLabelThree.textColor = Colors.secondaryText
                formLabelThree.borderColor = Colors.secondaryText
            default:
                break
            }
            
            switch teamForm[3] {
            case "W":
                formLabel4.text = "W"
                formLabel4.textColor = .systemGreen
                formLabel4.borderColor = .systemGreen
            case "L":
                formLabel4.text = "L"
                formLabel4.textColor = .systemRed
                formLabel4.borderColor = .systemRed
            case "D":
                formLabel4.text = "D"
                formLabel4.textColor = Colors.secondaryText
                formLabel4.borderColor = Colors.secondaryText
            default:
                break
            }
            
            switch teamForm[4] {
            case "W":
                formLabelFive.text = "W"
                formLabelFive.textColor = .systemGreen
                formLabelFive.borderColor = .systemGreen
            case "L":
                formLabelFive.text = "L"
                formLabelFive.textColor = .systemRed
                formLabelFive.borderColor = .systemRed
            case "D":
                formLabelFive.text = "D"
                formLabelFive.textColor = Colors.secondaryText
                formLabelFive.borderColor = Colors.secondaryText
            default:
                break
            }
        }

    }
    
    func setAwayTeamFormLabel(team: String) {
        let teamForm = Array(team)
        if !teamForm.isEmpty {
            switch teamForm[0] {
            case "W":
                formLabel6.text = "W"
                formLabel6.textColor = .systemGreen
                formLabel6.borderColor = .systemGreen
            case "L":
                formLabel6.text = "L"
                formLabel6.textColor = .systemRed
                formLabel6.borderColor = .systemRed
            case "D":
                formLabel6.text = "D"
                formLabel6.textColor = Colors.secondaryText
                formLabel6.borderColor = Colors.secondaryText
            default:
                break
            }
            
            switch teamForm[1] {
            case "W":
                formLabelSeven.text = "W"
                formLabelSeven.textColor = .systemGreen
                formLabelSeven.borderColor = .systemGreen
            case "L":
                formLabelSeven.text = "L"
                formLabelSeven.textColor = .systemRed
                formLabelSeven.borderColor = .systemRed
            case "D":
                formLabelSeven.text = "D"
                formLabelSeven.textColor = Colors.secondaryText
                formLabelSeven.borderColor = Colors.secondaryText
            default:
                break
            }
            
            switch teamForm[2] {
            case "W":
                formLabelEight.text = "W"
                formLabelEight.textColor = .systemGreen
                formLabelEight.borderColor = .systemGreen
            case "L":
                formLabelEight.text = "L"
                formLabelEight.textColor = .systemRed
                formLabelEight.borderColor = .systemRed
            case "D":
                formLabelEight.text = "D"
                formLabelEight.textColor = Colors.secondaryText
                formLabelEight.borderColor = Colors.secondaryText
            default:
                break
            }
            
            switch teamForm[3] {
            case "W":
                formLabelNine.text = "W"
                formLabelNine.textColor = .systemGreen
                formLabelNine.borderColor = .systemGreen
            case "L":
                formLabelNine.text = "L"
                formLabelNine.textColor = .systemRed
                formLabelNine.borderColor = .systemRed
            case "D":
                formLabelNine.text = "D"
                formLabelNine.textColor = Colors.secondaryText
                formLabelNine.borderColor = Colors.secondaryText
            default:
                break
            }
            
            switch teamForm[4] {
            case "W":
                formLabelTen.text = "W"
                formLabelTen.textColor = .systemGreen
                formLabelTen.borderColor = .systemGreen
            case "L":
                formLabelTen.text = "L"
                formLabelTen.textColor = .systemRed
                formLabelTen.borderColor = .systemRed
            case "D":
                formLabelTen.text = "D"
                formLabelTen.textColor = Colors.secondaryText
                formLabelTen.borderColor = Colors.secondaryText
            default:
                break
            }
        }

    }
}
