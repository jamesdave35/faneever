//
//  FixtureStatsVC.swift
//  Faneever
//
//  Created by James Meli on 4/6/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class FixtureStatsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var fixture: Fixture?
    var statsDict: [String: [StatComparison]] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = Colors.alternatePrimaryBackground
        tableView.delegate = self
        tableView.dataSource = self
        loadTableView()
        
    }
    
    private func loadTableView() {
        if let homeTeamStats = fixture?.getHomeTeamStats(), let awayTeamStat = fixture?.getAwayTeamStats() {
             statsDict = GlobalFunctions.currateListOfFixtureStats(firstTeamStat: homeTeamStats, secondTeamStat: awayTeamStat)
            tableView.reloadData()
        }
    }
    




}

extension FixtureStatsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return statsDict.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let keys = rearrangeKeys(keys: Array(statsDict.keys))
        if let count = statsDict[keys[section].key]?.count {
            return count + 1
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let keys = rearrangeKeys(keys: Array(statsDict.keys))
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FixtureStatsHeaderCell", for: indexPath) as! FixtureStatsHeaderCell
            cell.header = keys[indexPath.section].key
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FixtureStatsCell", for: indexPath) as! FixtureStatsCell
            if let stats = statsDict[keys[indexPath.section].key] {
                cell.teamStat = stats[indexPath.row - 1]
            }
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 50
        } else {
            return 90
        }
    }
}

extension FixtureStatsVC {
    
    func rearrangeKeys(keys: Array<String>) -> [StatKey] {
        var newKeys = [StatKey]()
        for key in keys {
            if key == "Top Stats" {
                newKeys.append(StatKey(key: key, priority: 0))
            } else if key == "Shots" {
                newKeys.append(StatKey(key: key, priority: 1))
            } else if key == "Passes" {
                newKeys.append(StatKey(key: key, priority: 2))
            } else if key == "Attacks" {
                newKeys.append(StatKey(key: key, priority: 3))
            } else if key == "Set Pieces" {
                newKeys.append(StatKey(key: key, priority: 4))
            } else {
                newKeys.append(StatKey(key: key, priority: 5))
            }
        }
        
        return newKeys.sorted(by: {$0.priority < $1.priority})
    }
}

struct StatKey {
    var key: String
    var priority: Int
}

