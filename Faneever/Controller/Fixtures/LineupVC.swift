//
//  LineupVC.swift
//  Faneever
//
//  Created by James Meli on 4/5/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class LineupVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var fixture: Fixture?
    
    var homeBenchPlayers = [LineupPlayer]()
    var awayBenchPlayers = [LineupPlayer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       configureUiElements()
    }
    
    private func configureUiElements() {
        self.view.backgroundColor = Colors.primaryBackground
        if let localBenchPlayers = fixture?.getHomeBenchPlayers(), let visitorBenchPlayers = fixture?.getAwayBenchPlayers() {
            homeBenchPlayers = localBenchPlayers
            awayBenchPlayers = visitorBenchPlayers
        }
        tableView.delegate = self
        tableView.dataSource = self
        

    }



}

extension LineupVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 2
        case 2:
            return homeBenchPlayers.count
        default:
            return awayBenchPlayers.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LineupCell", for: indexPath) as! LineUpCell
            if let fix = fixture {
                cell.fixture = fix
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LineupCoachesCell", for: indexPath) as! LineupCoachesCell
            if indexPath.row == 0 {
                if let localCoach = fixture?.localCoach?.data, let teamName = fixture?.getLocalTeam()?.short_code {
                    let coach = FixtureAndCoach(teamName: teamName, coach: localCoach)
                        cell.coach = coach
                }
                } else {
                    if let awayCoach = fixture?.visitorCoach?.data, let teamName = fixture?.getVisitorTeam()?.short_code {
                        let coach = FixtureAndCoach(teamName: teamName, coach: awayCoach)
                        cell.coach = coach
                }
                
                
            }
                
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LineupBenchCell", for: indexPath) as! LineupBenchCell
            cell.player = homeBenchPlayers[indexPath.row]
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LineupBenchCell", for: indexPath) as! LineupBenchCell
            cell.player = awayBenchPlayers[indexPath.row]
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 1140
        } else if indexPath.section == 1 {
            return 65
        } else {
            return 80
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            return nil
        } else {
            let headerView = UIView()
            headerView.backgroundColor = Colors.alternatePrimaryBackground
            let headerLabel = UILabel()
            headerLabel.textColor = Colors.secondaryText
            headerLabel.font = UIFont.systemFont(ofSize: 13, weight: .regular)
            if section == 1 {
               headerLabel.text = "COACHES"
            } else if section == 2 {
                if let teamName = fixture?.getLocalTeam()?.name {
                    headerLabel.text = "\(teamName.uppercased()) BENCH"
                }
            } else {
                if let teamName = fixture?.getVisitorTeam()?.name {
                    headerLabel.text = "\(teamName.uppercased()) BENCH"
                }
            }
            
            headerLabel.translatesAutoresizingMaskIntoConstraints = false
            let verticalConstraint = NSLayoutConstraint(item: headerLabel, attribute: .centerY, relatedBy: .equal, toItem: headerView, attribute: .centerY, multiplier: 1, constant: 0)
            let leadingConstraint = NSLayoutConstraint(item: headerLabel, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1, constant: 15)
            headerView.addSubview(headerLabel)
            headerView.addConstraints([verticalConstraint, leadingConstraint])
            return headerView
        }
    }
    

}
