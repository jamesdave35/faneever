//
//  FixtureVC.swift
//  Faneever
//
//  Created by James Meli on 3/25/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable
import EZYGradientView

class FixtureVC: UIViewController, UICollectionViewDelegateFlowLayout  {
    

    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var fixtureDate: UILabel!
    @IBOutlet weak var awayTeamName: UILabel!
    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var timeView: AnimatableView!
    @IBOutlet weak var homeTeamName: UILabel!
    @IBOutlet weak var homeTeamImage: UIImageView!
    @IBOutlet weak var transparentView: EZYGradientView!
    @IBOutlet weak var stadiumImage: UIImageView!
    
    weak var pageViewController: FixturePVC!
    weak var fixtureOverviewVC: FixtureOverviewVC!
    weak var fixtureTableVC: FixtureTableVC!
    weak var headToHeadVC: HeadToHeadVC!
    weak var lineupVC: LineupVC!
    weak var statsVC: FixtureStatsVC!
    
    var viewControllers = [UIViewController]()
    
    var currentIndex = 0
    
    var fixtureOptions = ["Overview","Stats", "Table", "Head to Head", "Line up", "Buzz"]
    var currentOption = "Overview"
    var fixture: Fixture?
    var fixtureId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        configureNavBar()
        getFixtureData()
 
       
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
     
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = Colors.secondaryBackground
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func configureUiElements() {
        self.view.backgroundColor = Colors.primaryBackground
        collectionView.delegate = self
        collectionView.dataSource = self
        let homeTeamGesture = UITapGestureRecognizer(target: self, action: #selector(goToHomeTeamProfile))
        let awayTeamGesture = UITapGestureRecognizer(target: self, action: #selector(goToAwayTeamProfile))
        homeTeamImage.addGestureRecognizer(homeTeamGesture)
        awayTeamImage.addGestureRecognizer(awayTeamGesture)
        menuView.backgroundColor = Colors.primaryBackground
        collectionView.backgroundColor = Colors.primaryBackground
        menuView.applyShadow(cornerRadius: 0, shadowOpacity: 0.1, radius: 6, offset: CGSize(width: 0, height: 4))
        transparentView.firstColor = UIColor(hexString: "0F0F0F").withAlphaComponent(0.3)
        transparentView.secondColor = UIColor(hexString: "0F0F0F")
        transparentView.angleº = 0
        transparentView.colorRatio = 0.6
        transparentView.fadeIntensity = 1
    }
    
    private func configureNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white

    }
    
    private func setCellLayout() {
        let secondLayout = UICollectionViewFlowLayout()
        secondLayout.scrollDirection = .horizontal
        secondLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        secondLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 10)
        collectionView.collectionViewLayout = secondLayout
    }
    
    @objc private func goToHomeTeamProfile() {
        if let team = fixture?.getLocalTeam() {
            let teamProfileVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileVC") as! TeamProfileVC
            teamProfileVC.team = team
            navigationController?.pushViewController(teamProfileVC, animated: true)
        }
    }
    
    @objc private func goToAwayTeamProfile() {
        if let team = fixture?.getVisitorTeam() {
            let teamProfileVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileVC") as! TeamProfileVC
            teamProfileVC.team = team
            navigationController?.pushViewController(teamProfileVC, animated: true)
        }
    }
    
    private func getFixtureData() {
        if let fixtureId = fixtureId {
            FootballApi.getFixtureById(fixtureId: fixtureId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let fix):
                    self?.fixture = fix
                    DispatchQueue.main.async {
                        self?.configureUiElements()
                        self?.setCellLayout()
                        self?.displayFixtureData()
                        self?.configurePageViewController()
                    }

                }
            }
        } else {
            configureUiElements()
            setCellLayout()
            displayFixtureData()
            configurePageViewController()
        }
    }
    private func configurePageViewController() {
        pageViewController = storyboard?.instantiateViewController(withIdentifier: "FixturePVC") as! FixturePVC
        pageViewController.delegate = self
        pageViewController.dataSource = self
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageViewController.view)
        view.bringSubviewToFront(menuView)
        let topConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .top, relatedBy: .equal, toItem: self.menuView, attribute: .bottom, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        let rightConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        self.view.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
        
        fixtureOverviewVC = storyboard?.instantiateViewController(withIdentifier: "FixtureOverviewVC") as! FixtureOverviewVC
        fixtureTableVC = storyboard?.instantiateViewController(withIdentifier: "FixtureTableVC") as! FixtureTableVC
        headToHeadVC = storyboard?.instantiateViewController(withIdentifier: "HeadToHeadVC") as! HeadToHeadVC
        lineupVC = storyboard?.instantiateViewController(withIdentifier: "LineupVC") as! LineupVC
        statsVC = storyboard?.instantiateViewController(withIdentifier: "FixtureStatsVC") as! FixtureStatsVC

        
        if let fixture = self.fixture {
            fixtureOverviewVC.fixture = fixture
            fixtureTableVC.fixture = fixture
            headToHeadVC.fixture = fixture
            lineupVC.fixture = fixture
            statsVC.fixture = fixture
        }
        //["Overview","Stats", "Table", "Head to Head", "Line up", "Buzz"]
        viewControllers = [fixtureOverviewVC, statsVC, fixtureTableVC, headToHeadVC, lineupVC]
        pageViewController.setViewControllers([fixtureOverviewVC], direction: .forward, animated: true, completion: nil)
    }
    
    private func displayFixtureData() {
        if let fix = fixture {
            //11886280
            if let url = fix.getVenueImage(){
                stadiumImage.sd_setImage(with: url, placeholderImage: UIImage(named: ""))
            }
            if let homeTeam = fix.getLocalTeam()?.name, let homeTeamUrl = fix.getLocalTeam()?.getLogoUrl() {
                homeTeamName.text = homeTeam
                homeTeamImage.sd_setImage(with: homeTeamUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let awayTeam = fix.getVisitorTeam()?.name, let awayTeamUrl = fix.getVisitorTeam()?.getLogoUrl() {
                awayTeamName.text = awayTeam
                awayTeamImage.sd_setImage(with: awayTeamUrl, placeholderImage: Constants.defaultTeamImage)
            }
            
            if let status = fix.time?.status {
                if status != "LIVE" {
                    statusLabel.text = status
                } else {
                    if let min = fix.time?.minute, let second = fix.time?.second {
                        statusLabel.text = "\(min):\(second)"
                    }
                }
                
            }
            
            if !fix.hasPassed() || fix.isPostponed() {
                if let date = fix.getSchedule()?.getDateString(), let time = fix.getSchedule()?.getTimeString() {
                    fixtureDate.isHidden = false
                    if fix.isToday() {
                        fixtureDate.text = "Today\n\(time)"
                    } else {
                        fixtureDate.text = "\(date)\n\(time)"
                    }
                    
                }
            } else if fix.isPostponed(){
                 fixtureDate.isHidden = false
                 fixtureDate.text = "Postponed"
            } else {
                if let homeTeamScore = fix.getLocalScore(), let awayTeamScore = fix.getVisitorScore() {
                    scoreLabel.isHidden = false
                    scoreLabel.text = "\(homeTeamScore) - \(awayTeamScore)"
                }
            }

            

        }
    }
    
    private func jump(to: Int) {
         let toVC = viewControllers[to]
         var direction: UIPageViewController.NavigationDirection = .forward

        if currentIndex < to {
            direction = .forward
        } else {
            direction = .reverse
        }

        pageViewController.setViewControllers([toVC], direction: direction, animated: true) { _ in
            DispatchQueue.main.async {
                self.pageViewController.setViewControllers([toVC], direction: direction, animated: true, completion: nil)
            }
        }
    }
    
    
    

}

extension FixtureVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fixtureOptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FixtureOptionCell", for: indexPath) as! FixtureOptionCell
        cell.option = fixtureOptions[indexPath.row]
        if currentOption == cell.option {
            cell.isSelected = true
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        } else {
            cell.isSelected = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalFunctions.playSelectionFeedbackGenerator()
        currentOption = fixtureOptions[indexPath.row]
        jump(to: indexPath.row)
        currentIndex = indexPath.row
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
}

extension FixtureVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    
    //["Overview","Stats", "Table", "Head to Head", "Line up", "Buzz"]
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController is FixtureTableVC {
            statsVC = storyboard?.instantiateViewController(withIdentifier: "FixtureStatsVC") as! FixtureStatsVC
            statsVC.fixture = self.fixture
            return statsVC
        } else if viewController is HeadToHeadVC {
            fixtureTableVC = storyboard?.instantiateViewController(withIdentifier: "FixtureTableVC") as! FixtureTableVC
            fixtureTableVC.fixture = self.fixture
            return fixtureTableVC
        } else if viewController is LineupVC {
            headToHeadVC = storyboard?.instantiateViewController(withIdentifier: "HeadToHeadVC") as! HeadToHeadVC
            headToHeadVC.fixture = self.fixture
            return headToHeadVC
        } else if viewController is FixtureStatsVC {
             fixtureOverviewVC = storyboard?.instantiateViewController(withIdentifier: "FixtureOverviewVC") as! FixtureOverviewVC
            fixtureOverviewVC.fixture = self.fixture
            return fixtureOverviewVC
        } else {
            return nil
        }
       
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController is FixtureOverviewVC {
            statsVC = storyboard?.instantiateViewController(withIdentifier: "FixtureStatsVC") as! FixtureStatsVC
            statsVC.fixture = self.fixture
            return statsVC
        } else if viewController is FixtureTableVC {
            headToHeadVC = storyboard?.instantiateViewController(withIdentifier: "HeadToHeadVC") as! HeadToHeadVC
            headToHeadVC.fixture = self.fixture
            return headToHeadVC
        } else if viewController is HeadToHeadVC {
            let lineupVC = storyboard?.instantiateViewController(withIdentifier: "LineupVC") as! LineupVC
            lineupVC.fixture = self.fixture
            return lineupVC
        } else if viewController is FixtureStatsVC {
            fixtureTableVC = storyboard?.instantiateViewController(withIdentifier: "FixtureTableVC") as! FixtureTableVC
            fixtureTableVC.fixture = self.fixture
            return fixtureTableVC
        } else {
           return nil
        }
        
    }
}
