//
//  HeadToHeadVC.swift
//  Faneever
//
//  Created by James Meli on 4/4/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class HeadToHeadVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var fixtures = [Fixture]()
    var fixture: Fixture?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUiElements()
        getHeadToHead()
    }
    
    private func configureUiElements() {
        self.view.backgroundColor = Colors.primaryBackground
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = Colors.seperatorColor
    }
    
    private func getHeadToHead() {
        if let firstTeamId = fixture?.getLocalTeam()?.id, let secondTeamId = fixture?.getVisitorTeam()?.id {
            FootballApi.getHeadToHeadFixtures(firstTeamId: firstTeamId, secondTeamId: secondTeamId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let fixes):
                    self?.fixtures = fixes
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            
         }
       }
    }
    



}

extension HeadToHeadVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fixtures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeadToHeadCell", for: indexPath) as! HeadToHeadCell
        cell.fixture = fixtures[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}
