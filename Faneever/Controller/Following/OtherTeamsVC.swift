//
//  OtherTeamsVC.swift
//  Faneever
//
//  Created by James Meli on 3/25/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class OtherTeamsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataDict: [Int: TeamAndFixture] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

       configureUiElements()
       getFollowingTeamsFixtures()
    }
    
    private func configureUiElements() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func getFollowingTeamsFixtures() {
        FootballApi.getFollowingTeamsFixtures { [weak self] result in
            switch result {
            case .failure(let error):
                print("Error")
            case .success(let dataDict):
                self?.dataDict = dataDict
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }
    }
    



}

extension OtherTeamsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataDict.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !dataDict.keys.isEmpty {
            if let teamAndFixture = dataDict[section] {
                return teamAndFixture.fixtures.count + 1
            }
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FixtureTeamCell", for: indexPath) as! FixtureTeamCell
            cell.team = dataDict[indexPath.section]?.team
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FollowingTeamsFixtureCell", for: indexPath) as! FollowingTeamsFixtureCell
            
            cell.fixture = dataDict[indexPath.section]?.fixtures[indexPath.row - 1]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 60
        } else {
            return 90
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let teamProfileVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileVC") as! TeamProfileVC
            teamProfileVC.teamId = dataDict[indexPath.section]?.team.id
            navigationController?.pushViewController(teamProfileVC, animated: true)
        } else {
            let cell = tableView.cellForRow(at: indexPath) as! FollowingTeamsFixtureCell
            if let fixture = cell.fixture {
                let fixtureVC = storyboard?.instantiateViewController(withIdentifier: "FixtureVC") as! FixtureVC
                fixtureVC.fixture = fixture
                navigationController?.pushViewController(fixtureVC, animated: true)
            }
        }
    }
    

    
}
