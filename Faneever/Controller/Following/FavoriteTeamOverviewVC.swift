//
//  FavoriteTeamOverviewVC.swift
//  Faneever
//
//  Created by James Meli on 3/25/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

class FavoriteTeamOverviewVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var nextGameFixture: Fixture?
    var last5Fixtures = [Fixture]()
    var standings = [Standings]()
    var seasonLogoUrl: URL?
    var topPlayers = [TopPlayerCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUiColors()
        tableView.delegate = self
        tableView.dataSource = self
        getTeamNextFixture()
        getLeagueStandings()
        getTeamTopPlayers()

        
    }
    
    private func configureUiColors() {
    
    }
    
    private func getTeamNextFixture() {
        if let teamId = Constants.currentUser?.getFavoriteTeamId() {
            FootballApi.getTeamNextFixture(teamId: teamId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let fix):
                    self?.nextGameFixture = fix
                    self?.getLast5Fixtures()

                }
            }
        }
        
    }
    
    private func getLast5Fixtures() {
        if let teamId = Constants.currentUser?.getFavoriteTeamId() {
            FootballApi.getTeamLast5Fixtures(teamId: teamId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let fixtures):
                    self?.last5Fixtures = fixtures
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    private func getLeagueStandings() {
        if let seasonId = Constants.currentUser?.getFavoriteTeamSeasonId() {
            FootballApi.getLeagueStandings(seasonId: seasonId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let leagueStandings):
                    if let id = Constants.currentUser?.getFavoriteTeamId() {
                        if let index = leagueStandings.firstIndex(where: {$0.team_id == id}) {
                            if index == leagueStandings.count - 1 {
                                self?.standings = [leagueStandings[index - 2], leagueStandings[index - 1], leagueStandings[index]]
                            } else if index == 0 {
                                self?.standings = [leagueStandings[index], leagueStandings[index + 1], leagueStandings[index + 2]]
                            } else {
                                self?.standings = [leagueStandings[index - 1], leagueStandings[index], leagueStandings[index + 1]]
                            }
                            
                            FootballApi.getSeasonById(seasonId: seasonId) { [weak self] result2 in
                                switch result2 {
                                    case .failure(let error):
                                        print("Error")
                                    case .success(let season):
                                        self?.seasonLogoUrl = season.league?.data?.getLogoURL()
                                        DispatchQueue.main.async {
                                            self?.tableView.reloadData()
                                        }
                                }
                            }
                            

                        }
                    }
                }
            }
        }
    }
    
    private func getTeamTopPlayers() {
        if let seasonId = Constants.currentUser?.getFavoriteTeamSeasonId(), let teamId = Constants.currentUser?.getFavoriteTeamId() {
            FootballApi.getTeamTopPlayers(seasonId: seasonId, teamId: teamId) { [weak self] result in
                switch result {
                case .failure(let error):
                    print("Error")
                case .success(let teamPlayerStats):
                    let topGoalScorers = TopPlayerCategory(players: teamPlayerStats.topGoalScorers(), title: "Goal Scorers")
                    let topAssits = TopPlayerCategory(players: teamPlayerStats.topAssits(), title: "Assists")
                    let topPasses = TopPlayerCategory(players: teamPlayerStats.topPasses(), title: "Key Passes")
                    let topDribbles = TopPlayerCategory(players: teamPlayerStats.topDribbles(), title: "Dribbles")
                    let topInterceptions = TopPlayerCategory(players: teamPlayerStats.topInterceptions(), title: "Interceptions")
                    let topYellowCards = TopPlayerCategory(players: teamPlayerStats.topYellowCards(), title: "Yellow Cards")
                    let topRedCards = TopPlayerCategory(players: teamPlayerStats.topRedCards(), title: "Red Cards")
                    self?.topPlayers = [topGoalScorers, topAssits, topPasses, topDribbles, topInterceptions, topYellowCards, topRedCards]
                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
                    }
                }
            }
        }
    }
    


}

extension FavoriteTeamOverviewVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamOverviewCell", for: indexPath) as! TeamOverviewCell
            cell.delegate = self
            cell.nextGameFixture = nextGameFixture
            cell.last5Games = self.last5Fixtures
            return cell
        } else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueStandingsCell", for: indexPath) as! LeagueStandingsCell
            cell.delegate = self
            cell.leagueLogoURL = self.seasonLogoUrl
            cell.standings = standings
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TopPlayersCell", for: indexPath) as! TopPlayersCell
            cell.topPlayers = topPlayers
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 375
        } else if indexPath.section == 1 {
            return 305
        } else {
            return 335
        }
        
    }
    

}

extension FavoriteTeamOverviewVC: FollowingDelegate {
    
    func didSelectFixture(fixture: Fixture) {
        let fixtureVC = storyboard?.instantiateViewController(withIdentifier: "FixtureVC") as! FixtureVC
        fixtureVC.fixture = fixture
        navigationController?.pushViewController(fixtureVC, animated: true)
    }
    
    func didSelectLeague(league: League) {
        let leagueProfileVC = storyboard?.instantiateViewController(withIdentifier: "LeagueProfileVC") as! LeagueProfileVC
        leagueProfileVC.league = league
        navigationController?.pushViewController(leagueProfileVC, animated: true)
    }
    
    func didSelectTeam(teamId: Int) {
        let teamProfileVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileVC") as! TeamProfileVC
        teamProfileVC.teamId = teamId
        navigationController?.pushViewController(teamProfileVC, animated: true)
    }
}
