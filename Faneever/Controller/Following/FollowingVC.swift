//
//  TeamsVC.swift
//  Faneever
//
//  Created by James Meli on 3/1/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit
import IBAnimatable

class FollowingVC: UIViewController, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerView: AnimatableView!

    weak var pageViewController: FollowingPVC!
    weak var favoriteTeamOverviewVC: FavoriteTeamOverviewVC!
    weak var otherTeamFixturesVC: OtherTeamsVC!
    var viewControllers = [UIViewController]()
    var currentIndex = 0
    var options = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavBar()
        configureUiElements()
        setCellLayout()
        configurePageViewController()
        
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
   
    
    private func configureNavBar() {
        navigationController?.navigationBar.barTintColor = Colors.secondaryBackground
        self.navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = Colors.greenBrandColor
        navigationItem.title = "Following"
        navigationController?.navigationBar.titleTextAttributes = Constants.navBarTitleAttributes
        let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(presentSearchController))
        searchButton.tintColor = Colors.greenBrandColor
        navigationItem.rightBarButtonItem = searchButton
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        
       
    }
    
    private func configureUiElements() {
        if let teamName = Constants.currentUser?.getFavoriteTeamName() {
            options = [teamName, "Teams", "Leagues"]
        }
        
        collectionView.delegate = self
        collectionView.dataSource = self
      
    }
    
    private func setCellLayout() {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        collectionView.collectionViewLayout = collectionViewLayout
    }
    
    private func configurePageViewController() {
        pageViewController = storyboard?.instantiateViewController(withIdentifier: "FollowingPVC") as! FollowingPVC
        pageViewController.delegate = self
        pageViewController.dataSource = self
        addChild(pageViewController)
        pageViewController.didMove(toParent: self)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageViewController.view)
        view.bringSubviewToFront(headerView)
        let topConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .top, relatedBy: .equal, toItem: self.headerView, attribute: .bottom, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
        let rightConstraint = NSLayoutConstraint(item: pageViewController.view, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
        self.view.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
        
         favoriteTeamOverviewVC = storyboard?.instantiateViewController(withIdentifier: "FavoriteTeamOverviewVC") as! FavoriteTeamOverviewVC
         otherTeamFixturesVC = storyboard?.instantiateViewController(withIdentifier: "OtherTeamsVC") as! OtherTeamsVC
        
        viewControllers = [favoriteTeamOverviewVC, otherTeamFixturesVC]

        pageViewController.setViewControllers([favoriteTeamOverviewVC], direction: .forward, animated: true, completion: nil)
    }
    
    private func jump(to: Int) {
         let toVC = viewControllers[to]
         var direction: UIPageViewController.NavigationDirection = .forward
         
        if currentIndex < to {
            direction = .forward
        } else {
            direction = .reverse
        }
        
        pageViewController.setViewControllers([toVC], direction: direction, animated: true) { _ in
            DispatchQueue.main.async {
                self.pageViewController.setViewControllers([toVC], direction: direction, animated: true, completion: nil)
            }
        }
    }
    
    @objc private func presentSearchController() {
        GlobalFunctions.playSelectionFeedbackGenerator()
        let searchVC = storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        searchVC.delegate = self
        let navController = UINavigationController(rootViewController: searchVC)
        navController.modalPresentationStyle = .pageSheet
        present(navController, animated: true, completion: nil)
       
    }
    
    

}

extension FollowingVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FollowingOptionCell", for: indexPath) as! FollowingOptionCell
        cell.option = options[indexPath.row]
        if indexPath.row == 0 {
            cell.isSelected = true
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
        } else {
            cell.isSelected = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        GlobalFunctions.playSelectionFeedbackGenerator()
        jump(to: indexPath.row)
        currentIndex = indexPath.row

    }
    

}

extension FollowingVC: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if viewController is OtherTeamsVC {
          collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: [])
          return favoriteTeamOverviewVC
        } else {
          return nil
        }
        

        
        


    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if viewController is FavoriteTeamOverviewVC {
            collectionView.selectItem(at: IndexPath(item: 1, section: 0), animated: true, scrollPosition: [])
            return otherTeamFixturesVC
        } else {
            return nil
        }

 


    }

    
}

extension FollowingVC: SearchDelegate {
    
    func didTapOnSearchResult(player: Player?, team: Team?, league: League?) {
        if let player = player {
            
        } else if let team = team {
            let teamProfileVC = storyboard?.instantiateViewController(withIdentifier: "TeamProfileVC") as! TeamProfileVC
            teamProfileVC.teamId = team.id
            navigationController?.pushViewController(teamProfileVC, animated: true)
        } else if let league = league {
            let leagueProfileVC = storyboard?.instantiateViewController(withIdentifier: "LeagueProfileVC") as! LeagueProfileVC
            leagueProfileVC.league = league
            navigationController?.pushViewController(leagueProfileVC, animated: true)
        }
    }
}



