//
//  FixtureStats.swift
//  Faneever
//
//  Created by James Meli on 4/6/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct FixtureStatResponse: Decodable {
    var data: [FixtureStats]?
}
struct FixtureStats: Decodable {
    var team_id: Int?
    var fixture_id: Int?
    var shots: FixtureShots?
    var passes: FixturePasses?
    var attacks: FixtureAttacks?
    var fouls: Int?
    var corners: Int?
    var offsides: Int?
    var possessiontime: Int?
    var yellowcards: Int?
    var redcards: Int?
    var yellowredcards: Int?
    var saves: Int?
    var substitutions: Int?
    var goal_kick: Int?
    var goal_attempts: Int?
    var free_kick: Int?
    var throw_in: Int?
    var ball_safe: Int?
    var goals: Int?
    var penalties: Int?
    var injuries: Int?
    
    func getPassPercentage() -> Int? {
        if let percent = self.passes?.percentage {
            return Int(percent)
        }
        
        return nil
    }
    
}

struct FixtureShots: Decodable {
    var total: Int?
    var ongoal: Int?
    var offgoal: Int?
    var insidebox: Int?
    var outsidebox: Int?
    
}

struct FixturePasses: Decodable {
    var total: Int?
    var accurate: Int?
    var percentage: Double?
}

struct FixtureAttacks: Decodable {
    var attacks: Int?
    var dangerous_attacks: Int?
}
