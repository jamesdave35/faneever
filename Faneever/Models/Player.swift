//
//  Player.swift
//  Faneever
//
//  Created by James Meli on 3/5/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct PlayersResponse: Decodable {
    var data: [Player]?
}

struct PlayerResponse: Decodable {
    var data: Player?
}

struct SquadResponse: Decodable {
    var data: [PlayerStatNumbers]?
}


struct Player: Decodable {
    
    var display_name: String?
    var firstname: String?
    var lastname: String?
    var player_id: Int? = 0
    var age: Int?
    var birthdate: String?
    var height: String?
    var weight: String?
    var nationality: String?
    var image_path: String?
    var team_id: Int?
    var country: CountryResponse?
    var team: TeamResponse?
    var position: PositionResponse?
   
    
    init(firebaseData: NSDictionary) {
        self.display_name = firebaseData["playerName"] as? String
        self.firstname = firebaseData["firstName"] as? String
        self.lastname = firebaseData["lastName"] as? String
        self.image_path = firebaseData["imageURL"] as? String
        if let playerId = firebaseData["id"] as? Int {
            self.player_id = playerId
        }
        self.team_id = firebaseData["teamId"] as? Int
        self.height = firebaseData["height"] as? String
        self.weight = firebaseData["weight"] as? String
        self.age = firebaseData["age"] as? Int
    }
    
    func getCommonName() -> String? {
        if let name = self.display_name {
            let names = name.components(separatedBy: " ")
            if names.count == 1 {
                return names[0]
            } else if names.count == 2 {
                return names[1]
            } else if names.count == 3   {
                return names[2]
            }
        }
        
        return nil
    }
    
    func getPlayerAge() -> Int? {
        if let dateOfBirth = self.birthdate {
            let convertedString = dateOfBirth.adjustDateOfBirth()
            if let birthday = convertedString.convertToDate(format: "MM/dd/yyy") {
                return GlobalFunctions.getAgeFromBirthDate(birthday: birthday)
            }
        }
        
        return nil
    }
    
    
    func getImageURL() -> URL? {
        if let urlString = self.image_path {
            return URL(string: urlString)
        } else {
            return nil
        }
    }
    
    
    func getTeam(completion: @escaping(Result<Team?, RequestError>) -> Void) {
        if let teamId = self.team_id {
            FootballApi.getTeamById(id: teamId) { result in
                completion(result)
            }
        }
    }
    
    func getPositionName() -> String? {
        return position?.data?.name
    }
    
    func getTeamName() -> String? {
        return team?.data?.name
    }
    
    func getTeamLogoUrl() -> URL? {
        return team?.data?.getLogoUrl()
    }
    
    func getCountryName() -> String? {
        return country?.data?.name
    }
    
    func getCountryFlagUrl() -> URL? {
        return country?.data?.getLogoUrl()
    }
    

    
    func toDictionary() -> [String: Any] {
        return ["playerName": self.display_name, "firstName": self.firstname, "lastName": self.lastname, "age": self.getPlayerAge(), "height": self.height, "weight": self.weight, "imageURL": self.image_path, "id": self.player_id, "teamId": self.team_id]
    }
}




