//
//  Standings.swift
//  Faneever
//
//  Created by James Meli on 3/21/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct LeagueSeasonStandingsResponse: Decodable {
    var data: [LeagueSeasonStandings]?
}

struct LeagueSeasonStandings: Decodable {
    var standings: StandingsResponse?
}

struct StandingsResponse: Decodable {
    var data: [Standings]?
}

struct Standings: Decodable {
    var position: Int?
//    var played: Int?
    var team_id: Int?
    var team_name: String?
//    var short_code: String?
//    var team_logo: String?
//    var goals: String?
//    var goal_diff: Int?
//    var wins: Int?
//    var lost: Int?
//    var draws: Int?
//    var points: Int?
//    var description: String?
//    var recent_form: String?
//    var fairplay_points_lose: Int?
    var overall: OverallGame?
    var total: Total?
    var result: String?
    var team: TeamResponse?
    
    func isChampionsLeagueQualified() -> Bool {
        if let desc = self.result {
            if desc == "UEFA Champions League" {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func isEuropaLeagueQualified() -> Bool {
        if let desc = self.result {
            if desc == "UEFA Europa League" {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func isInRelegationZone() -> Bool {
        if let desc = self.result {
            if desc == "Relegation" {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    
}

struct OverallGame: Decodable {
    var games_played: Int?
    var won: Int?
    var draw: Int?
    var lost: Int?
}

struct Total: Decodable {
    var points: Int?
}
