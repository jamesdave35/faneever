//
//  TopScorers.swift
//  Faneever
//
//  Created by James Meli on 5/12/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct TopScorersResponse: Decodable {
    var data: TopScorers?
}

struct TopScorers: Decodable {
    var id: Int?
    var name: String?
    var aggregatedGoalscorers: AggregatedGoalScorerResponse?
    var aggregatedAssistscorers: AggregatedAssistScocersResponse?
    var aggregatedCardscorers: AggregatedCardScorerResponse?
    
    
    
}

struct AggregatedGoalScorerResponse: Decodable {
    var data: [AggregatedGoalScorers]?
}

struct AggregatedGoalScorers: Decodable {
    var goals: Int?
    var player: PlayerResponse?
    var team: TeamResponse?
    var position: Int?
    var type: String?
    
    func getPlayerName() -> String {
        if let name = player?.data?.display_name {
            return name
        }
        
        return ""
    }
    
    func getPlayerUrl() -> URL? {
        return player?.data?.getImageURL()
    }
    
    func getTeamName() -> String {
        if let name = team?.data?.name {
            return name
        }
        
        return ""
    }
    
    func getTeamURL() -> URL? {
        return team?.data?.getLogoUrl()
    }
    
}

struct AggregatedAssistScocersResponse: Decodable {
    var data: [AggregatedAssitScorers]?
}

struct AggregatedAssitScorers: Decodable {
    var assists: Int?
    var player: PlayerResponse?
    var team: TeamResponse?
    var position: Int?
    var type: String?
    
    func getPlayerName() -> String {
        if let name = player?.data?.display_name {
            return name
        }
        
        return ""
    }
    
    func getPlayerUrl() -> URL? {
        return player?.data?.getImageURL()
    }
    
    func getTeamName() -> String {
        if let name = team?.data?.name {
            return name
        }
        
        return ""
    }
    
    func getTeamURL() -> URL? {
        return team?.data?.getLogoUrl()
    }
    
}

struct AggregatedCardScorerResponse: Decodable {
    var data: [AggregatedCardScorers]?
}

struct AggregatedCardScorers: Decodable {
    var yellowcards: Int?
    var redcards: Int?
    var player: PlayerResponse?
    var team: TeamResponse?
    var position: Int?
    var type: String?
    
    func getPlayerName() -> String {
        if let name = player?.data?.display_name {
            return name
        }
        
        return ""
    }
    
    func getPlayerUrl() -> URL? {
        return player?.data?.getImageURL()
    }
    
    func getTeamName() -> String {
        if let name = team?.data?.name {
            return name
        }
        
        return ""
    }
    
    func getTeamURL() -> URL? {
        return team?.data?.getLogoUrl()
    }
}

struct TopPlayerDisplay {
    var value: Int?
    var team: Team?
    var player: Player?
    var position: Int?
}
