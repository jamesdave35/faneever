//
//  FixtureEvent.swift
//  Faneever
//
//  Created by James Meli on 4/4/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct FixtureEventResponse: Decodable {
    var data: [FixtureEvent]?
}

struct FixtureEvent: Decodable {
    var id: Int?
    var team_id: String?
    var type: String?
    var fixture_id: Int?
    var player_id: Int?
    var player_name: String?
    var related_player_id: Int?
    var related_player_name: String?
    var minute: Int?
    var extra_minute: Int?
    var reason: String?
   // var injuried:
    var result: String?
    
    func isHomeTeamEvent(fixture: Fixture) -> Bool {
        if let homeTeamId = fixture.getLocalTeam()?.id, let eventTeamId = self.team_id {
            if String(homeTeamId) == eventTeamId {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func eventType() -> String {
        if let type = self.type {
            if type == "yellowcard" || type == "redcard" || type == "yellowred" {
                return "card"
            } else if type == "substitution" {
                return type
            } else if type == "penalty" || type == "goal" {
                return "goal"
            }
        }
        
        return "undefined"
    }
}

struct EventFix {
    var fixture: FixtureEvent
    var isHomeTeamEvent: Bool
}
