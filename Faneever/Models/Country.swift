//
//  Country.swift
//  Faneever
//
//  Created by James Meli on 3/10/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct CountryResponse: Decodable {
    
    var data: Country?
}

struct Country: Decodable {
    
    var name: String?
    var id: Int?
    var image_path: String?
    
    func getLogoUrl() -> URL? {
        if let urlString = self.image_path {
            return URL(string: urlString)
        }
        
        return nil
    }
    
    
}
