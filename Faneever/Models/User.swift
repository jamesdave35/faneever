//
//  User.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import UIKit

  struct User {
    
    var displayName: String
    var username: String
    var emailAddress: String
    var profileImageUrl: String
    var favoriteTeam: NSDictionary
    var followingTeams: [Int]
    var followingLeagues: [Int]
    var uid: String
    
    init(displayName: String, username: String, emailAddress: String, profileImageUrl: String, favoriteTeam: NSDictionary, followingTeams: [Int], followingLeagues: [Int], uid: String) {
        
        self.displayName = displayName
        self.username = username
        self.emailAddress = emailAddress
        self.profileImageUrl = profileImageUrl
        self.favoriteTeam = favoriteTeam
        self.followingTeams = followingTeams
        self.followingLeagues = followingLeagues
        self.uid = uid
        
    }
    
    init(data: NSDictionary) {
        
        self.displayName = data["displayName"] as! String
        self.username = data["username"] as! String
        self.emailAddress = data["emailAddress"] as! String
        self.profileImageUrl = data["profileImageUrl"] as! String
        self.favoriteTeam = data["favoriteTeam"] as! NSDictionary
        self.followingTeams = data["followingTeams"] as! [Int]
        self.followingLeagues = data["followingLeagues"] as! [Int]
        self.uid = data["uid"] as! String
    }
    
    func getProfileImageURL() -> URL? {
        return URL(string: self.profileImageUrl)
    }
    
    func getFavoriteTeamName() -> String? {
        return self.favoriteTeam["name"] as? String
    }
    
    func getFavoriteTeamId() -> Int? {
        return self.favoriteTeam["id"] as? Int
    }
    
    func getFavoriteTeamSeasonId() -> Int? {
        return self.favoriteTeam["currentSeasonId"] as? Int
    }
    
    func toDictionary() -> [String: Any] {
        return ["displayName": self.displayName, "username": self.username, "emailAddress": self.emailAddress, "profileImageUrl": self.profileImageUrl, "favoriteTeam": self.favoriteTeam, "followingTeams": self.followingTeams, "followingLeagues": self.followingLeagues, "uid": self.uid]
    }
    
}
