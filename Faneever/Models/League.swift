//
//  League.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct LeagueResponses: Decodable {
    var data: [League]?
}

struct LeagueResponse: Decodable {
    var data: League?
}

struct League: Decodable {
    
    var id: Int?
    var name: String?
    var type: String?
    var country_id: Int?
    var current_season_id: Int?
    var logo_path: String?
    var is_cup: Bool?
    var country: CountryResponse?
    
    
    func getLogoURL() -> URL? {
        if let urlString = self.logo_path {
            return URL(string: urlString)
        }
        
        return nil
    }
}
