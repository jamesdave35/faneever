//
//  Team.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct TeamsResponse: Decodable {
    var data: [Team]?
}

struct TeamResponse: Decodable {
    var data: Team?
}


struct Team: Decodable {
    
    var id: Int?
    var name: String?
    var short_code: String?
    var logo_path: String?
    var current_season_id: Int?
    var country_id: Int?
    
    init(firebaseData: NSDictionary) {
        self.id = firebaseData["id"] as? Int
        self.name = firebaseData["name"] as? String
        self.logo_path = firebaseData["teamLogo"] as? String
        self.current_season_id = firebaseData["currentSeasonId"] as? Int
        self.country_id = firebaseData["countryId"] as? Int
    }
    
    func getLogoUrl() -> URL? {
        if let urlString = self.logo_path {
            return URL(string: urlString)
        }
        
        return nil
    }
    
    func getCountry(completion: @escaping(Swift.Result<Country, RequestError>) -> Void) {
        if let countryId = self.country_id {
            FootballApi.getCountryFromId(id: countryId) { result in
                completion(result)
            }
           
        }
    }
    
    func getTeamForm(completion: @escaping(_ form: String) -> Void) {
        var teamForm = ""
        if let id = self.id {
            FootballApi.getTeamLast5Fixtures(teamId: id) { (result) in
                switch result {
                case .failure(let error):
                    print("Error")
                    completion(teamForm)
                case .success(let fixtures):
                    for fix in fixtures {
                        switch fix.teamMatchResult(teamId: id) {
                        case "won":
                            teamForm = teamForm + "W"
                        case "lost":
                            teamForm = teamForm + "L"
                        case "draw":
                            teamForm = teamForm + "D"
                        default:
                            break
                        }
                        
                    }
                    
                    completion(teamForm)
                }
            }
        }
    }
    
    func toDictionary() -> [String: Any] {
        return ["id": self.id, "name": self.name, "teamLogo": self.logo_path, "currentSeasonId": self.current_season_id, "countryId": self.country_id]
    }
}
