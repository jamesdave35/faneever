//
//  Fixture.swift
//  Faneever
//
//  Created by James Meli on 3/20/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct FixturesResponse: Decodable {
    var data: [Fixture]?
}

struct FixtureResponse: Decodable {
    var data: Fixture?
}

struct Fixture: Decodable {
    
    var id: Int?
    var league_id: Int?
    var season_id: Int?
    var stage_id: Int?
    var round_id: Int?
    var formations: Formation?
    var time: FixtureStatusResponse?
    var scores: FixtureScores?
    var localTeam: TeamResponse?
    var visitorTeam: TeamResponse?
    var venue_id: Int?
    var referee_id: Int?
    var localCoach: CoachResponse?
    var visitorCoach: CoachResponse?
    var referee: RefereeResponse?
    var round: RoundResponse?
    var stage: StageResponse?
    var league: LeagueResponse?
    var venue: VenueResponse?
    var events: FixtureEventResponse?
    var lineup: LineupResponse?
    var bench: LineupResponse?
    var stats: FixtureStatResponse?
    
    func isToday() -> Bool {
        if let date = self.getSchedule()?.getDate() {
            let calendar = Calendar.current
            if calendar.isDateInToday(date){
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func hasStarted() -> Bool {
        if let date = self.getSchedule()?.getDate() {
            if !self.isPostponed() && date < Date() {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func getHomeCoachName() -> String? {
        return self.localCoach?.data?.fullname
    }
    
    func getAwayCoachName() -> String? {
        return self.visitorCoach?.data?.fullname
    }
    
    func getHomeCoachImage() -> URL? {
        if let urlString = self.localCoach?.data?.image_path {
            return URL(string: urlString)
        }
        
        return nil
    }
    
    func getAwayCoachImage() -> URL? {
        if let urlString = self.visitorCoach?.data?.image_path {
            return URL(string: urlString)
        }
        
        return nil
    }
    
    func getRefereeName() -> String? {
        return self.referee?.data?.common_name
    }
    
    func getLeagueName() -> String? {
        return self.league?.data?.name
    }
    
    func getLeagueLogo() -> URL? {
        return self.league?.data?.getLogoURL()
    }
    
    func getRoundName() -> Int? {
        return self.round?.data?.name
        
    }
    
    func getStageName() -> String? {
        return self.stage?.data?.name
    }
    
    func getVenueName() -> String? {
        return self.venue?.data?.name
    }
    
    func getVenueImage() -> URL? {
        return self.venue?.data?.getImageUrl()
    }
   
    func getSchedule() -> FixtureDateResponse? {
        return self.time?.starting_at
    }
    
    
    func getLocalTeam() -> Team? {
        return self.localTeam?.data
    }
    
    func getVisitorTeam() -> Team? {
        return self.visitorTeam?.data
    }
    
    func getLocalScore() -> Int? {
        return self.scores?.localteam_score
    }
    
    func getVisitorScore() -> Int? {
        return self.scores?.visitorteam_score
    }
    
    func isTeamHomeTeam(teamId: Int) -> Bool {
        if self.getLocalTeam()?.id == teamId {
            return true
        } else {
            return false
        }
    }
    
    func getScoreAsString() -> String {
        if let localScore = self.getLocalScore(), let visitorScore = self.getVisitorScore() {
            return "\(localScore) - \(visitorScore)"
        }
        
        return "-"
    }
    
    func isPostponed() -> Bool {
        if let status = self.time?.status {
            if status == "POSTP" {
                return true
            }
        }
        
        return false
    }
    
    func hasPassed() -> Bool {
        let now = Date()
        if let gameDate = self.time?.starting_at?.getDate() {
            if now > gameDate {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func teamMatchResult(teamId: Int) -> String {
        var teamScore = 0
        var secondScore = 0
        if let localScore = self.getLocalScore(), let visitorScore = self.getVisitorScore() {
            if self.getLocalTeam()?.id == teamId {
               teamScore = localScore
               secondScore = visitorScore
            } else {
                teamScore = visitorScore
                secondScore = localScore
            }
            
        }
        
        if teamScore > secondScore {
            return "won"
        } else if teamScore < secondScore {
            return "lost"
        } else {
            return "draw"
        }
        
        return "undefined"
        
    }
    
    func getHomeTeamPlayersLineup() -> [LineupPlayer]? {
        if let players = lineup?.data, let teamId = self.getLocalTeam()?.id {
            let homePlayers = players.filter({$0.team_id == teamId})
            return homePlayers.sorted(by: {$0.formationPosition() < $1.formationPosition()})
        }
        
        return nil
    }

    func getAwayTeamPlayersLineup() -> [LineupPlayer]? {
        if let players = lineup?.data, let teamId = self.getVisitorTeam()?.id {
            let awayPlayers = players.filter({$0.team_id == teamId})
            return awayPlayers.sorted(by: {$0.formationPosition() > $1.formationPosition()})
        }
        
        return nil
    }
    
    func getHomeBenchPlayers() -> [LineupPlayer]? {
        if let players = bench?.data, let teamId = self.getLocalTeam()?.id {
            return players.filter({$0.team_id == teamId})
        }
        
        return nil
    }
    
    func getAwayBenchPlayers() -> [LineupPlayer]? {
        if let players = bench?.data, let teamId = self.getVisitorTeam()?.id {
            return players.filter({$0.team_id == teamId})
        }
        
        return nil
    }
    
    func getHomeTeamStats() -> FixtureStats? {
        if let stats = stats?.data {
            if stats.count > 1 {
                return stats[0]
            }
        }
        
        return nil
    }
    
    func getAwayTeamStats() -> FixtureStats? {
        if let stats = stats?.data {
            if stats.count > 1 {
                return stats[1]
            }
        }
        
        return nil
    }
    
    
}



struct FixtureStatusResponse: Decodable {
    var status: String?
    var starting_at: FixtureDateResponse?
    var minute: Int?
    var second: String?
    
}

struct FixtureDateResponse: Decodable {
    var date: String?
    var time: String?
    var date_time: String?
    var timezone: String?
    
    func getDate() -> Date? {
        if let date = self.date_time {
            return date.convertToDate(format: "yyyy-MM-dd HH:mm:ss")
        }
        return nil
    }
    
    func getDateString() -> String? {
        if let date = self.getDate() {
            return date.convertToString(withFormat: "MMM, d")
        }
        
        return nil
    }
    
    func getSecondDateString() -> String? {
        if let date = self.getDate() {
            return date.convertToString(withFormat: "MMM d, yyyy")
        }
        
        return nil
    }
    
    func getTimeString() -> String? {
        if let date = self.getDate() {
            return date.convertToString(withFormat: "h:mm a")
        }
        
        return nil
    }
    

}

struct FixtureScores: Decodable {
    var localteam_score: Int?
    var visitorteam_score: Int?
    
}

struct FixtureCoaches: Decodable {
    var localteam_coach_id: Int?
    var visitorteam_coach_id: Int?
}

struct TeamAndFixture {
    var team: Team
    var fixtures: ArraySlice<Fixture>
}

struct RefereeResponse: Decodable {
    var data: Referee?
}

struct Referee: Decodable {
    var id: Int?
    var common_name: String?
}

struct RoundResponse: Decodable {
    var data: Round?
}

struct Round: Decodable {
    var id: Int?
    var name: Int?
}

struct StageResponse: Decodable {
    var data: Stage?
}

struct Stage: Decodable {
    var id: Int?
    var name: String?
}

struct Formation: Decodable {
    var localteam_formation: String?
    var visitorteam_formation: String?
}



