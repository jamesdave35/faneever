//
//  Post.swift
//  Faneever
//
//  Created by James Meli on 3/2/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation
import Firebase

class Post {
    
    var message: String
    var type: String
    var imageUrl: String?
    var videoUrl: String?
    var postId: String
    var userId: String
    var timeStamp: Date
    var isReplyTo: String?
    var playerComparisonDict: NSDictionary?
    var teamComparisonDict: NSDictionary?
    var likes: [String]?
    var userName: String?
    var userTeamName: String?
    var userProfileImageUrl: String?
    var numberOfLikes = 0
    var numberOfReplies = 0
    var hasLiked = false
    //var ref: DocumentReference?
    
    init(message: String, type: String, imageUrl: String?, videoUrl: String?, postId: String, userId: String, timeStamp: Date, isReply: String?, likes: [String]?, playerCompDict: NSDictionary?, teamCompDict: NSDictionary?) {
        self.message = message
        self.type = type
        self.imageUrl = imageUrl
        self.videoUrl = videoUrl
        self.postId = postId
        self.userId = userId
        self.timeStamp = timeStamp
        self.isReplyTo = isReply
        self.playerComparisonDict = playerCompDict
        self.teamComparisonDict = teamCompDict
        self.likes = likes
    }
    
    init(data: NSDictionary) {
        
        self.message = data["message"] as! String
        self.type = data["type"] as! String
        self.imageUrl = data["imageUrl"] as? String
        self.videoUrl = data["videoUrl"] as? String
        self.postId = data["postId"] as! String
        self.userId = data["userId"] as! String
        self.isReplyTo = data["isReplyTo"] as? String
        self.playerComparisonDict = data["playerCompDict"] as? NSDictionary
        self.teamComparisonDict = data["teamCompDict"] as? NSDictionary
        self.likes = data["likes"] as? [String]
        if let stamp = data["timeStamp"] as? Timestamp {
            self.timeStamp = stamp.dateValue()
        } else {
            self.timeStamp = Date()
        }
        

    }
    
    func setPostData(completion: @escaping(_ success: Bool) -> Void) {
       DatabaseServices.getUserWithId(id: self.userId) { (user) in
           if let user = user {
               self.userName = user.displayName
               if let teamName = user.getFavoriteTeamName() {
                 self.userTeamName = teamName
               }
               
               self.userProfileImageUrl = user.profileImageUrl
               if let likes = self.likes {
                   self.numberOfLikes = likes.count
               }
            
            completion(true)
               
           }
       }
    }
    
    func getUserImageURL() -> URL? {
        if let urlString = self.userProfileImageUrl {
            return URL(string: urlString)
        } else {
            return nil
        }
    }
    
    func isTextPost() -> Bool {
        if self.imageUrl == nil && self.videoUrl == nil && !self.isPlayerComparison() && !self.isTeamComparison() {
            return true
        } else {
            return false
        }
    }
    
    func hasImage() -> Bool {
        if self.imageUrl == nil {
            return false
        } else {
            return true
        }
    }
    
    func hasVideo() -> Bool {
        if self.videoUrl == nil {
            return false
        } else {
            return true
        }
    }
    
    func getImageURL() -> URL? {
        if let urlString = self.imageUrl {
            if let url = URL(string: urlString) {
                return url
            }
        }
        
        return nil

    }
    
    func getVideoURL() -> URL? {
        if let urlString = self.videoUrl {
            if let url = URL(string: urlString) {
                return url
            }
        }
        
        return nil
    }
    
    func isPlayerComparison() -> Bool {
        if self.type == "Player Comparison" {
            return true
        } else {
            return false
        }
    }
    
    func isTeamComparison() -> Bool {
        if self.type == "Team Comparison" {
            return true
        } else {
            return false
        }
    }
    
    func toDictionary() -> [String: Any] {
        return ["message": self.message, "type": self.type, "imageUrl": self.imageUrl, "videoUrl": self.videoUrl, "isReplyTo": self.isReplyTo, "likes": self.likes, "playerCompDict": self.playerComparisonDict, "teamCompDict": self.teamComparisonDict, "postId": self.postId, "userId": self.userId, "timeStamp": self.timeStamp]
    }
}
