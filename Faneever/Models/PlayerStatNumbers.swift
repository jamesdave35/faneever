//
//  StatNumbers.swift
//  Faneever
//
//  Created by James Meli on 3/7/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct PlayerStatResponse: Decodable {
    
    var data: StatResponse?
}

struct StatResponse: Decodable {
    var stats: TeamPlayersStats?
}

struct TeamPlayersStats: Decodable {
    var data: [PlayerStatNumbers]?
    
    func topGoalScorers() -> [PlayerStatNumbers] {
        if let players = self.data {
            return players.sorted(by: {$0.getNumOfGoals() > $1.getNumOfGoals()})
        }
        
        return []
    }
    
    func topAssits() -> [PlayerStatNumbers] {
         if let players = self.data {
            return players.sorted(by: {$0.getNumOfAssists() > $1.getNumOfAssists()})
         }
         
         return []
     }
    
    func topPasses() -> [PlayerStatNumbers] {
         if let players = self.data {
            return players.sorted(by: {$0.getNumOfKeyPasses() > $1.getNumOfKeyPasses()})
         }
         
         return []
     }
    
     func topDribbles() -> [PlayerStatNumbers] {
          if let players = self.data {
             return players.sorted(by: {$0.getNumOfDribbles() > $1.getNumOfDribbles()})
          }
          
          return []
      }
    
      func topInterceptions() -> [PlayerStatNumbers] {
           if let players = self.data {
              return players.sorted(by: {$0.getNumOfInterceptions() > $1.getNumOfInterceptions()})
           }
           
           return []
       }
    
       func topYellowCards() -> [PlayerStatNumbers] {
            if let players = self.data {
               return players.sorted(by: {$0.getNumOfYellowCards() > $1.getNumOfYellowCards()})
            }
            
            return []
        }
    
        func topRedCards() -> [PlayerStatNumbers] {
             if let players = self.data {
                return players.sorted(by: {$0.getNumOfRedCards() > $1.getNumOfRedCards()})
             }
             
             return []
         }
}

class PlayerStatNumbers: NSObject, Decodable {
    
    var appearences: Int? = 0
    var lineups: Int? = 0
    var minutes: Int? = 0
    var rating: String? = ""
    var captain: Int? = 0
    var substitute_in: Int? = 0
    var substitute_out: Int? = 0
    var substitutes_on_bench: Int? = 0
    var goals: Int? = 0
    var concededGoals: Int? = 0
    var assists: Int? = 0
    var tackles: Int? = 0
    var blocks: Int? = 0
    var fouls: Fouls?
    var crosses: Crosses?
    var dribbles: Dribbles?
    var duels: Duels?
    var passes: Passes?
    var penalties: Penalties?
    var dispossesed: Int? = 0
    var interceptions: Int? = 0
    var yellowcards: Int? = 0
    var redcards: Int? = 0
    var minsPerGoal: Int? = 0
    var saves: Int? = 0
    var inside_box_saves: Int? = 0
    var player: PlayerResponse?
    var position: PositionResponse?
    var league: LeagueResponse?
    var season: SeasonResponse?
    
    
    func getPlayer() -> Player? {
        return player?.data
    }
    
    func getMinsPerGoal() -> Int? {
        if let mins = self.minutes, let goals = self.goals {
            if goals > 0 {
                return mins / goals
            }
            
        }
        
        return nil
        
    }
    
    func getNumOfGoals() -> Int {
        if let goals = self.goals {
            return goals
        }
        return 0
    }
    
    func getNumOfAssists() -> Int {
        if let assists = self.assists {
            return assists
        }
        return 0
    }
    
    func getNumOfDribbles() -> Int {
        if let dribbles = self.dribbles?.success {
            return dribbles
        }
        return 0
    }
    
    func getNumOfKeyPasses() -> Int {
        if let passes = self.passes?.key_passes {
            return passes
        }
        return 0
    }
    
    func getNumOfInterceptions() -> Int {
        if let interceptions = self.interceptions {
            return interceptions
        }
        return 0
    }
    
    func getNumOfYellowCards() -> Int {
        if let cards = self.yellowcards {
            return cards
        }
        return 0
    }
    
    func getNumOfRedCards() -> Int {
        if let cards = self.redcards {
            return cards
        }
        return 0
    }
    
    func isGoalkeeper() -> Bool {
        if let pos = position?.data?.name {
            if pos == "Goalkeeper" {
                return true
            }
        }
        
        return false
    }
    
    func isMidfielder() -> Bool {
        if let pos = position?.data?.name {
            if pos == "Midfielder" {
                return true
            }
        }
        
        return false
    }
    
    func isDefender() -> Bool {
        if let pos = position?.data?.name {
            if pos == "Defender" {
                return true
            }
        }
        
        return false
    }
    
    func isAttacker() -> Bool {
        if let pos = position?.data?.name {
            if pos == "Attacker" {
                return true
            }
        }
        
        return false
    }
    

    
    
    
}

struct Fouls: Decodable {
    var committed: Int?
    var drawn: Int?
}

struct Crosses: Decodable {
    var total: Int?
    var accurate: Int?
}

struct Dribbles: Decodable {
    var attempts: Int?
    var success: Int?
    var dribbled_past: Int?
}

struct Duels: Decodable {
    var won: Int?
    var total: Int?
}

struct Passes: Decodable {
    var total: Int?
    var accuracy: Int?
    var key_passes: Int?
}

struct Penalties: Decodable {
    var won: Int?
    var scores: Int?
    var missed: Int?
    var committed: Int?
    var saves: Int?
}

struct StatComparison {
    
    var firstValue: Int? = 0
    var secondValue: Int?
    var statName: String = ""
    
    init(firstValue: Int?, secondValue: Int?, name: String) {
        self.firstValue = firstValue
        self.secondValue = secondValue
        self.statName = name
    }
    
    init(data: NSDictionary) {
        self.statName = data["statName"] as! String
        self.firstValue = data["firstValue"] as? Int
        self.secondValue = data["secondValue"] as? Int
    }
    
    func toDictionary() -> [String: Any] {
        return ["statName": self.statName, "firstValue": self.firstValue, "secondValue": self.secondValue]
    }
}

struct PositionResponse: Decodable {
    var data: Position?
}

struct Position: Decodable {
    var id: Int?
    var name: String?
}

struct TopPlayerCategory {
    var players: [PlayerStatNumbers]
    var title: String
}
