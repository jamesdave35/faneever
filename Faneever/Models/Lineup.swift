//
//  Lineup.swift
//  Faneever
//
//  Created by James Meli on 4/5/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct LineupResponse: Decodable {
    var data: [LineupPlayer]?
}

struct LineupPlayer: Decodable {
    
    var team_id: Int?
    var fixture_id: Int?
    var player_id: Int?
    var player_name: String?
    var number: Int?
    var position: String?
    var additional_position: Int?
    var formation_position: Int?
    var posx: Int?
    var posy: Int?
    var captain: Bool?
    var type: String?
    var stats: LineupPlayerStats?
    var player: PlayerResponse?
    
    func formationPosition() -> Int {
        if let pos = self.formation_position {
            return pos
        }
        
        return 0
    }
    
    func isCaptain() -> Bool {
        if let captain = self.captain {
            if captain {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func hasScored() -> Bool {
        if let goals = stats?.goals?.scored {
            if goals > 0 {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func hasAssited() -> Bool {
        if let goals = stats?.goals?.assists {
            if goals > 0 {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func numberOfGoals() -> Int {
        if let goals = stats?.goals?.scored {
           return goals
        }
        
        return 0
    }
    
    func numberOfAssits() -> Int {
        if let assists = stats?.goals?.assists {
           return assists
        }
        
        return 0
    }
    
    func hasYellowcard() -> Bool {
        if let card = stats?.cards?.yellowcards {
            if card > 0 {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func hasRedCard() -> Bool {
        if let card = stats?.cards?.redcards, let cardTwo = stats?.cards?.yellowredcards {
            if card > 0 || cardTwo > 0 {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
}

struct LineupPlayerStats: Decodable {
    var shots: LineupPlayerShots?
    var goals: LineupPlayerGoals?
    var fouls: LineupPlayerFouls?
    var cards: LineupPlayerCards?
    var passing: LineupPlayerPassing?
    var dribbles: LineupPlayerDribbles?
    var duels: LineupPlayerDuels?
    var other: LineupPlayerOtherStats?
}

struct LineupPlayerShots: Decodable {
    var shots_total: Int?
    var shots_on_goal: Int?
}

struct LineupPlayerGoals: Decodable {
    var scored: Int?
    var assists: Int?
    var conceded: Int?
}

struct LineupPlayerFouls: Decodable {
    var drawn: Int?
    var committed: Int?
    
}

struct LineupPlayerCards: Decodable {
    var yellowcards: Int?
    var redcards: Int?
    var yellowredcards: Int?
}

struct LineupPlayerPassing: Decodable {
    var total_crosses: Int?
    var crosses_accuracy: Int?
    var passes: Int?
    var passes_accuracy: Int?
    var key_passes: Int?
}

struct LineupPlayerDribbles: Decodable {
    var attempts: Int?
    var success: Int?
    var dribbled_past: Int?
    
}

struct LineupPlayerDuels: Decodable {
    var total: Int?
    var won: Int?
    
}

struct LineupPlayerOtherStats: Decodable {
    var offsides: Int?
    var saves: Int?
    var inside_box_saves: Int?
    var pen_scored: Int?
    var pen_missed: Int?
    var pen_saved: Int?
    var pen_committed: Int?
    var pen_won: Int?
    var hit_woodwork: Int?
    var tackles: Int?
    var blocks: Int?
    var interceptions: Int?
    var clearances: Int?
    var dispossesed: Int?
    var minutes_played: Int?
}
