//
//  Venue.swift
//  Faneever
//
//  Created by James Meli on 3/25/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct VenueResponse: Decodable {
    var data: Venue?
}

struct Venue: Decodable {
    var id: Int?
    var name: String?
    var surface: String?
    var address: String?
    var city: String?
    var capacity: Int?
    var image_path: String?
    var coordinates: String?
    
    func getImageUrl() -> URL? {
        if let urlString = self.image_path {
            return URL(string: urlString)
        }
        
        return nil
    }
}
