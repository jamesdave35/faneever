//
//  Season.swift
//  Faneever
//
//  Created by James Meli on 3/21/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct SeasonResponse: Decodable {
    var data: Season?
}

struct Season: Decodable {
    
    var id: Int?
    var name: String?
    var is_current_season: Bool?
    var league: LeagueResponse?
    var fixtures: FixturesResponse?
}
