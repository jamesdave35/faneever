//
//  TeamStatNumbers.swift
//  Faneever
//
//  Created by James Meli on 3/11/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct TeamStatsResponse: Decodable {
    var data: TeamStats?
}

struct SeasonTeamStats: Decodable {
    var data: [TeamStats]?
    
    func getTopGoals(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getGoalsScored() > $1.getRequiredSeason(id: seasonId).getGoalsScored()})
        }
        
        return []
    }
    
    func getTopWins(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getWins() > $1.getRequiredSeason(id: seasonId).getWins()})
        }
        
        return []
    }
    
    func getTopDraws(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getDraws() > $1.getRequiredSeason(id: seasonId).getDraws()})
        }
        
        return []
    }
    
    func getTopLost(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getLosses() > $1.getRequiredSeason(id: seasonId).getLosses()})
        }
        
        return []
    }
    
    func getTopCleenSheats(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getCleanSheets() > $1.getRequiredSeason(id: seasonId).getCleanSheets()})
        }
        
        return []
    }
    
    func getTopGoalsConceded(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getGoalsConceded() > $1.getRequiredSeason(id: seasonId).getGoalsConceded()})
        }
        
        return []
    }
    
    func getTopAvgGoalsPerGame(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).avgGoalsPerGame() > $1.getRequiredSeason(id: seasonId).avgGoalsPerGame()})
        }
        
        return []
    }
    
    func getTopAvgShotsOnTgt(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).avgShotsOnTarget() > $1.getRequiredSeason(id: seasonId).avgShotsOnTarget()})
        }
        
        return []
    }
    
    func getTopCorners(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getCorners() > $1.getRequiredSeason(id: seasonId).getCorners()})
        }
        
        return []
    }
    
    func getTopAvgPossession(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).avgPossession() > $1.getRequiredSeason(id: seasonId).avgPossession()})
        }
        
        return []
    }
    
    func getTopFouls(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getFouls() > $1.getRequiredSeason(id: seasonId).getFouls()})
        }
        
        return []
    }
    
    func getTopOffsides(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getOffsides() > $1.getRequiredSeason(id: seasonId).getOffsides()})
        }
        
        return []
    }
    
    func getTopYellowCards(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getYellowCards() > $1.getRequiredSeason(id: seasonId).getYellowCards()})
        }
        
        return []
    }
    
    func getTopRedCards(seasonId: Int) -> [TeamStats] {
        if let teams = data {
            return teams.sorted(by: {$0.getRequiredSeason(id: seasonId).getRedCards() > $1.getRequiredSeason(id: seasonId).getRedCards()})
        }
        
        return []
    }
}

struct TeamStats: Decodable {
    var name: String?
    var id: Int?
    var logo_path: String?
    var stats: TeamStatsData?
    var current_season_id: Int?
    
    func getRequiredSeason(id: Int) -> TeamStatNumebrs {
        if let teamStats = stats?.data {
            for stats in teamStats {
                if stats.season_id == id {
                    return stats
                }
            }
        }

        
        return TeamStatNumebrs()
    }
    
    func getLogoURL() -> URL? {
        if let urlString = self.logo_path {
            return URL(string: urlString)
        }
        
        return nil
    }
    
}

struct TeamStatsData: Decodable {
    
    var data: [TeamStatNumebrs]?
    

}

struct TeamStatNumebrs: Decodable {
    
    var team_id: Int?
    var season_id: Int?
    var games: Int?
    var win: Win?
    var draw: Draw?
    var lost: Lost?
    var goals_for: GoalsFor?
    var goals_against: GoalsAgainst?
    var clean_sheet: CleanSheets?
    var avg_goals_per_game_scored: AverageGoalsPerGameScored?
    var avg_goals_per_game_conceded: AverageGoalsPerGameConceded?
    var attacks: QuantumValue?
    var dangerous_attacks: QuantumValue?
    var avg_ball_possession_percentage: QuantumValue?
    var fouls: QuantumValue?
    var avg_fouls_per_game: QuantumValue?
    var offsides: QuantumValue?
    var redcards: QuantumValue?
    var yellowcards: QuantumValue?
    var shots_blocked: QuantumValue?
    var shots_off_target: QuantumValue?
    var avg_shots_off_target_per_game: QuantumValue?
    var shots_on_target: QuantumValue?
    var avg_shots_on_target_per_game: QuantumValue?
    var avg_corners: QuantumValue?
    var total_corners: QuantumValue?
    var season: SeasonResponse?
    
    init() {
        
    }
    
    init(data: NSDictionary) {

    }
    
    func getTotalGames() -> Int? {
        if let wins = self.win?.total?.intValue, let draws = self.draw?.total?.intValue, let lost = self.lost?.total?.intValue {
            return wins + draws + lost
        }
        
        return nil
    }
    
    func getGoalsScored() -> Int {
        if let value = self.goals_for?.total?.intValue {
            return value
        }
        
        return 0
    }
    
    func getGoalsConceded() -> Int {
        if let value = self.goals_against?.total?.intValue {
            return value
        }
        
        return 0
    }
    
    func getWins() -> Int {
        if let value = self.win?.total?.intValue {
            return value
        }
        
        return 0
    }
    
    func getDraws() -> Int {
        if let value = self.draw?.total?.intValue {
            return value
        }
        
        return 0
    }
    
    func getLosses() -> Int {
        if let value = self.lost?.total?.intValue {
            return value
        }
        
        return 0
    }
    
    func getCleanSheets() -> Int {
        if let value = self.clean_sheet?.total?.intValue {
            return value
        }
        
        return 0
    }
    
    func avgGoalsPerGame() -> Int {
        if let value = self.avg_goals_per_game_scored?.total?.intValue {
            return value
        }
        
        return 0
    }
    
    func avgShotsOnTarget() -> Int {
        if let value = self.avg_shots_on_target_per_game?.intValue {
            return value
        }
        
        return 0
    }
    
    func getCorners() -> Int {
        if let value = self.total_corners?.intValue {
            return value
        }
        
        return 0
    }
    
    func avgPossession() -> Int {
        if let value = self.avg_ball_possession_percentage?.intValue {
            return value
        }
        
        return 0
    }
    
    func getFouls() -> Int {
        if let value = self.fouls?.intValue {
            return value
        }
        
        return 0
    }
    
    func getOffsides() -> Int {
        if let value = self.offsides?.intValue {
            return value
        }
        
        return 0
    }
    
    func getYellowCards() -> Int {
        if let value = self.yellowcards?.intValue {
            return value
        }
        
        return 0
    }
    
    func getRedCards() -> Int {
        if let value = self.redcards?.intValue {
            return value
        }
        
        return 0
    }
    
}

struct Win: Decodable {
    var total: QuantumValue?
    var home: QuantumValue?
    var away: QuantumValue?
}

struct Draw: Decodable {
    var total: QuantumValue?
    var home: QuantumValue?
    var away: QuantumValue?
}

struct Lost: Decodable {
    var total: QuantumValue?
    var home: QuantumValue?
    var away: QuantumValue?
}

struct GoalsFor: Decodable {
    var total: QuantumValue?
    var home: QuantumValue?
    var away: QuantumValue?
}

struct GoalsAgainst: Decodable {
    var total: QuantumValue?
    var home: QuantumValue?
    var away: QuantumValue?
}

struct CleanSheets: Decodable {
    var total: QuantumValue?
    var home: QuantumValue?
    var away: QuantumValue?
}

struct AverageGoalsPerGameScored: Decodable {
    var total: QuantumValue?
    var home: QuantumValue?
    var away: QuantumValue?
}

struct AverageGoalsPerGameConceded: Decodable {
    var total: QuantumValue?
    var home: QuantumValue?
    var away: QuantumValue?
}

struct StatDisplay {
    
    var statName: String
    var statValue: Int?
}





enum QuantumValue: Decodable {

    case int(Int), string(String), double(Double)
    
    var intValue: Int? {
        switch self {
        case .int(let value): return value
        case .string(let value): return Int(value)
        case .double(let value): return Int(value)
        }
    }
    
    var stringValue: String? {
        switch self {
        case .string(let value): return value
        case .int(let value): return String(value)
        case .double(let value): return String(value)
        }
    }

    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self = .int(int)
            return
        }

        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(string)
            return
        }
        
        if let double = try? decoder.singleValueContainer().decode(Double.self) {
            self = .double(double)
            return
        }

        throw QuantumError.missingValue
    }

    enum QuantumError:Error {
        case missingValue
    }
}
