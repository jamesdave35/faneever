//
//  Coach.swift
//  Faneever
//
//  Created by James Meli on 4/1/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation

struct CoachResponse: Decodable {
    var data: Coach?
}

struct Coach: Decodable {
    var coach_id: Int?
    var team_id: Int?
    var fullname: String?
    var image_path: String?
    
    func getImageURL() -> URL? {
        if let urlString = image_path {
            return URL(string: urlString)
        }
        
        return nil
    }
}
