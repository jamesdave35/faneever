//
//  AuthServices.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Firebase

 class AuthServices {
    
     static func createUser(withEmail email: String, andPassword password: String, completion: @escaping(_ user: AuthDataResult?, _ error: Error?) -> Void) {
        
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if error != nil {
                completion(nil, error)
            } else {
                completion(result, nil)
            }
        }
        
    }
    
    static func signInUser(withEmail email: String, andPassword password: String, completion: @escaping(_ user: AuthDataResult?, _ error: Error?) -> Void) {
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            completion(result, error)
        }
        
    }
}
