//
//  StorageServices.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Firebase

 class StorageServices {
    
     static func saveUserProfilePhoto(image: UIImage, id: String, completion: @escaping(_ error: Error?, _ url: String?) -> Void) {
        
        let storageRef = Storage.storage().reference().child("profile-photos").child("\(id).jpeg")
        if let data = image.jpegData(compressionQuality: 0.3) {
            storageRef.putData(data, metadata: nil) { (metadata, error) in
                if error != nil {
                    completion(error, nil)
                } else {
                    storageRef.downloadURL { (url, error2) in
                        if error2 != nil {
                            completion(error2, nil)
                        } else {
                            if let photoUrl = url?.absoluteString {
                                completion(nil, photoUrl)
                            }
                        }
                    }
                }
            }
        }
    }
    
     static func savePostImage(image: UIImage?, id: String, videoUrl: URL?, videoPicked: Bool, completion: @escaping(_ error: Error?, _ imageUrl: String?, _ postVideoUrl: String?) -> Void) {
        
        if image == nil {
            completion(nil, nil, nil)
        } else {
            if videoPicked {
                let storageRef = Storage.storage().reference().child("post-videos").child("\(id).mp4")
                var data = Data()
                if let url = videoUrl {
                    do {
                        data = try Data(contentsOf: url, options: .dataReadingMapped)
                        storageRef.putData(data, metadata: nil) { (metadata, error2) in
                            if error2 != nil {
                                print("Error: \(error2?.localizedDescription)")
                                completion(error2, nil, nil)
                            } else {
                                storageRef.downloadURL { (url2, error3) in
                                    if error3 != nil {
                                        print("Error: \(error3?.localizedDescription)")
                                        completion(error3, nil, nil)
                                    } else {
                                        if let videoUrlToPass = url2?.absoluteString {
                                            completion(nil, nil, videoUrlToPass)
                                        }
                                    }
                                }
                            }
                        }
                    } catch let error {
                        print("Error: \(error.localizedDescription)")
                        completion(error, nil, nil)
                    }
                    
                    
                } else {
                    completion(nil, nil, nil)
                }

            } else {
                let storageRef = Storage.storage().reference().child("post-images").child("\(id).jpeg")
                if let data = image?.jpegData(compressionQuality: 0.3) {
                    storageRef.putData(data, metadata: nil) { (metadata, error) in
                        if error != nil {
                            completion(error, nil, nil)
                        } else {
                            storageRef.downloadURL { (url, error2) in
                                if error2 != nil {
                                    completion(error2, nil, nil)
                                } else {
                                    if let photoUrl = url?.absoluteString {
                                        completion(nil, photoUrl, nil)
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

    }
}
