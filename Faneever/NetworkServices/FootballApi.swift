//
//  FootballApi.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Foundation
import Alamofire

enum RequestError: Error {
    case networkError
    case noDataAvalaible
    case couldNotDecode
    case couldNotCast
}

 class FootballApi {
    
    static let baseUrl = "https://api-football-v1.p.rapidapi.com/v2/"
    static let betaBaseUrl = "https://api-football-beta.p.rapidapi.com/"
    static let footballProBaseUrl = "https://football-pro.p.rapidapi.com/api/v2.0/"
    static let countries = ["England", "Spain", "Germany", "Italy", "France"]
    static let popularLeagueIds = ["8", "564", "384", "82", "301"]
    static let headers = ["x-rapidapi-key": Constants.rapidApiKey]
    
    static func getPopularLeagues(completion: @escaping(Swift.Result<[League],RequestError>) -> Void) {
        var popularLeagues = [League]()
        var myGroup = DispatchGroup()
        
        for id in popularLeagueIds {
            myGroup.enter()
            Alamofire.request("\(footballProBaseUrl)leagues/\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
                if response.result.isFailure {
                    guard let error = response.result.error else {return}
                    completion(.failure(.networkError))
                } else if response.result.isSuccess {
                  
                    if let jsonData = response.result.value as? Data {
                        do {
                            let leagueResponse = try JSONDecoder().decode(LeagueResponse.self, from: jsonData)
                            if let league = leagueResponse.data {
                                popularLeagues.append(league)
                                myGroup.leave()
                            }
                        } catch {
                            completion(.failure(.couldNotDecode))
                        }
                    } else {
                        completion(.failure(.couldNotDecode))
                    }
                    
                }
            }
        }
        
        myGroup.notify(queue: .main) {
            completion(.success(popularLeagues))
        }
        
    }
    
    static func getTeamsWithSeasonId(seasonId: Int, completion: @escaping(Swift.Result<[Team], RequestError>) -> Void) {
        
        Alamofire.request("\(footballProBaseUrl)teams/season/\(seasonId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                do {
                    let teamResponse = try JSONDecoder().decode(TeamsResponse.self, from: jsonData)
                    if let teams = teamResponse.data {
                        completion(.success(teams))
                    }
                } catch {
                    completion(.failure(.couldNotDecode))
                }
            } else {
                guard let requestError = response.result.error else {return}
                completion(.failure(.networkError))
            }
        }
        
    }
    
    static func getLeagueById(leagueId: Int, completion: @escaping(Swift.Result<League, RequestError>) -> Void) {
        
        let params: Parameters = ["include": "country"]
        
        Alamofire.request("\(footballProBaseUrl)leagues/\(leagueId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                do {
                    let leagueResponse = try JSONDecoder().decode(LeagueResponse.self, from: jsonData)
                    if let league = leagueResponse.data {
                        completion(.success(league))
                    }
                } catch {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                guard let requestError = response.result.error else {return}
                print("Request error: \(requestError.localizedDescription)")
                completion(.failure(.networkError))
            }
        }
        
    }
    
    
    static func getTeamById(id: Int, completion: @escaping(Swift.Result<Team?, RequestError>) -> Void) {
        Alamofire.request("\(footballProBaseUrl)teams/\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                do {
                    let teamResponse = try JSONDecoder().decode(TeamResponse.self, from: jsonData)
                    if let team = teamResponse.data {
                        completion(.success(team))
                    }
                } catch {
                    completion(.failure(.couldNotDecode))
                }
            } else {
                guard let requestError = response.result.error else {return}
                completion(.failure(.networkError))
            }
        }
    }
    
    
    static func getPlayersFromSearch(searchString: String, completion: @escaping(Swift.Result<[Player], RequestError>) -> Void) {
        
        Alamofire.request("\(footballProBaseUrl)players/search/\(searchString)", method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                do {
                    let playersResponse = try JSONDecoder().decode(PlayersResponse.self, from: jsonData)
                    if let players = playersResponse.data {
                        completion(.success(players))
                    }
                } catch {
                    completion(.failure(.couldNotDecode))
                }
            } else {
                completion(.failure(.networkError))
            }
        }
        
    }
    
    static func getLeaguesFromSearch(searchString: String, completion: @escaping(Swift.Result<[League], RequestError>) -> Void) {
        Alamofire.request("\(footballProBaseUrl)leagues/search/\(searchString)", method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                do {
                    let leagueResponse = try JSONDecoder().decode(LeagueResponses.self, from: jsonData)
                    if let leagues = leagueResponse.data {
                        completion(.success(leagues))
                    }
                } catch let error {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                guard let requestError = response.result.error else {return}
                completion(.failure(.networkError))
            }
        }
    }
    
    static func getPlayerSeasonStats(playerId: Int, seasonId: Int, completion:
        @escaping(Swift.Result<PlayerStatNumbers, RequestError>) -> Void) {
        let params: Parameters = ["include": "stats.season,stats.league", "seasons": "\(seasonId)"]
        Alamofire.request("\(footballProBaseUrl)players/\(playerId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                do {
                    let playerStatResponse = try JSONDecoder().decode(PlayerStatResponse.self, from: jsonData)
                    if let statRes = playerStatResponse.data, let statData = statRes.stats, let data = statData.data {
                        
                        if !data.isEmpty {
                            completion(.success(data[0]))
                        }
                    }
                } catch {
                    completion(.failure(.couldNotDecode))
                }
            } else {
                completion(.failure(.networkError))
            }
        }
        
    }
    
    static func getPlayerAllSeasonsStats(playerId: Int, completion: @escaping(Swift.Result<[PlayerStatNumbers], RequestError>) -> Void) {
        
        let params: Parameters = ["include": "stats.season,stats.league"]
        Alamofire.request("\(footballProBaseUrl)players/\(playerId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                do {
                    let playerStatResponse = try JSONDecoder().decode(PlayerStatResponse.self, from: jsonData)
                    if let statRes = playerStatResponse.data, let statData = statRes.stats, let data = statData.data {
                        
                        if !data.isEmpty {
                            completion(.success(data))
                        }
                    }
                } catch {
                    completion(.failure(.couldNotDecode))
                }
            } else {
                completion(.failure(.networkError))
            }
        }
        
    }
    
    static func getTeamsFromSearch(searchString: String, completion: @escaping(Swift.Result<[Team], RequestError>) -> Void) {
        
        Alamofire.request("\(footballProBaseUrl)teams/search/\(searchString)", method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                do {
                    let teamResponse = try JSONDecoder().decode(TeamsResponse.self, from: jsonData)
                    if let teams = teamResponse.data {
                        completion(.success(teams))
                    }
                } catch {
                    completion(.failure(.couldNotDecode))
                }
            } else {
                guard let requestError = response.result.error else {return}
                completion(.failure(.networkError))
            }
        }
        
    }
    
    static func getCountryFromId(id: Int, completion: @escaping(Swift.Result<Country, RequestError>) -> Void) {
        Alamofire.request("\(footballProBaseUrl)countries/\(id)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                do {
                    let countryResponse = try JSONDecoder().decode(CountryResponse.self, from: jsonData)
                    if let country = countryResponse.data {
                        completion(.success(country))
                    }
                } catch {
                    completion(.failure(.couldNotDecode))
                }
            } else {
                completion(.failure(.networkError))
            }
        }
    }
    
    static func getTeamMainSeasonStats(teamId: Int, completion: @escaping(Swift.Result<TeamStatNumebrs, RequestError>) -> Void) {
        
        let params: Parameters = ["include": "stats.season.league"]
        Alamofire.request("\(footballProBaseUrl)teams/\(teamId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                
                do {
                    let teamStatResponse = try JSONDecoder().decode(TeamStatsResponse.self, from: jsonData)
                    if let teamStats = teamStatResponse.data, let statData = teamStats.stats, let currentSeasonId = teamStats.current_season_id, let stats = statData.data {
                        for stat in stats {
                            if let seasonId = stat.season_id {
                                if currentSeasonId == seasonId {
                                    completion(.success(stat))
                                }
                            }
                        }
                    }
                } catch let error {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                completion(.failure(.networkError))
            }
        }
    }
    
    static func getTeamNextFixture(teamId: Int, completion: @escaping(Swift.Result<Fixture?, RequestError>) -> Void) {
        
        if let currentDatePlus5Months = GlobalFunctions.currentDatePlusMonths(months: 5) {
            let endDateString = GlobalFunctions.dateAsStringForFixtures(date: currentDatePlus5Months)
            let currentDateString = GlobalFunctions.dateAsStringForFixtures(date: Date())
            
            let params: Parameters = ["tz": "America/Chicago", "include": "localTeam,visitorTeam, localCoach,visitorCoach,referee,round,league,venue,stage,events,lineup.player,bench.player,stats"]
            Alamofire.request("\(footballProBaseUrl)fixtures/between/\(currentDateString)/\(endDateString)/\(teamId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
                if response.result.isSuccess {
                    guard let jsonData = response.result.value as? Data else {return}
                    
                    do {
                        
                        let fixtureResponse = try JSONDecoder().decode(FixturesResponse.self, from: jsonData)
                        if let fixtures = fixtureResponse.data {
                            if !fixtures.isEmpty {
                                completion(.success(fixtures[0]))
                            } else {
                                completion(.success(nil))
                            }
                            
                        }
                    } catch {
                        completion(.failure(.couldNotDecode))
                    }
                } else {
                    print("Error getting fixtures: \(response.result.error)")
                    completion(.failure(.networkError))
                }
            }
        }
        
    }
    
    static func getTeamNextFixtures(teamId: Int, completion: @escaping(Swift.Result<[Fixture], RequestError>) -> Void) {
        
        if let currentDatePlus5Months = GlobalFunctions.currentDatePlusMonths(months: 5) {
            let endDateString = GlobalFunctions.dateAsStringForFixtures(date: currentDatePlus5Months)
            let currentDateString = GlobalFunctions.dateAsStringForFixtures(date: Date())
            
            let params: Parameters = ["tz": "America/Chicago", "include": "localTeam,visitorTeam, localCoach,visitorCoach,referee,round,league,venue,stage,events,lineup.player,bench.player,stats"]
            Alamofire.request("\(footballProBaseUrl)fixtures/between/\(currentDateString)/\(endDateString)/\(teamId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
                if response.result.isSuccess {
                    guard let jsonData = response.result.value as? Data else {return}
                    
                    do {
                        
                        let fixtureResponse = try JSONDecoder().decode(FixturesResponse.self, from: jsonData)
                        if let fixtures = fixtureResponse.data {
                            completion(.success(fixtures))
                        }
                    } catch {
                        completion(.failure(.couldNotDecode))
                    }
                } else {
                    print("Error getting fixtures: \(response.result.error)")
                    completion(.failure(.networkError))
                }
            }
        }
        
    }
    
    static func getTeamLast5Fixtures(teamId: Int, completion: @escaping(Swift.Result<[Fixture], RequestError>) -> Void) {
        
        var fixtures = [Fixture]()
        
        if let currentDateMinus5Months = GlobalFunctions.currentDateMinusMonths(months: -5) {
            let startDateString = GlobalFunctions.dateAsStringForFixtures(date: currentDateMinus5Months)
            let currentDateString = GlobalFunctions.dateAsStringForFixtures(date: Date())
            
            let params: Parameters = ["tz": "America/Chicago", "include": "localTeam,visitorTeam, localCoach,visitorCoach,referee,round,league,venue,stage,events,lineup.player,bench.player,stats"]
            
            Alamofire.request("\(footballProBaseUrl)fixtures/between/\(startDateString)/\(currentDateString)/\(teamId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
                if response.result.isSuccess {
                    guard let jsonData = response.result.value as? Data else {return}
                    
                    do {
                        let fixtureResponse = try JSONDecoder().decode(FixturesResponse.self, from: jsonData)
                        if let fixs = fixtureResponse.data {
                            if fixs.count >= 5 {
                                var counter = 0
                                for i in stride(from: fixs.count - 1, through: 0, by: -1) {
                                    if !fixs[i].isPostponed() && !fixs[i].isToday() {
                                        if counter < 5 {
                                            fixtures.append(fixs[i])
                                            counter += 1
                                        }
                                        
                                    }
                                    
                                }
                                
                                completion(.success(fixtures))
                            } else {
                                completion(.success(fixs))
                            }
                        }
                    } catch let error {
                        print("Error decoding: \(error)")
                        completion(.failure(.couldNotDecode))
                    }
                } else {
                    print("Error getting fixtures: \(response.result.error)")
                    completion(.failure(.networkError))
                }
            }
        }
        
    }
    
    static func getLeagueStandings(seasonId: Int, completion: @escaping(Swift.Result<[Standings], RequestError>) -> Void) {
        let params: Parameters = ["include": "standings.team"]
        Alamofire.request("\(footballProBaseUrl)standings/season/\(seasonId)", method: .get
            , parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
                if response.result.isSuccess {
                    guard let jsonData = response.result.value as? Data else {return}
                    
                    do {
                        let standingsResponse = try JSONDecoder().decode(LeagueSeasonStandingsResponse.self, from: jsonData)
                        if let standings = standingsResponse.data {
                            if !standings.isEmpty {
                                if let stans = standings[0].standings?.data {
                                    completion(.success(stans))
                                }
                            }
                            
                            
                        }
                        
                    }  catch let error {
                        print("Error decoding: \(error)")
                        completion(.failure(.couldNotDecode))
                    }
                } else {
                    print("Error getting fixtures: \(response.result.error)")
                    completion(.failure(.networkError))
                }
        }
        
    }
    
    static func getTeamTopPlayers(seasonId: Int, teamId: Int, completion: @escaping(Swift.Result<TeamPlayersStats, RequestError>) -> Void) {
        
        let params: Parameters = ["include": "player"]
        Alamofire.request("\(footballProBaseUrl)squad/season/\(seasonId)/team/\(teamId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                
                do {
                    let teamPlayerResponse = try JSONDecoder().decode(TeamPlayersStats.self, from: jsonData)
                    completion(.success(teamPlayerResponse))
                    
                }  catch let error {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                print("Error getting fixtures: \(response.result.error)")
                completion(.failure(.networkError))
            }
        }
        
    }
    
    static func getSeasonById(seasonId: Int, completion: @escaping(Swift.Result<Season, RequestError>) -> Void) {
        let params: Parameters = ["include": "league,stages,rounds"]
        Alamofire.request("\(footballProBaseUrl)seasons/\(seasonId)", method: .get
            , parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
                if response.result.isSuccess {
                    guard let jsonData = response.result.value as? Data else {return}
                    
                    do {
                        let seasonResponse = try JSONDecoder().decode(SeasonResponse.self, from: jsonData)
                        if let season = seasonResponse.data {
                            completion(.success(season))
                        }
                        
                    }  catch let error {
                        print("Error decoding: \(error)")
                        completion(.failure(.couldNotDecode))
                    }
                } else {
                    print("Error getting fixtures: \(response.result.error)")
                    completion(.failure(.networkError))
                }
        }
    }
    
    static func getVenueById(venueId: Int, completion: @escaping(Swift.Result<Venue, RequestError>) -> Void) {
        
        Alamofire.request("\(footballProBaseUrl)venues/\(venueId)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                
                do {
                    let venueResponse = try JSONDecoder().decode(VenueResponse.self, from: jsonData)
                    if let venue = venueResponse.data {
                        completion(.success(venue))
                    }
                    
                }  catch let error {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                print("Error getting fixtures: \(response.result.error)")
                completion(.failure(.networkError))
            }
        }
        
    }
    
    static func getFixtureById(fixtureId: Int, completion: @escaping(Swift.Result<Fixture, RequestError>) -> Void) {
        
        let params: Parameters = ["tz": "America/Chicago", "include": "localTeam,visitorTeam, localCoach,visitorCoach,referee,round,league,venue,stage,events,lineup.player,bench.player,stats"]
        
        Alamofire.request("\(footballProBaseUrl)fixtures/\(fixtureId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                
                do {
                    let fixtureResponse = try JSONDecoder().decode(FixtureResponse.self, from: jsonData)
                    if let fix = fixtureResponse.data {
                        completion(.success(fix))
                    }
                } catch let error {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                print("Error getting fixtures: \(response.result.error)")
                completion(.failure(.networkError))
            }
        }
        
    }
    
    static func getFollowingTeams(completion: @escaping(Swift.Result<[Team], RequestError>) -> Void) {
        var myGroup = DispatchGroup()
        var teams = [Team]()
        if let followingTeams = Constants.currentUser?.followingTeams {
            for team in followingTeams {
                myGroup.enter()
                self.getTeamById(id: team) {  (result) in
                    switch result {
                    case .failure(let error):
                        completion(.failure(error))
                    case .success(let returnedTeam):
                        if let teamToAdd = returnedTeam {
                            teams.append(teamToAdd)
                            myGroup.leave()
                        }
                    }
                }
            }
            
            myGroup.notify(queue: .main) {
                completion(.success(teams))
            }
        }
    }
    
    static func getFollowingTeamsFixtures(completion: @escaping(Swift.Result<[Int: TeamAndFixture], RequestError>) -> Void) {
        var teamsAndFixtures: [Int: TeamAndFixture] = [:]
        var counter = 0
        var dispatchGroup = DispatchGroup()
        self.getFollowingTeams { (result) in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let teams):
                for team in teams {
                    dispatchGroup.enter()
                    self.getTeamNextFixtures(teamId: team.id!) { (fixtureResults) in
                        switch fixtureResults {
                        case .failure(let error2):
                            completion(.failure(error2))
                        case .success(let teamFixtures):
                            let teamAndFixToAdd = TeamAndFixture(team: team, fixtures: teamFixtures.prefix(3))
                            teamsAndFixtures[counter] = teamAndFixToAdd
                            counter += 1
                            dispatchGroup.leave()
                        }
                    }
                }
                
                dispatchGroup.notify(queue: .main) {
                    completion(.success(teamsAndFixtures))
                }
            }
        }
        
    }
    
    static func getHeadToHeadFixtures(firstTeamId: Int, secondTeamId: Int, completion: @escaping(Swift.Result<[Fixture], RequestError>) -> Void) {
        
        let params: Parameters = ["tz": "America/Chicago", "include": "localTeam,visitorTeam,league"]
        Alamofire.request("\(footballProBaseUrl)head2head/\(firstTeamId)/\(secondTeamId)", method: .get
            , parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
                if response.result.isSuccess {
                    guard let jsonData = response.result.value as? Data else {return}
                   
                    do {

                        let fixtureResponse = try JSONDecoder().decode(FixturesResponse.self, from: jsonData)
                        if let fixtures = fixtureResponse.data {
                            completion(.success(fixtures))
                        }
                    } catch let error {
                        print("Error decoding: \(error)")
                        completion(.failure(.couldNotDecode))
                    }
                } else {
                    print("Error getting fixtures: \(response.result.error)")
                    completion(.failure(.networkError))
                }
        }
        
    }
    
    static func getTeamFixtures(teamId: Int, completion: @escaping(Swift.Result<[Fixture], RequestError>) -> Void) {
        
        if let endDate = GlobalFunctions.currentDatePlusMonths(months: 5), let startDate = GlobalFunctions.currentDateMinusMonths(months: -5) {
            let endDateString = GlobalFunctions.dateAsStringForFixtures(date: endDate)
            let startDateString = GlobalFunctions.dateAsStringForFixtures(date: startDate)
            
            let params: Parameters = ["tz": "America/Chicago", "include": "localTeam,visitorTeam, localCoach,visitorCoach,referee,round,league,venue,stage,events,lineup.player,bench.player,stats"]
            
            Alamofire.request("\(footballProBaseUrl)fixtures/between/\(startDateString)/\(endDateString)/\(teamId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
                if response.result.isSuccess {
                    guard let jsonData = response.result.value as? Data else {return}
                    
                    do {
                        let fixtureResponse = try JSONDecoder().decode(FixturesResponse.self, from: jsonData)
                        if let fixs = fixtureResponse.data {
                            completion(.success(fixs))
                        }
                    } catch let error {
                        print("Error decoding: \(error)")
                        completion(.failure(.couldNotDecode))
                    }
                } else {
                    print("Error getting fixtures: \(response.result.error)")
                    completion(.failure(.networkError))
                }
            }
            
        }
        
    }
    
    
    static func getTeamSquad(teamId: Int, seasonId: Int, completion: @escaping(Swift.Result<[PlayerStatNumbers], RequestError>) -> Void) {
        
        let params: Parameters = ["include": "player.country,player.team,player.position,position"]
        
        Alamofire.request("\(footballProBaseUrl)squad/season/\(seasonId)/team/\(teamId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                
                do {
                    let squadResponse = try JSONDecoder().decode(SquadResponse.self, from: jsonData)
                    if let players = squadResponse.data {
                        completion(.success(players))
                    }
                } catch let error {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                print("Error getting fixtures: \(response.result.error)")
                completion(.failure(.networkError))
            }
        }
        
    }
    
    static func getLeagueFixtures(seasonId: Int, completion: @escaping(Swift.Result<[Fixture], RequestError>) -> Void) {
    
        let params: Parameters = ["tz": "America/Chicago", "include": "fixtures.localTeam,fixtures.visitorTeam"]
        
        Alamofire.request("\(footballProBaseUrl)seasons/\(seasonId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                
                do {
                    let seasonResponse = try JSONDecoder().decode(SeasonResponse.self, from: jsonData)
                    if let fixs = seasonResponse.data?.fixtures?.data {
                        completion(.success(fixs))
                    }
                } catch let error {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                print("Error getting fixtures: \(response.result.error)")
                completion(.failure(.networkError))
            }
        }
        
        
    }
    
    static func getLeagueTopPlayers(seasonId: Int, completion: @escaping(Swift.Result<TopScorers, RequestError>) -> Void) {
        
        let params: Parameters = ["tz": "America/Chicago", "include": "aggregatedGoalscorers.player,aggregatedGoalscorers.team,aggregatedCardscorers.player,aggregatedCardscorers.team,aggregatedAssistscorers.player,aggregatedAssistscorers.team"]
        
        Alamofire.request("\(footballProBaseUrl)topscorers/season/\(seasonId)/aggregated", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                
                do {
                    let topScorerResponse = try JSONDecoder().decode(TopScorersResponse.self, from: jsonData)
                    if let topScorers = topScorerResponse.data {
                        completion(.success(topScorers))
                    }
                } catch let error {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                print("Error getting fixtures: \(response.result.error)")
                completion(.failure(.networkError))
            }
        }
    }
    
    static func getLeagueTeamStats(seasonId: Int, completion: @escaping(Swift.Result<SeasonTeamStats, RequestError>) -> Void) {
        
        let params: Parameters = ["include": "stats"]
        
        Alamofire.request("\(footballProBaseUrl)teams/season/\(seasonId)", method: .get, parameters: params, encoding: URLEncoding.queryString, headers: headers).responseData { (response) in
            if response.result.isSuccess {
                guard let jsonData = response.result.value as? Data else {return}
                
                do {
                    let seasonTeamStats = try JSONDecoder().decode(SeasonTeamStats.self, from: jsonData)
                    completion(.success(seasonTeamStats))
                    
                } catch let error {
                    print("Error decoding: \(error)")
                    completion(.failure(.couldNotDecode))
                }
            } else {
                print("Error getting fixtures: \(response.result.error)")
                completion(.failure(.networkError))
            }
        }
        
    }
    
    
    
    
}
