//
//  DatabaseServices.swift
//  Faneever
//
//  Created by James Meli on 2/29/20.
//  Copyright © 2020 James Meli. All rights reserved.
//

import Firebase

 class DatabaseServices {
    
    
     static func saveUserToDatabase(user: User, completion: @escaping(_ error: Error?) -> Void) {
        
        let userRef = Firestore.firestore().collection("Users").document(user.uid)
        userRef.setData(user.toDictionary()) { (error) in
            completion(error)
        }
    }
    
     static func addProfilePhotoUrlToDatabase(id: String, url: String) {
        let userRef = Firestore.firestore().collection("Users").document(id)
        userRef.setData(["profileImageUrl": url], mergeFields: ["profileImageUrl"]) { (error) in
            if error != nil {
                print("Error saving url to database: \(error?.localizedDescription)")
            }
        }
    }
    
     static func saveFavoriteTeamToDatabase(team: NSDictionary, completion: @escaping(_ error: Error?) -> Void) {
        if let id = Constants.currentUserId {
            let userRef = Firestore.firestore().collection("Users").document(id)
            userRef.setData(["favoriteTeam": team], mergeFields: ["favoriteTeam"]) { (error) in
                if error != nil {
                    print("Error saving team to database: \(error?.localizedDescription)")
                    completion(error)
                } else {
                    completion(nil)
                }
            }
            
        }
    }
    
     static func saveFollowingTeamsToDatabase(teams: [Int], completion: @escaping(_ error: Error?) -> Void) {
        if let id = Constants.currentUserId {
            let userRef = Firestore.firestore().collection("Users").document(id)
            userRef.setData(["followingTeams": teams], mergeFields: ["followingTeams"]) { (error) in
                if error != nil {
                    print("Error saving teams to database: \(error?.localizedDescription)")
                    completion(error)
                } else {
                    completion(nil)
                }
            }
            
        }
    }
    
     static func saveFollowingLeaguesToDatabase(leagues: [Int], completion: @escaping(_ error: Error?) -> Void) {
        if let id = Constants.currentUserId {
            let userRef = Firestore.firestore().collection("Users").document(id)
            userRef.setData(["followingLeagues": leagues], mergeFields: ["followingLeagues"]) { (error) in
                if error != nil {
                    print("Error saving leagues to database: \(error?.localizedDescription)")
                    completion(error)
                } else {
                    completion(nil)
                }
            }
            
        }
    }
    
     static func getCurrentUser(completion: @escaping(_ user: User?) -> Void) {
        if let id = Constants.currentUserId {
            let userRef = Firestore.firestore().collection("Users").document(id)
            userRef.getDocument { (snapshot, error) in
                if error != nil {
                    print("Error fetching user: \(error?.localizedDescription)")
                } else {
                    if let data = snapshot?.data() as? NSDictionary {
                        let user = User(data: data)
                        completion(user)
                    }
                }
            }
        }
    }
    
     static func getUserWithId(id: String, completion: @escaping(_ user: User?) -> Void) {
        let userRef = Firestore.firestore().collection("Users").document(id)
        userRef.getDocument { (snapshot, error) in
            if error != nil {
                print("Error fetching user: \(error?.localizedDescription)")
            } else {
                if let data = snapshot?.data() as? NSDictionary {
                    let user = User(data: data)
                    completion(user)
                }
            }
        }
    }
    
    static func savePostToDatabase(post: Post, completion: @escaping(_ error: Error?) -> Void) {
        let postRef = Firestore.firestore().collection("Posts").document(post.postId)
        postRef.setData(post.toDictionary()) { (error) in
            completion(error)
        }
    }
    
    static func savePostReply(reply: Post, isReplyTo post: Post, completion: @escaping(_ error: Error?) -> Void) {
        let postRef = Firestore.firestore().collection("Posts").document(post.postId).collection("Replies").document(reply.postId)
        postRef.setData(reply.toDictionary()) { (error) in
            completion(error)
        }
    }
    
    static func getNumberOfPostReplies(post: Post, completion: @escaping(_ number: Int?) -> Void) {
        let postRef = Firestore.firestore().collection("Posts").document(post.postId).collection("Replies")
        postRef.addSnapshotListener { (snapshot, error) in
            if error != nil {
                print("Error: \(error?.localizedDescription)")
            } else {
                guard let querySnapshot = snapshot else {return}
                completion(querySnapshot.documents.count)
                
            }
        }
    }
    
    static func likePost(id: String, post: Post) {
        let postRef = Firestore.firestore().collection("Posts").document(post.postId)
        postRef.getDocument { (snapshot, error) in
            if error != nil {
                print("Error liking post: \(error?.localizedDescription)")
            } else if let data = snapshot?.data() as? NSDictionary {
                let post = Post(data: data)
                if var likes = post.likes {
                    likes.append(id)
                    postRef.setData(["likes": likes], mergeFields: ["likes"])
                }
            }
        }
    }
    
    static func unLikePost(id: String, post: Post) {
        let postRef = Firestore.firestore().collection("Posts").document(post.postId)
        postRef.getDocument { (snapshot, error) in
            if error != nil {
                print("Error liking post: \(error?.localizedDescription)")
            } else if let data = snapshot?.data() as? NSDictionary {
                let post = Post(data: data)
                if var likes = post.likes {
                    if likes.contains(id) {
                        if let index = likes.index(of: id) {
                            likes.remove(at: index)
                            postRef.setData(["likes": likes], mergeFields: ["likes"])
                        }
                    }
                    
                    
                }
            }
        }
    }
    
    static func hasLikedPost(id: String, post: Post, completion: @escaping(_ value: Bool) -> Void) {
        let postRef = Firestore.firestore().collection("Posts").document(post.postId)
        postRef.getDocument { (snapshot, error) in
            if error != nil {
                print("Error liking post: \(error?.localizedDescription)")
                completion(false)
            } else if let data = snapshot?.data() as? NSDictionary {
                let post = Post(data: data)
                if var likes = post.likes {
                    if likes.contains(id) {
                        completion(true)
                    } else {
                        completion(false)
                    }
                    
                }
            } else {
                completion(false)
            }
        }
    }
    
    static func getNumberOfLikes(post: Post, completion: @escaping(_ number: Int?) -> Void) {
        let postRef = Firestore.firestore().collection("Posts").document(post.postId)
        postRef.addSnapshotListener { (snapshot, error) in
            if let data = snapshot?.data() as? NSDictionary {
                let post = Post(data: data)
                if let likes = post.likes {
                    completion(likes.count)
                } else {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
    }
    
     static func getPosts(completion: @escaping(_ posts: [Post], _ error: Error?) -> Void) {
        var posts = [Post]()
        let postRef = Firestore.firestore().collection("Posts")
        postRef.getDocuments { (snapshot, error) in
            if error != nil {
                completion(posts, nil)
            } else {
                if let documents = snapshot?.documents {
                    for document in documents {
                        if let data = document.data() as? NSDictionary {
                            let postToAppend = Post(data: data)
                            postToAppend.setPostData { (success) in
                                posts.append(postToAppend)
                                completion(posts, nil)
                                
                            }
                            
                            
                        }
                    }
                    
                }
            }
        }
        
    }
    
    
}
